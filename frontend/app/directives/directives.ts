import * as angular from "angular";
import focusMeModuleName from './focusme.directive';
import equalsModuleName from './equals.directive';
import ngEnterModuleName from './ngenter.directive';
import closeOnEnterModuleName from './closeOnEnter';
import remoteValidationModuleName from './remotevalidate.directive';

let directivesModule: ng.IModule = angular.module('app.directives', [
    closeOnEnterModuleName,
    focusMeModuleName,
    equalsModuleName,
    remoteValidationModuleName,
    ngEnterModuleName
]);

let directivesModuleName: string = directivesModule.name;

export default directivesModuleName;
