import * as angular from "angular";

let closeOnEnter = function (): angular.IDirective {
    return {
        restrict: 'A',
        require: 'mdAutocomplete',
        link: function(scope, element) {
            element.on('keydown keypress', function($event) {
                // 13: Enter
                if ($event.keyCode == 13) {
                    console.log('here')
                    let eAcInput = this.getElementsByTagName('input')[0];
                    setTimeout(() => {
                        eAcInput.blur();
                    }, 50);

                }
            });
        },
    };
};

let closeOnEnterModule: ng.IModule = angular.module('closeOnEnter', []);

closeOnEnterModule.directive('closeOnEnter', closeOnEnter);

let closeOnEnterModuleName: string = closeOnEnterModule.name;

export default closeOnEnterModuleName;
