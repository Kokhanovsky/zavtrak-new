import * as angular from "angular";

let ngEnter = function (): angular.IDirective {

    return (scope: ng.IScope, element: any, attrs: any) => {
        element.bind("keydown keypress", (event: any) => {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });
                event.preventDefault();
            }
        });
    };

};

let ngEnterModule: ng.IModule = angular.module('ngEnter', []);

ngEnterModule.directive('ngEnter', ngEnter);

let ngEnterModuleName: string = ngEnterModule.name;

export default ngEnterModuleName;

