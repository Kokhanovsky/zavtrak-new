import IPromise = angular.IPromise;
import { IFile } from './photo.controller';

export class PhotoService {

    /** @ngInject */
    constructor(private Upload: any, private $http: angular.IHttpService) {
    }

    upload(type: string, files: Array<angular.angularFileUpload.IFileUploadConfigFile>) {
        let url = this.getEndpoint(type);
        if (!url) {
            return;
        }
        return this.Upload.upload({
            url,
            arrayKey: '',
            data: { files },
            method: 'POST'
        });
    }

    deletePhoto(type, file: IFile) {
        let url = this.getEndpoint(type);
        if (!url) {
            return;
        }
        return this.$http({
            method: 'DELETE',
            url,
            data: { file }
        });
    }

    getEndpoint(type: string) {
        let url: string;
        switch (type) {
            case 'recipe':
                url = '/api/recipe/photo';
                break;
            case 'article':
                url = '/api/article/photo';
                break;
        }
        return url;
    }

}
