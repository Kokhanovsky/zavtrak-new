import * as angular from "angular";
import photoComponentOptions from './photo.component';
import { PhotoService } from './photo.service';

let photoModule: ng.IModule = angular.module('photoUploadModule', []);

photoModule.component('photo', photoComponentOptions);
photoModule.service('PhotoService', PhotoService);

let photoModuleName: string = photoModule.name;

export default photoModuleName;
