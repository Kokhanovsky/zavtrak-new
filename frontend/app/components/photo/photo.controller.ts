import { PhotoService } from './photo.service';

enum Mode { Add, Loading }

export interface IFile {
    file: string;
    user: string;
    item?: string;
    description?: string;
}

class PhotoController {

    public model: ng.INgModelController;
    loading: boolean = false;
    progress: number = 0;
    mode: Mode = Mode.Add;
    cancelUploading: any;
    maxFiles: number = 8;
    maxSize: string = '15MB';
    itemId: string;
    files: any[] = [];
    editMode: boolean;
    type: string;

    /** @ngInject */
    constructor(private PhotoService: PhotoService,
                private $mdDialog: any,
                private toastr: any,
                private $scope: ng.IScope) {
        $scope.$watch(() => this.model.$viewValue, (val: IFile[]) => {
            if (val) {
                this.files = val;
            }
        });
    }

    upload(files: any) {

        if (!files.length) {
            //this.showError('Неправильный формат изображения или размер изображения слишком большой');
            return;
        }

        if ((this.files.length + files.length) > this.maxFiles) {
            this.showError('Максимальное количество фото: ' + this.maxFiles + 'шт. Чтобы добавить новые фото, необходимо удалить какие-то фото из списка.');
            return;
        }

        this.progress = 0;
        this.loading = true;
        let isAborted = false;
        this.mode = Mode.Loading;
        this.PhotoService.upload(this.type, files)
            .xhr((xhr: any) => {
                this.cancelUploading = () => {
                    xhr.abort();
                    this.mode = Mode.Add;
                    isAborted = true;
                };
            })
            .progress((data: angular.angularFileUpload.IFileProgressEvent) => {
                this.progress = Math.round((data.loaded / data.total) * 100);
            })

            .then((response: any) => {
                let files: IFile[] = response.data;
                this.mode = Mode.Add;
                this.files = this.files.concat(files).splice(0, this.maxFiles);
                this.model.$setViewValue(this.files);
            })

            .catch((err: any) => {
                if (isAborted) {
                    console.log('Загрузка файла прервана');
                    return;
                }
                this.toastr.error(`Ошибка загрузки`);
                this.mode = Mode.Add;
            })

            .then(() => {
                this.loading = false;
            });
    }

    getFileSrc(file: any) {
        let res: string = '';
        switch(this.type)
        {
            case 'recipe': res = '/storage/cache/recipes/' + file._id + '/150x150.jpg';
                break;
            case 'article': res = '/storage/cache/articles/' + file._id + '/150x150.jpg';
                break;
        }
        return res;
    }

    removePhoto(file: IFile) {
        this.files = this.files.filter(f => (f !== file));
        this.model.$setViewValue(this.files);
        if (file.item) {
            return;
        }
        if (this.editMode) {
            return;
        }
        this.PhotoService.deletePhoto(this.type, file).then(() => console.log('deleted file'));
    }

    resetUpload() {
        this.mode = Mode.Add;
    }

    private showError(message: string) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Ошибка загрузки фото')
            .textContent(message)
            .ok('ok'));
        return;
    }

    private showHint(message: string) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Подсказка')
            .textContent(message)
            .ok('ok'));
        return;
    }

}

export default PhotoController;
