//import { IUserLogin } from './login.controller';
import IPromise = angular.IPromise;

export class ResetPasswordService {

    /** @ngInject */
    constructor(private $log: angular.ILogService,
                private $http: angular.IHttpService) {
    }

    reset(loginData: any): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/reset/password',
                data: loginData
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    update(updateData: any): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/reset/update-password',
                data: updateData
            })
            .then((response: any): any => {
                return response.data;
            });
    }
}
