import { ResetPasswordService } from './reset-password.service';

export interface IModelUpdatePassword {
    password: string;
    retypePassword: string;
}

class UpdatePasswordController {

    updateSuccess: boolean = false;
    loading: boolean = false;

    model: IModelUpdatePassword = {
        password: '',
        retypePassword: ''
    };
    serverError: string;

    /** @ngInject */
    constructor(private $window: ng.IWindowService,
                private ResetPasswordService: ResetPasswordService,
                private $scope: ng.IScope) {

        $scope.$watch(() => this.model, () => {
            this.serverError = null;
        }, true);

    }

    update(model: IModelUpdatePassword) {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.ResetPasswordService.update(model)
            .then(() => {
                this.updateSuccess = true;
            })
            .catch(() => {
                this.$window.location.href = '/reset/recovery-error';
            })
            .then(() => {
                this.loading = false;
            });
    }

}

export default UpdatePasswordController;
