import UpdatePasswordController from './update-password.controller';
import './reset-password.sass';

let UpdatePasswordComponent: ng.IComponentOptions = {
    bindings: {
        showComponent: '='
    },
    template: require('./update-password.html'),
    controller:  UpdatePasswordController
};

export default UpdatePasswordComponent;
