import * as angular from "angular";
import tagComponentOptions from './tag.component';
import { TagService } from './tag.service';

let tagModule: ng.IModule = angular.module('tagModule', []);

tagModule.component('tag', tagComponentOptions);
tagModule.service('TagService', TagService);

let tagModuleName: string = tagModule.name;

export default tagModuleName;

