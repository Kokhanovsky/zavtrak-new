import IPromise = angular.IPromise;

export class TagService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getTags(q: string): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/tags',
                params: { q }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    getTagsByIds(ids: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/tags/ids',
                data: {ids}
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    addTag(tag: string): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/tags',
                data: {tag}
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
