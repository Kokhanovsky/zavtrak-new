import CategoryController from './tag.controller';
import './tag.sass';

let categoryComponent: angular.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    template: require('./tag.html'),
    controller:  CategoryController
};

export default categoryComponent;
