import { TagService } from './tag.service';
import { ITag } from '../../../../backend/models/tag';
import * as _ from 'underscore';

class TagController {

    public model: ng.INgModelController;

    searchText: any = null;
    selectedTags: any[] = [];

    /** @ngInject */
    constructor(private TagService: TagService, private $scope: ng.IScope, private $timeout: ng.ITimeoutService) {
    }

    $onInit() {
        this.$timeout(() => {
            if (_.isArray(this.model.$viewValue)) {
                this.TagService.getTagsByIds(this.model.$viewValue).then(tags => this.selectedTags = tags);
            }
        }).then(() => {
            this.$scope.$watch(() => this.selectedTags, (tags: ITag[]) => {
                let val = tags.map(tag => tag._id);
                this.model.$setViewValue(val);
                this.model.$setValidity("required", !!val.length);
            }, true);
        });
    }

    querySearchDeferred() {
        return this.TagService.getTags(this.searchText);
    }

    add(chip: any) {
        this.selectedTags = _.without(this.selectedTags, chip);
        if (_.isString(chip)) {
            this.TagService.addTag(chip).then((tag: ITag) => {
                if (!_.findWhere(this.selectedTags, {tag: tag.tag})) {
                    this.selectedTags.push(tag);
                }
            })
        } else {
            this.selectedTags.push(chip);
        }
    }


}

export default TagController;
