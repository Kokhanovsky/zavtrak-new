import * as angular from 'angular';

import recipesRecentComponentOptions from './recipes-recent.component';
import { RecipesRecentService } from './recipes-recent.service';

let recipesRecentModule: ng.IModule = angular.module('recipesRecent', []);

recipesRecentModule.component('recipesRecent', recipesRecentComponentOptions);
recipesRecentModule.service('RecipesRecentService', RecipesRecentService);

let recipesRecentModuleName: string = recipesRecentModule.name;

export default recipesRecentModuleName;


