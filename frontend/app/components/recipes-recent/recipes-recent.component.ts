import RecipesRecentController from './recipes-recent.controller';
import './recipes-recent.sass';

let recipesRecentComponent: ng.IComponentOptions = {
    bindings: {
    },
    template: require('./recipes-recent.html'),
    controller:  RecipesRecentController
};

export default recipesRecentComponent;
