import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { RecipesRecentService } from './recipes-recent.service';
import { IRecipe } from '../../../../backend/models/recipe';

class RecipesRecentController {

    recipes: IRecipe[];

    /** @ngInject */
    constructor(private RecipesRecentService: RecipesRecentService) {

    }

    $onInit() {
        this.recipes = this.RecipesRecentService.getRecipes();
    }

}

export default RecipesRecentController;
