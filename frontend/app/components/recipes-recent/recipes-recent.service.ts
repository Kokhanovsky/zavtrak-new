import * as angular from "angular";

export class RecipesRecentService {

    /** @ngInject */
    constructor(private $window: angular.IWindowService) {
    }

    getRecipes() {
        let window: any = this.$window;
        if (!window.recipes)
            return;
        return window.recipes;
    }

}
