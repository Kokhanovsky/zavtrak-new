import { ArticleAddService } from './article-add.service';
import * as angular from 'angular';
import { IArticleCategory } from '../../../../backend/models/article-category';

export interface IArticleData {
    title?: string;
    content?: string;
    article_id?: string;
    tags?: string[];
    category?: string[];
}

class ArticleAddController {

    loading: boolean = false;
    articleData: IArticleData = {
        tags: []
    };
    initialData: IArticleData;
    keys: any[];
    editId: string;
    sendform: boolean = false;
    categories: IArticleCategory[];


    /** @ngInject */
    constructor(private ArticleAddService: ArticleAddService,
                private $mdConstant: any,
                private $window: ng.IWindowService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {

        this.$timeout(() => {
            this.initialData = angular.copy(this.articleData);
        });

        this.$window.onbeforeunload = event => {
            if (!angular.equals(this.articleData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };

        this.categories = this.ArticleAddService.getCategories();

        if (this.editId) {
            this.ArticleAddService.getItem(this.editId).then((item: IArticleData) => {
                this.articleData = item;
                this.initialData = angular.copy(this.articleData);
            })
            .catch(() => {
                this.$window.location.href = '/';
            })
        }

    }

    sendForm(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.articleData);
        let action = this.editId ? this.ArticleAddService.edit(this.articleData) : this.ArticleAddService.add(this.articleData);
        action
        .then(url => {
            this.$window.location.href = url;
        })
        .catch((err: any) => {
            console.log(err);
            this.loading = false;
            (this.$rootScope as any).loading = true;
        });
    }

    insertImage(file) {
        this.articleData.content = '<img src="' + '/storage/cache/articles/' + file._id + '/1280x720.jpg' + '" style="width: 100%">' + this.articleData.content;
    }

}

export default ArticleAddController;
