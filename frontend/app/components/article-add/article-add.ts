import * as angular from "angular";
import articleAddComponentOptions from './article-add.component';
import { ArticleAddService } from './article-add.service';

let articleAddModule: ng.IModule = angular.module('articleAddModule', []);

articleAddModule.component('articleAdd', articleAddComponentOptions);
articleAddModule.service('ArticleAddService', ArticleAddService);

let articleAddModuleName: string = articleAddModule.name;

export default articleAddModuleName;


