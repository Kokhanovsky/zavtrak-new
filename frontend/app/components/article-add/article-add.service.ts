import IPromise = angular.IPromise;
import { IArticleData } from "./article-add.controller";

export class ArticleAddService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService, private $window: ng.IWindowService) {
    }

    add(articleData: IArticleData) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/article',
                data: articleData
            }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/article',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    getItem(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/article/edit',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    getCategories() {
        let window: any = this.$window;
        if (!window.categories)
            return;
        return window.categories;
    }

}
