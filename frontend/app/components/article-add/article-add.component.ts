import ArticleAddController from './article-add.controller';

let articleAddComponent: ng.IComponentOptions = {
    bindings: {
        editId: '<'
    },
    template: require('./article-add.html'),
    controller:  ArticleAddController
};

export default articleAddComponent;
