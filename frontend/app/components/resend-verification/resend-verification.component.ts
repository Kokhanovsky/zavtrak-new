import ResendVerificationController from './resend-verification.controller';
import './resend-verification.sass';

let ResetPasswordComponent: ng.IComponentOptions = {
    bindings: {
        recaptchaSiteKey: '<'
    },
    template: require('./resend-verification.html'),
    controller:  ResendVerificationController
};

export default ResetPasswordComponent;
