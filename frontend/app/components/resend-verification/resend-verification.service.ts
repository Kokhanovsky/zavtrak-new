import IPromise = angular.IPromise;

export class ResendVerificationService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    resend(loginData: any): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/user/resend/verification',
                data: loginData
            })
            .then((response: any): any => {
                return response.data;
            });
    }
}
