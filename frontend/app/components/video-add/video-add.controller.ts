import { VideoAddService } from './video-add.service';
import * as angular from 'angular';
import { IYoutubeItem } from '../../../../backend/lib/interfaces';

export interface IVideoData {
    title?: string;
    description?: string;
    video_id?: string;
    tags?: string[];
    category?: string[];
}

class VideoAddController {

    loading: boolean = false;
    videoData: IVideoData = {
        tags: []
    };
    initialData: IVideoData;
    keys: any[];
    editId: number;
    sendform: boolean = false;
    videos: IYoutubeItem[];

    /** @ngInject */
    constructor(private VideoAddService: VideoAddService,
                private $mdConstant: any,
                private $window: ng.IWindowService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {
        this.$rootScope.$watch(() => this.videos, (videos: IYoutubeItem[]) => {
            if (videos && videos.length) {
                this.videoData.title = videos[0].title;
                this.videoData.description = videos[0].description;
                this.videoData.video_id = videos[0].youtubeId;
            } else {
                this.videoData.title = '';
                this.videoData.description = '';
                this.videoData.video_id = '';
            }

        }, true);

        this.$timeout(() => {
            this.initialData = angular.copy(this.videoData);
        });

        this.$window.onbeforeunload = event => {
            if (!angular.equals(this.videoData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };


        if (this.editId) {
            this.VideoAddService.getItem(this.editId).then((item: IVideoData) => {
                this.videoData = item;
                this.videos[0] = {
                    youtubeId: item.video_id,
                    title: item.title,
                    description: item.description
                };
                this.initialData = angular.copy(this.videoData);
            })
            .catch(() => {
                this.$window.location.href = '/';
            })
        }

        // working with commas when user does copy/paste or when user writes keywords with commas
/*        this.$rootScope.$watch(() => this.videoData.tags, (tags) => {
            let res = [];
            if (tags.length) {
                tags.forEach((tag: string) => {
                    res = res.concat(tag.split(',').map(tag => tag.trim())).filter(tag => tag != '');
                });
            }
            this.videoData.tags = _.uniq(res);
        }, true);*/

    }

    sendForm(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.videoData);
        let action = this.editId ? this.VideoAddService.edit(this.videoData) : this.VideoAddService.add(this.videoData);
        action
        .then(url => {
            this.$window.location.href = url;
        })
        .catch((err: any) => {
            console.log(err);
            this.loading = false;
            (this.$rootScope as any).loading = true;
        });
    }

}

export default VideoAddController;
