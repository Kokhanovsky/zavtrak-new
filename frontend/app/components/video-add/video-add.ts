import * as angular from "angular";
import videoAddComponentOptions from './video-add.component';
import { VideoAddService } from './video-add.service';

let videoAddModule: ng.IModule = angular.module('videoAddModule', []);

videoAddModule.component('videoAdd', videoAddComponentOptions);
videoAddModule.service('VideoAddService', VideoAddService);

let videoAddModuleName: string = videoAddModule.name;

export default videoAddModuleName;


