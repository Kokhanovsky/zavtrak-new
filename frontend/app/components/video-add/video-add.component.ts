import VideoAddController from './video-add.controller';

let videoAddComponent: ng.IComponentOptions = {
    bindings: {
        editId: '<'
    },
    template: require('./video-add.html'),
    controller:  VideoAddController
};

export default videoAddComponent;
