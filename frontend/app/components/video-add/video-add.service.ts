import IPromise = angular.IPromise;
import { IVideoData } from "./video-add.controller";

export class VideoAddService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(videoData: IVideoData) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/video',
                data: videoData
            }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/video',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    getItem(id: number) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/video/edit',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
