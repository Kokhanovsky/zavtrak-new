import * as angular from 'angular';

import recipeCardComponentOptions from './recipe-card.component';

let recipeCardModule: ng.IModule = angular.module('recipeCard', []);

recipeCardModule.component('recipeCard', recipeCardComponentOptions);

let recipeCardModuleName: string = recipeCardModule.name;

export default recipeCardModuleName;


