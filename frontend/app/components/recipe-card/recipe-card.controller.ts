import * as angular from "angular";
import IHttpService = angular.IHttpService;


interface IUser {
    name: string;
    isOnline: boolean;
    avatarSrc: string;
    profileUrl: string;
}

interface IRecipe {
    title: string;
    rating: number;
    recipeNumber: number;
    slug: string;
    user: IUser;
    url: string;
}


class RecipeCardController {

    recipe: IRecipe;

    /** @ngInject */
    constructor() {

    }

    $onInit() {
    }

}

export default RecipeCardController;
