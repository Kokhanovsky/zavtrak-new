import RecipeCardController from './recipe-card.controller';
import './recipe-card.sass';

let recipeCardComponent: ng.IComponentOptions = {
    bindings: {
        recipe: '<'
    },
    template: require('./recipe-card.html'),
    controller:  RecipeCardController
};

export default recipeCardComponent;
