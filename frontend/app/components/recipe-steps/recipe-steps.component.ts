import RecipeStepsController from './recipe-steps.controller';
import './recipe-steps.sass';

let recipeStepsComponent: ng.IComponentOptions = {
    require: {
        model: 'ngModel'
    },
    bindings: {
        editId: '<',
    },
    template: require('./recipe-steps.html'),
    controller:  RecipeStepsController
};

export default recipeStepsComponent;
