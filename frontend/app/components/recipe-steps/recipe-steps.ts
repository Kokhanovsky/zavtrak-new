import * as angular from "angular";
import recipeStepsComponentOptions from './recipe-steps.component';
import stepComponentOptions from './step.component';

let recipeStepsModule: ng.IModule = angular.module('recipeStepsModule', []);

recipeStepsModule.component('recipeSteps', recipeStepsComponentOptions);
recipeStepsModule.component('step', stepComponentOptions);

let recipeStepsModuleName: string = recipeStepsModule.name;

export default recipeStepsModuleName;


