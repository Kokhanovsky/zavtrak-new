import { IRecipeStep } from '../../../../backend/models/recipe';
import RecipeStepsController from './recipe-steps.controller';
import * as angular from 'angular';

class StepController {

    steps: IRecipeStep[] = [];

    step: IRecipeStep;

    parentController: RecipeStepsController;

    /** @ngInject */
    constructor(private $mdDialog: any) {

    }

    removeStep(stepNum) {
        if (this.isEmptyStep()) {
            this.steps = this.steps.splice(stepNum, 1);
            return;
        }
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Шаг номер ' + (stepNum + 1) + ' содержит данные, удалить?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            this.steps = this.steps.splice(stepNum, 1);
        }, () => {
            console.log('Cancel');
        });
    }

    isEmptyStep() {
        return !((this.step.ingredients && this.step.ingredients.length) || (this.step.photos && this.step.photos.length) || this.step.description);
    }

}

export default StepController;
