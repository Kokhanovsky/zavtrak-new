import './recipe-steps.sass';
import StepController from './step.controller';

let StepComponent: ng.IComponentOptions = {
    bindings: {
        stepNum: '<',
        steps: '=',
        step: '=',
        parentController: '='
    },
    template: require('./step.html'),
    controller: StepController
};

export default StepComponent;
