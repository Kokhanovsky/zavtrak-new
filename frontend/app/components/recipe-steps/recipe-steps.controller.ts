import * as angular from 'angular';
import * as _ from 'underscore';
import { IRecipeStep } from '../../../../backend/models/recipe';
import ITimeoutService = angular.ITimeoutService;

class RecipeStepsController {

    public model: ng.INgModelController;
    steps: IRecipeStep[] = [];

    /** @ngInject */
    constructor(private $document: ng.IDocumentService,
                private $scope: ng.IScope,
                private $timeout: ITimeoutService) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$scope.$watch(() => this.model.$viewValue, vv => {
                if (_.isObject(vv)) {
                    this.steps = this.model.$viewValue;
                }
            });
        }).then(() => {
            this.$scope.$watch(() => this.steps, (steps: any[]) => this.model.$setViewValue(steps), true);
        });
    }

    addStep(index: number) {
        let step: any = {};
        this.steps.splice(index, 0, step);
        this.$timeout(() => {
            let stepElement = angular.element(document.getElementById('step' + index));
            (this.$document as any).scrollToElementAnimated(stepElement, 10, 800, t => 1-(--t)*t*t*t);
        }, 200)
    }

}

export default RecipeStepsController;
