import AccountController from './account.controller';
import './account.sass';

let accountComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./account.html'),
    controller:  AccountController
};

export default accountComponent;
