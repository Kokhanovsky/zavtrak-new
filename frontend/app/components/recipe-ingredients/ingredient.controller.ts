import { IngredientService } from './ingredient.service';
import { IRecipeIngredient } from '../../../../backend/models/recipe';

export interface IIngredientValue {
    food?: { _id: string, title: string },
    measure?: { _id: string, title: string },
    quantity?: number
}

class IngredientController {

    value: IIngredientValue;
    ingredients: IRecipeIngredient[] = [];

    /** @ngInject */
    constructor(private IngredientService: IngredientService) {

    }

    searchFoods(food: string) {
        return this.IngredientService.searchFoods(food);
    }

    addFood(food: string) {
        if (!food) return;
        this.IngredientService.addFood(food).then(food => this.value.food = food);
    }

    searchMeasures(measure: string) {
        return this.IngredientService.searchMeasures(measure);
    }

    addMeasure(measure: string) {
        if (!measure) return;
        this.IngredientService.addMeasure(measure).then(m => this.value.measure = m);
    }

    removeIngredient(ingredientNum) {
        this.ingredients = this.ingredients.splice(ingredientNum, 1);
    }

}

export default IngredientController;
