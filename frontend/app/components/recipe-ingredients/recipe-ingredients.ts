import * as angular from "angular";
import recipeIngredientsComponentOptions from './recipe-ingredients.component';
import ingredientComponentOptions from './ingredient.component';
import { IngredientService } from './ingredient.service';

let recipeIngredientsModule: ng.IModule = angular.module('recipeIngredientsModule', []);

recipeIngredientsModule.component('recipeIngredients', recipeIngredientsComponentOptions);
recipeIngredientsModule.component('ingredient', ingredientComponentOptions);
recipeIngredientsModule.service('IngredientService', IngredientService);

let recipeIngredientsModuleName: string = recipeIngredientsModule.name;

export default recipeIngredientsModuleName;


