import RecipeIngredientsController from './recipe-ingredients.controller';
import './recipe-ingredients.sass';

let recipeIngredientsComponent: ng.IComponentOptions = {
    require: {
        model: 'ngModel'
    },
    template: require('./recipe-ingredients.html'),
    controller:  RecipeIngredientsController
};

export default recipeIngredientsComponent;
