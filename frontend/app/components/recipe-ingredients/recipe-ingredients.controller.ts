import * as _ from 'underscore';
import { IIngredientValue } from './ingredient.controller';

class RecipeIngredientsController {

    public model: ng.INgModelController;
    ingredients: IIngredientValue[] = [];

    /** @ngInject */
    constructor(private $scope: ng.IScope, private $timeout: ng.ITimeoutService) {

    }

    $onInit() {
        this.$timeout(() => {
            this.$scope.$watch(() => this.model.$viewValue, vv => {
                if (_.isObject(vv)) {
                    this.ingredients = this.model.$viewValue;
                }
            });

        }).then(() => {
            this.$scope.$watch(() => this.ingredients, (ingredients: IIngredientValue[]) => {
                //this.model.$setViewValue(ingredients.filter((ingredient: IIngredientValue) => !(!ingredient.food && !ingredient.measure && !ingredient.quantity)));
                this.model.$setViewValue(ingredients);
            }, true);
        });
    }

    addIngredient() {
        let ingr: any = {};
        this.ingredients.push(ingr);
    }

}

export default RecipeIngredientsController;
