import IPromise = angular.IPromise;

export class IngredientService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    searchFoods(food: string): IPromise<any> {
        return this.$http({
            method: 'GET',
            url: '/api/foods',
            params: { food }
        })
        .then((response: any): any => {
            return response.data;
        });
    }

    searchMeasures(measure: string): IPromise<any> {
        return this.$http({
            method: 'GET',
            url: '/api/measures',
            params: { measure }
        })
        .then((response: any): any => {
            return response.data;
        });
    }

    addFood(food: string): IPromise<any> {
        return this.$http({
                method: 'POST',
                url: '/api/foods',
                data: { food }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    addMeasure(measure: string): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/measures',
                data: { measure }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
