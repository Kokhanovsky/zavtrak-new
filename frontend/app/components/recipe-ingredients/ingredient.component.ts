import IngredientController from './ingredient.controller';

import './recipe-ingredients.sass';

let IngredientComponent: ng.IComponentOptions = {
    bindings: {
        ingredientNum: '<',
        ingredients: '=',
        value: '='
    },
    template: require('./ingredient.html'),
    controller: IngredientController
};

export default IngredientComponent;
