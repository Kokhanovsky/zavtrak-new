export class VideoEditService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getTitle(youtubeId: string) {
        return this.$http({
            url: 'https://www.googleapis.com/youtube/v3/videos',
            method: 'GET',
            params: {
                id: youtubeId,
                key: 'AIzaSyCF96bFM8Y2GDqEc2rEKxcZk3dLk2fCdts',
                part: 'snippet',
                fields: 'items(snippet(title, description))'
            }
        }).then((res: any) => {
            let ytResult: any;
            if (res.data && res.data.items && res.data.items.length) {
                ytResult = {
                    title: res.data.items["0"].snippet.title,
                    description: res.data.items["0"].snippet.description
                };
                return ytResult;
            } else {
                return null;
            }
        });
    }

}
