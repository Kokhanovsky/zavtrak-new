import * as angular from 'angular';
import videoEditComponentOptions from './video-edit.component';
import { VideoEditService } from './video-edit.service';

let videoEditModule: ng.IModule = angular.module('videoEditModule', []);

videoEditModule.component('videoEdit', videoEditComponentOptions);
videoEditModule.service('VideoEditService', VideoEditService);
let videoEditModuleName: string = videoEditModule.name;

export default videoEditModuleName;
