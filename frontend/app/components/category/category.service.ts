import IPromise = angular.IPromise;

export class CategoryService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getCategories(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/categories'
            })
            .then((response: any): any => {
                return response.data;
            });
    }
}
