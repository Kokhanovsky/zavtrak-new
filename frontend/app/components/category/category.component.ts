import CategoryController from './category.controller';
import './category.sass';

let categoryComponent: angular.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    template: require('./category.html'),
    controller:  CategoryController
};

export default categoryComponent;
