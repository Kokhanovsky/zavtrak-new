import { CategoryService } from './category.service';
import * as angular from "angular";
import * as _ from 'underscore';

interface ICategory {
    _id: string;
    id: string;
    parentId: string;
    title: string;
    selected: boolean;
    show: boolean;
    children?: ICategory[];
}

class CategoryController {

    public model: ng.INgModelController;
    public categories: ICategory[];
    public selectedCategories: ICategory[] = [];
    public val: string[];
    public required: boolean;


    /** @ngInject */
    constructor(private CategoryService: CategoryService, private $scope: ng.IScope, private $mdDialog: any, $timeout: ng.ITimeoutService) {

        $timeout(() => {
            CategoryService.getCategories().then((categories: ICategory[]) => {
                this.categories = categories;
                this.updateControl(this.model.$viewValue);
                this.openNodes(this.categories);
            });
            $scope.$watch(() => this.categories, (categories: ICategory[]) => {
                if (categories) {
                    this.getValue();
                }
            }, true);
        });

    }

    selectCategories() {
        this.$mdDialog.show({
            locals: {categories: this.categories},
            controller: SelectCategoriesController,
            controllerAs: '$ctrl',
            template: require('./category-popup.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((categories: ICategory[]) => {
            this.selectedCategories = categories;
        }, () => {
            console.log('cancel');
        });
    }

    removeFromSelected(node: ICategory) {
        node.selected = false;
    }

    getValue() {
        this.selectedCategories = [];
        this.scanNodes(this.categories);
        let val = this.selectedCategories.map(c => c._id);
        this.model.$setViewValue(val);
        this.model.$setValidity("required", !!val.length);
    }

    private updateControl(ids: string[]) {
        this.scanNodes(this.categories, ids);
    }

    private scanNodes(nodes: ICategory[], ids: string[] = []) {
        nodes.forEach(node => {
            if (_.contains(ids, node._id)) {
                node.selected = true;
            }
            if (node.selected) {
                this.selectedCategories.push(node);
            }
            if (node.children) {
                this.scanNodes(node.children, ids);
            }
        });
    }

    private openNodes(nodes: ICategory[]) {
        nodes.forEach(node => {
            let parentNode = this.getCategoryById(node.parentId);
            if (parentNode && node.selected) {
                parentNode.show = true;
            }
            if (node.children) {
                this.openNodes(node.children);
            }
        });
    }

    private getCategoryById(id: string) {
        let cat: ICategory = null;
        goThrough(this.categories);
        function goThrough(node: ICategory[]) {
            node.forEach((node: ICategory) => {
                if (node._id === id) {
                    cat = node;
                    return;
                }
                if (node.children) {
                    goThrough(node.children);
                }
            });
        }
        return cat;
    }


}

class SelectCategoriesController {

    /** @ngInject */
    constructor(private $mdDialog: any, private categories: ICategory[]) {
    }


    cancel() {
        this.$mdDialog.cancel();
    }

}

export default CategoryController;
