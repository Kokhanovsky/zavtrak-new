let categoryNodeComponent: angular.IComponentOptions = {
    bindings: {
        nodes: '='
    },
    template: require('./category-node.html')
};

export default categoryNodeComponent;
