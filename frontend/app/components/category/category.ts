import * as angular from "angular";
import categoryComponentOptions from './category.component';
import categoryNodeComponentOptions from './category-node.component';
import { CategoryService } from './category.service';

let categoryModule: ng.IModule = angular.module('categoryModule', []);

categoryModule.component('category', categoryComponentOptions);
categoryModule.component('categoryNode', categoryNodeComponentOptions);
categoryModule.service('CategoryService', CategoryService);

let categoryModuleName: string = categoryModule.name;

export default categoryModuleName;

