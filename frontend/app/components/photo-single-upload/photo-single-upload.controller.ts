import { PhotoSingleUploadService } from './photo-single-upload.service';

class PhotoSingleUploadController {

    public model: ng.INgModelController;
    endpoint: string;
    folder: string;
    imageUrl: string;
    showLoading: boolean = false;
    showDropButton: boolean = true;
    showDropBox: boolean;
    progress: number = 0;
    cancelUploading: any;
    maxSize: string = '50MB';
    file: string;
    editMode: boolean = false;

    /** @ngInject */
    constructor(private PhotoSingleUploadService: PhotoSingleUploadService,
                private $mdDialog: any,
                private $timeout: ng.ITimeoutService,
                private toastr: any,
                private $scope: ng.IScope) {
        $timeout(() => PhotoSingleUploadService.endpoint = this.endpoint);
        $scope.$watch(() => this.model.$viewValue, (val: string) => {
            if (val) {
                this.showDropBox = false;
                this.getImageUrl(val);
            } else {
                this.showDropBox = true;
            }
        });
    }


    upload(files: any) {
        if (!files.length) {
            //this.showError('Неправильный формат изображения или размер изображения слишком большой');
            return;
        }
        this.progress = 0;
        this.$timeout(() => this.showLoading = true);
        this.showDropButton = false;

        let isAborted = false;
        this.PhotoSingleUploadService.upload(files)
            .xhr((xhr: any) => {
                this.cancelUploading = () => {
                    xhr.abort();
                     isAborted = true;
                };
            })
            .progress((data: angular.angularFileUpload.IFileProgressEvent) => {
                this.progress = Math.round((data.loaded / data.total) * 100);
            })

            .then((response: any) => {
                let file: string = response.data.hash;
                this.showDropBox = false;
                this.$timeout(() => {
                    this.getImageUrl(file);
                    this.model.$setViewValue(file);
                });
            })

            .catch(err => {
                if (isAborted) {
                    console.log('Загрузка файла прервана');
                    return;
                }
                this.toastr.error(`Ошибка загрузки`);
            })

            .then(() => {
                this.showDropButton = true;
                this.showLoading = false;
            });
    }

    remove(file: string) {
        if (!this.editMode) {
            this.PhotoSingleUploadService.remove(this.file).then(() => console.log('file removed'));
        }
        this.file = null;
        this.$timeout(() => this.showDropBox = true);
        this.model.$setViewValue(null);
    }

    private getImageUrl(hash: string) {
        this.file = hash;
        this.imageUrl = '/storage/cache/' + (this.folder ? this.folder + '/' : '') + hash + '/200x200.jpg';
        return this.imageUrl;
    }

    private showError(message: string) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Ошибка загрузки фото')
            .textContent(message)
            .ok('ok'));
        return;
    }

    private showHint(message: string) {
        this.$mdDialog.show(
            this.$mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Подсказка')
            .textContent(message)
            .ok('ok'));
        return;
    }

}

export default PhotoSingleUploadController;
