import * as angular from "angular";
import photoSingleUploadComponentOptions from './photo-single-upload.component';
import { PhotoSingleUploadService } from './photo-single-upload.service';

let photoSingleUploadModule: ng.IModule = angular.module('photoSingleUploadUploadModule', []);

photoSingleUploadModule.component('photoSingleUpload', photoSingleUploadComponentOptions);
photoSingleUploadModule.service('PhotoSingleUploadService', PhotoSingleUploadService);

let photoSingleUploadModuleName: string = photoSingleUploadModule.name;

export default photoSingleUploadModuleName;
