import IPromise = angular.IPromise;

export class PhotoSingleUploadService {

    endpoint: string;

    /** @ngInject */
    constructor(private Upload: any, private $http: angular.IHttpService) {
    }

    upload(file: Array<angular.angularFileUpload.IFileUploadConfigFile>): angular.angularFileUpload.IUploadPromise<any> {
        return this.Upload.upload({
            url: this.endpoint,
            arrayKey: '',
            data: { file },
            method: 'POST'
        });
    }

    remove(file: string) {
        return this.$http({
            method: 'DELETE',
            url: this.endpoint,
            data: { file }
        });
    }
}
