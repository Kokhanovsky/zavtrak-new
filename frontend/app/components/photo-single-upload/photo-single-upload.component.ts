import PhotoController from './photo-single-upload.controller';
import './photo-single-upload.sass';

let PhotoComponent: ng.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    bindings: {
        endpoint: "<",
        folder: "<",
        editMode: "<",
        loading: "="
    },
    template: require('./photo-single-upload.html'),
    controller:  PhotoController
};

export default PhotoComponent;
