import ItemsListController from './items-list.controller';
import './items-list.sass';

let categoryComponent: angular.IComponentOptions = {
    bindings: {
        items: '<',
        perPage: '<',
        total: '<',
        page: '=',
        query: '=',
        deleteItem: '&'
    },
    template: require('./items-list.html'),
    controller:  ItemsListController
};

export default categoryComponent;
