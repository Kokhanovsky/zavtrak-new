import './items-list.sass';

let listItem: angular.IComponentOptions = {
    bindings: {
        item: '<',
        deleteItem: '&'
    },
    template: require('./list-item.html')
};

export default listItem;
