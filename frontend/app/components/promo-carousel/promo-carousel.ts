import * as angular from "angular";
import promoCarouselComponentOptions from './promo-carousel.component';
import { PromoCarouselService } from './promo-carousel.service';

let promoCarouselModule: ng.IModule = angular.module('promoCarouselModule', []);

promoCarouselModule.component('promoCarousel', promoCarouselComponentOptions);
promoCarouselModule.service('PromoCarouselService', PromoCarouselService);

let promoCarouselModuleName: string = promoCarouselModule.name;

export default promoCarouselModuleName;
