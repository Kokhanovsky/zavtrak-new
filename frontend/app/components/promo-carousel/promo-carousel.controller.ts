import { PromoCarouselService } from './promo-carousel.service';
import { IPromo } from '../../../../backend/models/promo';

class PromoCarouselController {

    promos: IPromo[];

    href: string;
    activeSlide: number = 0;
    autoplay: boolean = true;
    interval: any;
    promoShow: boolean = false;

    /** @ngInject */
    constructor(private PromoCarouselService: PromoCarouselService, private $window: ng.IWindowService) {
        this.promos = PromoCarouselService.getPromo();
        this.promoShow = true;
    }

    next(inf?: boolean) {
        if (this.activeSlide < this.promos.length - 1) {
            this.activeSlide = this.activeSlide + 1;
        } else if (inf) {
            this.activeSlide = 0;
        }
    }

    prev() {
        if (this.activeSlide > 0) {
            this.activeSlide = this.activeSlide - 1
        }
    }

    go($event, url) {
        $event.stopPropagation();
        if (!url) {
            return;
        }
        this.$window.location.href = url;
    }
}

export default PromoCarouselController;
