import PromoCarouselController from './promo-carousel.controller';
import './promo-carousel.sass';

let promoCarouselComponent: ng.IComponentOptions = {
    bindings: {
    },
    template: require('./promo-carousel.html'),
    controller:  PromoCarouselController
};

export default promoCarouselComponent;
