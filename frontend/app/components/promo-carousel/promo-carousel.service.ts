import * as angular from "angular";

export class PromoCarouselService {

    /** @ngInject */
    constructor(private $window: angular.IWindowService) {
    }

    getPromo() {
        let window: any = this.$window;
        if (!window.promo)
            return;
        return window.promo;
    }
}
