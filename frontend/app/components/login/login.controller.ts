import { LoginService } from './login.service';

export interface IUserLogin {
    email: string;
    password: string;
    remember: boolean;
}

class LoginController {

    user: IUserLogin;
    loginRedir: string;
    showComponent: boolean;
    wrongCredentials: boolean = false;
    loading: boolean = false;
    passwordInputType: string = 'password';

    /** @ngInject */
    constructor(private $window: ng.IWindowService,
                private LoginService: LoginService,
                private $scope: ng.IScope) {

        $scope.$watch(() => this.user, () => {
            this.wrongCredentials = false;
        }, true);

    }

    $onInit() {
        this.loginRedir = this.loginRedir ? this.loginRedir : '/profile';
    }

    changeInputPassType() {
        this.passwordInputType = this.passwordInputType === 'password' ? 'text' : 'password';
    }

    login() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.LoginService.login(this.user)
            .then(() => {
                // if success then redirect to the user account
                this.$window.location.href = this.loginRedir;
            })
            .catch(() => {
                this.wrongCredentials = true;
            })
            .then(() => {
                this.loading = false;
            });
    }

}

export default LoginController;
