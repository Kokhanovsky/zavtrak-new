class LoginPopupController {

    serverError: string;
    loading: boolean = false;
    success: boolean = false;
    widgetId: string;
    captchaResponse: string;
    response: any;

    /** @ngInject */
    constructor(private redirUrl: string,
                private vcRecaptchaService: any,
                private recaptchaSiteKey: string,
                private $mdDialog: any,
                $ocLazyLoad: any) {
        $ocLazyLoad.load('https://www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded');

    }

    cancel() {
        this.$mdDialog.cancel();
    }

    setResponse(response: string) {
        this.captchaResponse = response;
        console.info('Captcha response available');
    }

    setWidgetId(widgetId: string) {
        console.info('Created widget ID: %s', widgetId);
        this.widgetId = widgetId;
    }

    cbExpiration() {
        console.info('Captcha expired. Resetting response object');
        this.vcRecaptchaService.reload(this.widgetId);
        this.captchaResponse = null;
    }
}

export default LoginPopupController;
