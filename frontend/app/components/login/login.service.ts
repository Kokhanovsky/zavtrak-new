import { IUserLogin } from './login.controller';
import IPromise = angular.IPromise;
import * as angular from "angular";
import LoginPopupController from './login-popup.controller';

export class LoginService {

    recaptchaSiteKey = "6LdIEQ4UAAAAACOx2d8YteYOg3EUdE3PaNYrTf-Z";

    /** @ngInject */
    constructor(private $http: angular.IHttpService,
                private $mdDialog: any) {
    }

    login(loginData: IUserLogin): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/login',
                data: loginData
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    showLogin(redirUrl: string) {
        this.$mdDialog.show({
            locals: { redirUrl, recaptchaSiteKey: this.recaptchaSiteKey },
            controller: LoginPopupController,
            controllerAs: '$ctrl',
            template: require('./login-popup.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then(() => {
           // this.user = user;
        }, () => {
            console.log('cancel');
        });
    }
}
