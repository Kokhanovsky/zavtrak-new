import StarsController from './stars.controller';
import './stars.sass';

let starsComponent: ng.IComponentOptions = {
    bindings: {
        rating: '<',
        item: '<',
        itemType: '<',
        ownRating: '='
    },
    template: require('./stars.html'),
    controller:  StarsController
};

export default starsComponent;
