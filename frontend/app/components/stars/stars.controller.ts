import * as angular from "angular";
import IHttpService = angular.IHttpService;
import ITimeoutService = angular.ITimeoutService;
import { StarsService } from './stars.service';
import { LoginService } from '../login/login.service';

interface IStar {
    type: string;
    hover: boolean;
    own: boolean;
    set: boolean;
}

class StarsController {

    stars: IStar[] = [];
    starsCount: number = 5;
    rating: number;
    ownRating: number;
    item: string;
    itemType: string;

    /** @ngInject */
    constructor(private $timeout: ng.ITimeoutService,
                private $location: ng.ILocationService,
                private $scope: ng.IScope,
                private LoginService: LoginService,
                private StarsService: StarsService) {
    }

    $onInit() {
        for (let i = 0; i < this.starsCount; i++) {
            this.stars.push({ type: 'star-border', hover: false, set: false, own: false });
        }
        this.stars.forEach((star, index, stars) => {
            if (this.rating >= (index + 1)) {
                stars[index].type = "star";
            } else {
                let afterPoint = this.rating - Math.floor(this.rating);
                if (Math.floor(this.rating) === index) {
                    if (afterPoint >= 0.25)
                        stars[index].type = "star-half";
                    if (afterPoint >= 0.75)
                        stars[index].type = "star";
                }

            }
        });

        this.$scope.$watch(() => this.ownRating, rating => this.drawOwnRating());
    }

    drawOwnRating() {
        this.stars.forEach((star, index, stars) => {
            stars[index].own = this.ownRating >= (index + 1);
        });
    }

    mouseEnter(starIndex: number) {
        for (let i = 0; i < this.starsCount; i++) {
            this.stars[i].hover = (i <= starIndex);
        }
    }

    mouseLeave(starIndex: number) {
        for (let i = 0; i < this.starsCount; i++) {
            this.stars[i].hover = false;
        }
    }

    setRating(starIndex: number) {
        let rating = starIndex + 1;
        for (let i = 0; i < this.starsCount; i++) {
            this.stars[i].set = (i <= starIndex);
            this.$timeout(() => this.stars[i].set = false, 200);
        }
        this.StarsService.setRating(rating, this.item, this.itemType).then(() => {
            this.ownRating = rating;
        })
        .catch((err) => {
            if (err.status = 401) {
                this.LoginService.showLogin(this.$location.url());
            }
        });
    }

}

export default StarsController;
