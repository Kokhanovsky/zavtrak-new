import * as angular from "angular";

export class StarsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    setRating(rating: number, item: string, itemType: string) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/rating',
                data: { rating, item, itemType }
            }).then((res: any) => res.data.url);
    }

}
