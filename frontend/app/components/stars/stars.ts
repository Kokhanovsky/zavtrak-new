import * as angular from 'angular';

import starsComponentOptions from './stars.component';
import { StarsService } from './stars.service';

let starsModule: ng.IModule = angular.module('stars', []);

starsModule.component('stars', starsComponentOptions);
starsModule.service('StarsService', StarsService);

let starsModuleName: string = starsModule.name;

export default starsModuleName;


