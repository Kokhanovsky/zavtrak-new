import * as angular from "angular";
import HomeController from './home.controller';
import './home.sass';

let HomeModule: ng.IModule = angular.module('homeModule', []);

HomeModule.controller('home', HomeController);

let homeModuleName: string = HomeModule.name;

export default homeModuleName;
