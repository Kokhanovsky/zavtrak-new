import * as angular from "angular";

class HomeController {

    itemWidth: string;
    itemHeight: string;
    city: string;
    mapPoints: any[];

    /** @ngInject */
    constructor(private $timeout: ng.ITimeoutService, private $mdMedia: any, $scope: ng.IScope, $window: ng.IWindowService) {

        this.calcItemSizes();

        angular.element($window).bind('resize', () => {
            this.calcItemSizes();
            $scope.$apply();
        });

    }

    private calcItemSizes() {
        let width = (this.$mdMedia('xs') ? 280 : this.$mdMedia('sm') ? 225 : this.$mdMedia('md') ? 170 : 160);
        this.itemWidth = width + 'px';
        this.itemHeight = width * 3/4 + 'px';
    }


}

export default HomeController;
