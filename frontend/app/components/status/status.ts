import * as angular from 'angular';
import statusComponentOptions from './status.component';
import { StatusService } from './status.service';

let statusModule: ng.IModule = angular.module('statusModule', []);

statusModule.component('status', statusComponentOptions);
statusModule.service('StatusService', StatusService);

let statusModuleName: string = statusModule.name;

export default statusModuleName;


