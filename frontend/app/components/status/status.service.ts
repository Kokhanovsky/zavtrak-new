import * as angular from 'angular';

import IPromise = angular.IPromise;


export class StatusService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService, private $rootScope: ng.IRootScopeService) {
    }


    setStatus(itemId: string, type: string, status: string) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/moderate/status',
                data: { itemId, type, status }
            })
        .then(() => {
            this.$rootScope.$emit('status.changed');
            console.log('status changed');
        });
    }

}
