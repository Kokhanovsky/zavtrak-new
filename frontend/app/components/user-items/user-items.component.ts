import './user-items.sass';
import UserItemsController from './user-items.controller';

let userItemsComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./user-items.html'),
    controller: UserItemsController
};

export default userItemsComponent;
