import * as angular from "angular";

import userItemsComponentOptions from './user-items.component';

import routerConfig from './user-items.config';

let userItemsModule: ng.IModule = angular.module('userItemsModule', []);

userItemsModule.config(routerConfig);
userItemsModule.component('userItems', userItemsComponentOptions);


// COUNTS
import { UserItemsService } from './user-items.service';
import UserCountsController from './user-items.controller';

userItemsModule.service('UserItemsService', UserItemsService);
userItemsModule.controller('UserCountsController', UserCountsController);


// RECIPES
import userRecipesComponentOptions from './recipes/user-recipes.component';
import { UserRecipesService } from './recipes/user-recipes.service';
import UserRecipesController from './recipes/user-recipes.controller';

userItemsModule.service('UserRecipesService', UserRecipesService);
userItemsModule.controller('UserRecipesController', UserRecipesController);
userItemsModule.component('userRecipes', userRecipesComponentOptions);


// ARTICLES
import userArticlesComponentOptions from './articles/user-articles.component';
import { UserArticlesService } from './articles/user-articles.service';
import UserArticlesController from './articles/user-articles.controller';

userItemsModule.service('UserArticlesService', UserArticlesService);
userItemsModule.controller('UserArticlesController', UserArticlesController);
userItemsModule.component('userArticles', userArticlesComponentOptions);


// ADVICES
import userAdvicesComponentOptions from './advices/user-advices.component';
import { UserAdvicesService } from './advices/user-advices.service';
import UserAdvicesController from './advices/user-advices.controller';

userItemsModule.service('UserAdvicesService', UserAdvicesService);
userItemsModule.controller('UserAdvicesController', UserAdvicesController);
userItemsModule.component('userAdvices', userAdvicesComponentOptions);


let userItemsModuleName: string = userItemsModule.name;
export default userItemsModuleName;


