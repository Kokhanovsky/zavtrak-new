import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { Common } from '../../../../../backend/lib/common';
import { IAdvice } from '../../../../../backend/models/advice';

export class UserAdvicesService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getAdvices(page: number, query: string): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user-advices',
                params: { page, query }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((advice: IAdvice) => {
                let item: IItem = {
                    _id: advice._id,
                    title: Common.truncate(advice.content, 100),
                    url: Common.getAdviceUrl(advice),
                    editUrl: '/advice-add/' +  advice._id,
                    createdAt: Common.formatCreatedDate(advice.createdAt),
                    status: advice.status
                };
                return item;
            });
            return data;
        });
    }

    deleteAdvice(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/user-advices',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
