import { UserAdvicesService } from './user-advices.service';
import { IAdvice } from '../../../../../backend/models/advice';
import * as _ from 'underscore';

class UserAdvicesController {

    advices: IAdvice[] = [];
    _page: number = 1;
    perPage: number = 10;
    total: number;
    _query: string;

    set query(value: string) {
        this._query = value;
        this.page = 1;
        this.getAdvices();
    }

    get query() {
        return this._query;
    }

    set page(value: number) {
        this._page = value;
        this.getAdvices();
    }

    get page() {
        return this._page;
    }

    /** @ngInject */
    constructor(private UserAdvicesService: UserAdvicesService,
                private $rootScope: ng.IRootScopeService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.getAdvices();
    }

    getAdvices() {
        (this.$rootScope as any).loading = true;
        this.UserAdvicesService.getAdvices(this.page, this._query).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.advices = [];
                return;
            }
            this.advices = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading recipes') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    deleteAdvice(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить совет?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.advices = this.advices.filter(advice => (advice as any)._id !== id);
            this.UserAdvicesService.deleteAdvice(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }
}

export default UserAdvicesController;
