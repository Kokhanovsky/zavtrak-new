import UserAdvicesController from './user-advices.controller';
import './user-advices.sass';

let userAdvicesComponent: ng.IComponentOptions = {
    template: require('./user-advices.html'),
    controller:  UserAdvicesController
};

export default userAdvicesComponent;
