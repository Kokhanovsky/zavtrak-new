import { UserItemsService } from './user-items.service';
import { ICounts } from '../../../../backend/lib/interfaces';

class UserItemsController {

    counts: ICounts = {
        recipes: 0,
        articles: 0,
        advices: 0
    };

    /** @ngInject */
    constructor(private UserItemsService: UserItemsService) {
    }

    $onInit() {
        this.getCounts();
    }

    getCounts() {
        this.UserItemsService.getCounts().then((res: any) => {
            this.counts = res;
        });
    }

}

export default UserItemsController;
