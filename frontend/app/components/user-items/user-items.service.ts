import IPromise = angular.IPromise;

export class UserItemsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getCounts(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user-items/counts'
            })
        .then(result => result.data);
    }

}
