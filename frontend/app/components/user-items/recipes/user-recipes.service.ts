import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IRecipe } from '../../../../../backend/models/recipe';
import { Common } from '../../../../../backend/lib/common';


export class UserRecipesService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getRecipes(page: number, query: string): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user-recipes',
                params: { page, query }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((recipe: IRecipe) => {
                let item: IItem = {
                    _id: recipe._id,
                    title: recipe.title,
                    url: Common.getRecipeUrl(recipe),
                    editUrl: '/recipe-add/' +  recipe._id,
                    imageSrc: Common.getRecipeImgSrc(recipe),
                    createdAt: Common.formatCreatedDate(recipe.createdAt),
                    status: recipe.status
                };
                return item;
            });
            return data;
        });
    }

    deleteRecipe(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/user-recipes',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
