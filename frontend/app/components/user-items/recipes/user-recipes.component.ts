import UserRecipesController from './user-recipes.controller';
import './user-recipes.sass';

let userRecipesComponent: ng.IComponentOptions = {
    template: require('./user-recipes.html'),
    controller:  UserRecipesController
};

export default userRecipesComponent;
