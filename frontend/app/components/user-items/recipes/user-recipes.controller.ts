import * as angular from "angular";
import { UserRecipesService } from './user-recipes.service';
import { IRecipe } from '../../../../../backend/models/recipe';
import * as _ from 'underscore';
import { UserItemsService } from '../user-items.service';

class UserRecipesController {

    recipes: IRecipe[] = [];
    _page: number = 1;
    perPage: number = 10;
    total: number;
    _query: string;

    set query(value: string) {
        this._query = value;
        this.page = 1;
        this.getRecipes();
    }

    get query() {
        return this._query;
    }

    set page(value: number) {
        this._page = value;
        this.getRecipes();
    }

    get page() {
        return this._page;
    }

    /** @ngInject */
    constructor(private UserRecipesService: UserRecipesService,
                private UserItemsService: UserItemsService,
                private $rootScope: ng.IRootScopeService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.getRecipes();
    }

    getRecipes() {
        (this.$rootScope as any).loading = true;
        this.UserRecipesService.getRecipes(this.page, this._query).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.recipes = [];
                return;
            }
            this.recipes = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading recipes') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    deleteRecipe(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить рецепт?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.recipes = this.recipes.filter(recipe => (recipe as any)._id !== id);
            this.UserRecipesService.deleteRecipe(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

/*
    private highlightFilter(): any[] {
        if (!this._query) {
            return this.recipes;
        }
        let res = angular.copy(this.recipes);
        this.recipes = res.map((item: any) => {
            item.title = item.title.replace(new RegExp('('+this._query+')', 'gi'),
                '<span class="highlighted">$1</span>');
            return item;
        });
    }
*/


}

export default UserRecipesController;
