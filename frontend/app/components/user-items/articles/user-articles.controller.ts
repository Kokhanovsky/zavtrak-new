import { UserArticlesService } from './user-articles.service';
import { IArticle } from '../../../../../backend/models/article';
import * as _ from 'underscore';

class UserArticlesController {

    articles: IArticle[] = [];
    _page: number = 1;
    perPage: number = 10;
    total: number;
    _query: string;

    set query(value: string) {
        this._query = value;
        this.page = 1;
        this.getArticles();
    }

    get query() {
        return this._query;
    }

    set page(value: number) {
        this._page = value;
        this.getArticles();
    }

    get page() {
        return this._page;
    }

    /** @ngInject */
    constructor(private UserArticlesService: UserArticlesService,
                private $rootScope: ng.IRootScopeService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.getArticles();
    }

    getArticles() {
        (this.$rootScope as any).loading = true;
        this.UserArticlesService.getArticles(this.page, this._query).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.articles = [];
                return;
            }
            this.articles = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading articles') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    deleteArticle(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить статью?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.articles = this.articles.filter(article => (article as any)._id !== id);
            this.UserArticlesService.deleteArticle(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }
}

export default UserArticlesController;
