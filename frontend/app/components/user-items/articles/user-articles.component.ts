import UserArticlesController from './user-articles.controller';
import './user-articles.sass';

let userArticlesComponent: ng.IComponentOptions = {
    template: require('./user-articles.html'),
    controller:  UserArticlesController
};

export default userArticlesComponent;
