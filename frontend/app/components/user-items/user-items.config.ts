/** @ngInject */
export default function ($stateProvider: ng.ui.IStateProvider) {
    let recipes = {
        name: 'user-recipes',
        url: '/user-items/recipes',
        template: '<user-recipes></user-recipes>'
    };

    let articles = {
        name: 'user-articles',
        url: '/user-items/articles',
        template: '<user-articles></user-articles>'
    };

    let advices = {
        name: 'user-advices',
        url: '/user-items/advices',
        template: '<user-advices></user-advices>'
    };


    $stateProvider.state(recipes);
    $stateProvider.state(articles);
    $stateProvider.state(advices);
}
