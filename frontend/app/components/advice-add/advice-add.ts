import * as angular from "angular";
import adviceAddComponentOptions from './advice-add.component';
import { AdviceAddService } from './advice-add.service';

let adviceAddModule: ng.IModule = angular.module('adviceAddModule', []);

adviceAddModule.component('adviceAdd', adviceAddComponentOptions);
adviceAddModule.service('AdviceAddService', AdviceAddService);

let adviceAddModuleName: string = adviceAddModule.name;

export default adviceAddModuleName;


