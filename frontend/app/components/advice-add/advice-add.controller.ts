import { AdviceAddService } from './advice-add.service';
import * as angular from 'angular';
import { IYoutubeItem } from '../../../../backend/lib/interfaces';

export interface IAdviceData {
    title?: string;
    description?: string;
    advice_id?: string;
    tags?: string[];
    category?: string[];
}

class AdviceAddController {

    loading: boolean = false;
    adviceData: IAdviceData = {
        tags: []
    };
    initialData: IAdviceData;
    keys: any[];
    editId: string;
    sendform: boolean = false;
    advices: IYoutubeItem[];

    /** @ngInject */
    constructor(private AdviceAddService: AdviceAddService,
                private $mdConstant: any,
                private $window: ng.IWindowService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {

        this.$timeout(() => {
            this.initialData = angular.copy(this.adviceData);
        });

        this.$window.onbeforeunload = event => {
            if (!angular.equals(this.adviceData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };

        if (this.editId) {
            this.AdviceAddService.getItem(this.editId).then((item: IAdviceData) => {
                this.adviceData = item;
                this.initialData = angular.copy(this.adviceData);
            })
            .catch(() => {
                this.$window.location.href = '/';
            })
        }

    }

    sendForm(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.adviceData);
        let action = this.editId ? this.AdviceAddService.edit(this.adviceData) : this.AdviceAddService.add(this.adviceData);
        action
        .then(url => {
            this.$window.location.href = url;
        })
        .catch((err: any) => {
            console.log(err);
            this.loading = false;
            (this.$rootScope as any).loading = true;
        });
    }

}

export default AdviceAddController;
