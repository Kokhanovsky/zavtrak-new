import IPromise = angular.IPromise;
import { IAdviceData } from "./advice-add.controller";

export class AdviceAddService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(adviceData: IAdviceData) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/advice',
                data: adviceData
            }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/advice',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    getItem(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/advice/edit',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
