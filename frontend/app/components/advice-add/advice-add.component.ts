import AdviceAddController from './advice-add.controller';

let adviceAddComponent: ng.IComponentOptions = {
    bindings: {
        editId: '<'
    },
    template: require('./advice-add.html'),
    controller:  AdviceAddController
};

export default adviceAddComponent;
