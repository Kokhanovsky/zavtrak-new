import * as angular from "angular";

import passwordStrengthComponentOptions from './password-strength.component';

let passwordStrengthModule: ng.IModule = angular.module('passwordStrengthModule', []);

passwordStrengthModule.component('icPasswordStrength', passwordStrengthComponentOptions);

let passwordStrengthModuleName: string = passwordStrengthModule.name;

export default passwordStrengthModuleName;


