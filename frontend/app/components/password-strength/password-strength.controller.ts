class PasswordStrengthController {

    score: number;
    strength: number;
    password: string;

    /** @ngInject */
    constructor() {
    }

    set value(value: string) {
        this.password = value;
        this.score = this.scorePassword(value);
        this.strength = this.getStrength(this.score);
    }

    getStrength(score: number) {
        if (score > 80)
            return 3;
        if (score > 60)
            return 2;
        if (score >= 30)
            return 1;
        return 0;
    }

    scorePassword(pass: string): number {
        let score = 0;
        if (!pass)
            return score;

        // award every unique letter until 5 repetitions
        let letters: any = {};
        for (let i = 0; i < pass.length; i++) {
            letters[pass[i]] = (letters[pass[i]] || 0) + 1;
            score += 5.0 / letters[pass[i]];
        }

        // bonus points for mixing it up
        let letiations: any = {
            digits: /\d/.test(pass),
            lower: /[a-z]/.test(pass),
            upper: /[A-Z]/.test(pass),
            nonWords: /\W/.test(pass),
        };

        let letiationCount = 0;
        for (let check in letiations) {
            letiationCount += (letiations[check] == true) ? 1 : 0;
        }
        score += (letiationCount - 1) * 10;

        return score;
    }
}

export default PasswordStrengthController;
