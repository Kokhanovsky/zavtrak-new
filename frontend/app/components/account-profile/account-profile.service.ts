import * as angular from "angular";
import IPromise = angular.IPromise;
import IUserModel from './account-profile.controller';

export class AccountProfileService {

    /** @ngInject */
    constructor(private $http: ng.IHttpService) {
    }

    getUser() {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user'
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    putUser(user: IUserModel) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/user',
                data: user
            });
    }

    changePassword(currentPassword: string, newPassword: string) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/user/password/change',
                data: angular.copy({
                    currentPassword,
                    newPassword
                })
            });
    }

    changeEmail(email: string, password: string) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/user/email/change',
                data: angular.copy({
                    email,
                    password
                })
            });
    }

    addNickname(nickname: string) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/user/nickname/add',
                data: angular.copy({ nickname })
            });
    }

    deleteAvatar() {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/user/avatar/delete'
            });
    }

}
