import AccountProfileController from './account-profile.controller';
import './account-profile.sass';

let accountProfileComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./templates/account-profile.html'),
    controller:  AccountProfileController
};

export default accountProfileComponent;
