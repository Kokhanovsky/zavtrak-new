import * as angular from "angular";
import { CommonService } from '../../lib/common.service';
import { AccountProfileService } from './account-profile.service';
import { ICustomRootScope } from '../../app.run';

export interface IUserModel {
    id: string;
    nickname: string;
    firstname: string;
    lastname: string;
    email: string;
    verified: boolean;
    hasAvatar: boolean;
    role: string;
    host: string;
}

class AccountProfileController {

    user: IUserModel;
    editUser: IUserModel;
    edit: boolean = false;
    serverError: string;
    loading: boolean;
    emptyAvatar: string = '/images/avatar_empty.png';
    avatarImage: string;

    /** @ngInject */
    constructor(private $scope: ng.IScope,
                private $mdDialog: any,
                private AccountProfileService: AccountProfileService,
                private CommonService: CommonService) {
        this.getUser();
        this.avatarImage = this.emptyAvatar;
        // reset server error on model change
        $scope.$watch(() => this.editUser, () => {
            this.serverError = '';
        }, true);
    }

    getUser() {
        this.CommonService.getUserData().then((user: IUserModel) => {
            this.user = user;
            this.refreshAvatar();
        });
    }

    refreshAvatar() {
        if (this.user.hasAvatar) {
            this.avatarImage = '/avatar/user/' + this.user.id + '.jpg' + '?' + new Date().getTime();
        } else {
            this.avatarImage = this.emptyAvatar;
        }
    }

    changeName() {
        this.$mdDialog.show({
            locals: { user: this.user },
            controller: ChangeNameController,
            controllerAs: '$ctrl',
            template: require('./templates/change-name.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((user: IUserModel) => {
            this.user = user;
        }, () => {
            console.log('cancel');
        });
    }

    addNickname() {
        this.$mdDialog.show({
            locals: { user: this.user },
            controller: AddNicknameController,
            controllerAs: '$ctrl',
            template: require('./templates/add-nickname.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((user: IUserModel) => {
            this.user = user;
        }, () => {
            console.log('cancel');
        });
    }

    editAvatar() {
        this.$mdDialog.show({
            controller: ChangeAvatarController,
            controllerAs: '$ctrl',
            locals: { parentCtrl: this },
            template: require('./templates/change-avatar.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
    }

    deleteAvatar() {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Желаете удалить фото?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            this.AccountProfileService.deleteAvatar().then(() => {
                this.user.hasAvatar = false;
                //this.user.avatarSrc = null;
                this.refreshAvatar();
            });
        }, () => {
            console.log('Cancel');
        });
    }

    changePassword() {
        this.$mdDialog
        .show({
            controller: ChangePasswordController,
            controllerAs: '$ctrl',
            template: require('./templates/change-password.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then(() => {
            console.log('Success');
        }, () => {
            console.log('Cancel');
        });
    }

    changeEmail() {
        this.$mdDialog
        .show({
            controller: ChangeEmailController,
            controllerAs: '$ctrl',
            locals: { user: this.user },
            template: require('./templates/change-email.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((email: string) => {
            this.user.email = email;
        })
        .catch(() => {
            console.log('cancel');
        });
    }

}

class ChangeNameController {

    editUser: IUserModel;
    serverError: string;
    loading: boolean;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private user: IUserModel,
                private $scope: ng.IScope,
                private $rootScope: ICustomRootScope,
                private AccountProfileService: AccountProfileService) {
        this.editUser = angular.copy(user);
        $scope.$watch(() => (this.editUser), () => {
            this.serverError = null;
        }, true);
    }

    save() {
        this.loading = true;
        this.AccountProfileService.putUser(this.editUser as any).then(() => {
            this.$rootScope.user.username = [this.editUser.firstname, this.editUser.lastname].join(' ');
            this.$mdDialog.hide(this.editUser);
        })
        .catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Server error';
        })
        .then(() => {
            this.loading = false;
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

class ChangeAvatarController {
    image: string;
    hasAvatar: boolean;
    loading: boolean;
    serverError: string;
    avatarUpload: any;
    user: IUserModel;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private parentCtrl: AccountProfileController) {
        this.hasAvatar = parentCtrl.user.hasAvatar;
        this.user = parentCtrl.user;
        this.image = '/avatars/' + parentCtrl.user.id + '.jpg';
    }

    save() {
        this.avatarUpload.saveAvatar().then(() => {
            this.parentCtrl.user.hasAvatar = true;
            this.hasAvatar = true;
            this.parentCtrl.refreshAvatar();
            this.$mdDialog.hide();
        })
        .catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Server error';
        })
        .then(() => {
            this.loading = false;
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

class ChangePasswordController {

    currentPassword: string;
    newPassword: string;
    serverError: string;
    loading: boolean = false;
    success: boolean = false;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private $scope: ng.IScope,
                private AccountProfileService: AccountProfileService) {
        $scope.$watch(() => (this.currentPassword || this.newPassword), () => {
            this.serverError = null;
        }, true);
    }

    change() {
        this.loading = true;
        this.AccountProfileService.changePassword(this.currentPassword, this.newPassword).then(() => {
            this.success = true;
        })
        .catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Server error';
        })
        .then(() => {
            this.loading = false;
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

class ChangeEmailController {

    password: string;
    email: string;
    serverError: string;
    loading: boolean = false;
    success: boolean = false;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private $scope: ng.IScope,
                private user: IUserModel,
                private AccountProfileService: AccountProfileService) {

        $scope.$watch(() => (this.password || this.email), () => {
            this.serverError = null;
        }, true);

    }

    change() {
        this.loading = true;
        this.AccountProfileService.changeEmail(this.email, this.password).then(() => {
            this.success = true;
            this.user.firstname = this.email;
        })
        .catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Server error';
        })
        .then(() => {
            this.loading = false;
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

class AddNicknameController {

    nickname: string;
    serverError: string;
    loading: boolean = false;
    success: boolean = false;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private $scope: ng.IScope,
                private user: IUserModel,
                private AccountProfileService: AccountProfileService) {

        $scope.$watch(() => (this.nickname), () => {
            this.serverError = null;
        }, true);

    }

    add() {
        this.loading = true;
        this.AccountProfileService.addNickname(this.nickname).then(() => {
            this.success = true;
            this.user.nickname = this.nickname;
            this.$mdDialog.hide(this.user);
        })
        .catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Server error';
        })
        .then(() => {
            this.loading = false;
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AccountProfileController;
