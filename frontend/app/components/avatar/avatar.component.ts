import AvatarController from './avatar.controller';
import './avatar.sass';

let avatarComponent: ng.IComponentOptions = {
    bindings: {
        avatarUser: '<',
        avatarSize: '<',
        avatarHideName: '<'
    },
    template: require('./avatar.html'),
    controller:  AvatarController
};

export default avatarComponent;
