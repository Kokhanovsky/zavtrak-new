import * as angular from 'angular';

import avatarComponentOptions from './avatar.component';

let avatarModule: ng.IModule = angular.module('avatar', []);

avatarModule.component('avatar', avatarComponentOptions);

let avatarModuleName: string = avatarModule.name;

export default avatarModuleName;


