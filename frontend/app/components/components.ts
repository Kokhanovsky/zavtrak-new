import * as angular from "angular";
import accountModuleName from './account/account';
import accountProfileModuleName from './account-profile/account-profile';
import loginModuleName from './login/login';
import signUpModuleName from './signup/signup';
import passwordStrengthModuleName from './password-strength/password-strength';
import resetPasswordModuleName from './reset-password/reset-password';
import resendVerificationModuleName from './resend-verification/resend-verification';
import avatarUploadModuleName from './avatar-upload/avatar-upload';
import photoModuleName from './photo/photo';
import photoSingleUploadModuleName from './photo-single-upload/photo-single-upload';
import treeModuleName from './category/category';

import recipeAddModuleName from './recipe-add/recipe-add';
import videoAddModuleName from './video-add/video-add';
import adviceAddModuleName from './advice-add/advice-add';
import articleAddModuleName from './article-add/article-add';

import userItemsModuleName from './user-items/user-items';
import videoEditModuleName from './video-edit/video-edit';
import statusModuleName from './status/status';
import homeModuleName from './home/home';
import promoCarouselModuleName from './promo-carousel/promo-carousel';
import tagModuleName from './tag/tag';
import recipeStepsModuleName from './recipe-steps/recipe-steps';
import recipeIngredientsModuleName from './recipe-ingredients/recipe-ingredients';
import itemsListModuleName from './items-list/items-list';
import avatarModuleName from './avatar/avatar';
import recipeCardModuleName from './recipe-card/recipe-card';
import recipesRecentModuleName from './recipes-recent/recipes-recent';
import starsModuleName from './stars/stars';

let componentsModule: ng.IModule = angular.module('app.components', [
    recipeAddModuleName,
    accountModuleName,
    accountProfileModuleName,
    loginModuleName,
    signUpModuleName,
    passwordStrengthModuleName,
    resetPasswordModuleName,
    resendVerificationModuleName,
    photoModuleName,
    avatarUploadModuleName,
    treeModuleName,
    userItemsModuleName,
    statusModuleName,
    videoEditModuleName,
    homeModuleName,
    photoSingleUploadModuleName,
    promoCarouselModuleName,
    recipeStepsModuleName,
    recipeStepsModuleName,
    recipeIngredientsModuleName,
    itemsListModuleName,
    tagModuleName,
    videoAddModuleName,
    adviceAddModuleName,
    articleAddModuleName,
    avatarModuleName,
    recipeCardModuleName,
    recipesRecentModuleName,
    starsModuleName
]);

let componentsModuleName: string = componentsModule.name;

export default componentsModuleName;
