import AvatarUploadController from './avatar-upload.controller';
import './avatar-upload.sass';

let AvatarUploadComponent: ng.IComponentOptions = {
    bindings: {
        avuUser: '<',
        avuThis: '='
    },
    template: require('./avatar-upload.html'),
    controller:  AvatarUploadController
};

export default AvatarUploadComponent;
