import { AvatarUploadService } from './avatar-upload.service';
import { IUserModel } from '../account-profile/account-profile.controller';


enum Mode { Add, Loading, Success }

class AvatarUploadController {

    loading: boolean = false;
    progress: number = 0;
    mode: Mode = Mode.Add;
    avatarSrc: string = '';
    avatarCropped: string = '';
    cancelUploading: any;

    avuUser: IUserModel;
    avuThis: AvatarUploadController;

    /** @ngInject */
    constructor(private AvatarUploadService: AvatarUploadService,
                private $mdDialog: any,
                private toastr: any) {
    }

    $onInit() {
        this.avuThis = this;
        if (this.avuUser.hasAvatar) {
            this.updateAvatarSrc();
            this.mode = Mode.Success;
        }
    }

    updateAvatarSrc() {
        this.avatarSrc = '/storage/user/' + this.avuUser.id + '/avatar.jpg' + '?' + new Date().getTime();
    }

    upload(valid: any, files: any) {
        if (!valid) {
            this.$mdDialog.show(
                this.$mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Ошибка загрузки фото')
                    .textContent('Неправильный формат изображения или размер изображения слишком большой')
                    .ok('ok'));
            return;
        }

        this.progress = 0;
        this.loading = true;
        let isAborted = false;
        this.mode = Mode.Loading;
        this.AvatarUploadService.upload(files)
            .xhr((xhr: any) => {
                this.cancelUploading = () => {
                    xhr.abort();
                    this.mode = Mode.Add;
                    isAborted = true;
                };
            })
            .progress((data: angular.angularFileUpload.IFileProgressEvent) => {
                this.progress = Math.round((data.loaded / data.total) * 100);
            })

            .then((res: any) => {
                this.mode = Mode.Success;
                this.loading = false;
                this.updateAvatarSrc();
            })

            .catch((err: any) => {
                if (isAborted) {
                    console.log('Загрузка файла прервана');
                    return;
                }
                this.toastr.error(`Ошибка загрузки`);
                this.mode = Mode.Add;
            });
    }

    resetUpload() {
        this.mode = Mode.Add;
    }

    saveAvatar() {
        this.loading = true;
        return this.AvatarUploadService.uploadAvatar(this.avatarCropped).then(() => {
            this.loading = false;
        });
    }

}

export default AvatarUploadController;
