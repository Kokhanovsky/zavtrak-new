import * as angular from "angular";
import avatarUploadComponentOptions from './avatar-upload.component';
import { AvatarUploadService } from './avatar-upload.service';

let avatarUploadModule: ng.IModule = angular.module('avatarUploadModule', []);

avatarUploadModule.component('avatarUpload', avatarUploadComponentOptions);
avatarUploadModule.service('AvatarUploadService', AvatarUploadService);

let avatarUploadModuleName: string = avatarUploadModule.name;

export default avatarUploadModuleName;
