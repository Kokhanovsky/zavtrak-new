import * as angular from "angular";
import recipeAddComponentOptions from './recipe-add.component';
import { RecipeAddService } from './recipe-add.service';

let recipeAddModule: ng.IModule = angular.module('recipeAddModule', []);

recipeAddModule.component('recipeAdd', recipeAddComponentOptions);
recipeAddModule.service('RecipeAddService', RecipeAddService);

let recipeAddModuleName: string = recipeAddModule.name;

export default recipeAddModuleName;


