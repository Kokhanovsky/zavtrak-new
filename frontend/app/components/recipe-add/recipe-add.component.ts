import RecipeAddController from './recipe-add.controller';

let recipeAddComponent: ng.IComponentOptions = {
    bindings: {
        editId: '<'
    },
    template: require('./recipe-add.html'),
    controller:  RecipeAddController
};

export default recipeAddComponent;
