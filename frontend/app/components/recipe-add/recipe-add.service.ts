import IPromise = angular.IPromise;
import { IRecipeData } from "./recipe-add.controller";

export class RecipeAddService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(recipeData: IRecipeData) {
        return this.$http({
            method: 'POST',
            url: '/api/recipe',
            data: recipeData
        }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http({
            method: 'PUT',
            url: '/api/recipe',
            data: itemData
        }).then((res: any) => res.data.url);
    }

    getItem(id: number) {
        return this.$http({
            method: 'GET',
            url: '/api/recipe/edit',
            params: { id }
        })
        .then((response: any): any => {
            return response.data;
        });
    }

}
