import { RecipeAddService } from './recipe-add.service';
import * as angular from 'angular';
import { IRecipeIngredient, IRecipeVideo, IRecipeStep } from '../../../../backend/models/recipe';
import { ICategory } from '../../../../backend/models/category';

interface IPhoto {
    id: string;
    user?: string;
    item?: string;
}

export interface IRecipeData {
    title?: string;
    description?: string;
    ingredients?: IRecipeIngredient[];
    categories?: ICategory[];
    photos?: IPhoto[];
    videos?: IRecipeVideo[];
    steps?: IRecipeStep[];
    tags?: string[];
    category?: string[];
}

class RecipeAddController {

    loading: boolean = false;
    recipeData: IRecipeData = {
        ingredients: [],
        categories: [],
        tags: [],
        videos: [],
        steps: []
    };
    initialData: IRecipeData;
    keys: any[];
    editId: number;
    sendform: boolean = false;

    /** @ngInject */
    constructor(private RecipeAddService: RecipeAddService,
                private $mdConstant: any,
                private $window: ng.IWindowService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {
        this.$timeout(() => {
            this.initialData = angular.copy(this.recipeData);
        });

        this.$window.onbeforeunload = event => {
            console.log(this.initialData);
            console.log(this.recipeData);
            if (!angular.equals(this.recipeData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };

        if (this.editId) {
            this.RecipeAddService.getItem(this.editId).then((item: IRecipeData) => {
                this.recipeData = item;
                this.$timeout(() => this.initialData = angular.copy(item), 1500);
            })
            .catch(() => {
                this.$window.location.href = '/';
            })
        }

    }

    sendForm(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.recipeData);
        let action = this.editId ? this.RecipeAddService.edit(this.recipeData) : this.RecipeAddService.add(this.recipeData);
        action
        .then(url => {
            this.$window.location.href = url;
        })
        .catch((err: any) => {
            console.log(err);
            this.loading = false;
            (this.$rootScope as any).loading = true;
        });
    }

}

export default RecipeAddController;
