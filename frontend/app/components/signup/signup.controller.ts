import { SignUpService } from './signup.service';

export interface IUserSignUp {
    firstname: string;
    lastname?: string;
    email: string;
    password: string;
    captcha: string;
}

class SignUpController {

    recaptchaSiteKey: string;
    widgetId: string;
    response: any;
    serverError: string;
    success: boolean = false;
    loading: boolean = false;
    passwordInputType: string = 'password';
    showComponent: string;
    user: IUserSignUp = {
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        captcha: null
    };

    /** @ngInject */
    constructor(private SignUpService: SignUpService, private vcRecaptchaService: any) {
    }

    signUp() {
        if (this.loading) {
            return false;
        }
        this.serverError = '';
        this.loading = true;
        this.SignUpService.signUp(this.user)
            .then(() => {
                this.success = true;
            })
            .catch((err: any) => {
                console.log(err);
                this.serverError = 'Ошибка при обработке запроса.' + err.data.message ? err.data.message : '';
            })
            .then(() => {
                this.vcRecaptchaService.reload(this.widgetId);
                this.loading = false;
            });

    }

    changeInputPassType() {
        this.passwordInputType = this.passwordInputType === 'password' ? 'text' : 'password';
    }

    setResponse(response: string) {
        this.user.captcha = response;
        console.info('Captcha response available');
    }

    setWidgetId(widgetId: string) {
        console.info('Created widget ID: %s', widgetId);
        this.widgetId = widgetId;
    }

    cbExpiration() {
        console.info('Captcha expired. Resetting response object');
        this.vcRecaptchaService.reload(this.widgetId);
        this.user.captcha = null;
    }

}

export default SignUpController;
