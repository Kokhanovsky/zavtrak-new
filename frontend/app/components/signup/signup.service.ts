import * as angular from "angular";
import { IUserSignUp } from './signup.controller';
import IPromise = angular.IPromise;

export class SignUpService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    signUp(signUpData: IUserSignUp): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/signup',
                data: angular.copy(signUpData)
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
