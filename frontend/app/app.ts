import * as angular from "angular";
import * as moment from "moment";

import 'angular-material';
import 'angular-messages';
import 'angular-cookies';
import 'angular-recaptcha';
import 'angular-toastr';
import 'angular-drag-and-drop-lists';
import 'angular-paging';
import 'angular-youtube-embed'
import 'ng-idle';
import 'ng-file-upload';
import 'angular-sanitize';
import 'angular-local-storage';
import 'angular-scroll';
import 'angular-ui-router'
import 'textangular';
import 'oclazyload';
import 'textangular/dist/textAngular-sanitize.min';

import 'ng-img-crop/compile/minified/ng-img-crop.js';

// import css styles
import './cssmodules/index.sass';
import '../../node_modules/ng-img-crop/source/scss/ng-img-crop.scss';
import 'textangular/src/textAngular.css';

// import all components
import componentsModuleName from './components/components';

// import all directives
import directivesModuleName from './directives/directives';

// import libs
import libModuleName from './lib/lib';

import { runBlock } from './app.run';

moment.locale("ru");

angular.module('app', [
    'ngMaterial',
    'ngMessages',
    'ngIdle',
    'vcRecaptcha',
    'toastr',
    'ngCookies',
    'ngFileUpload',
    'ngImgCrop',
    'dndLists',
    'bw.paging',
    'ngSanitize',
    'ngSanitize',
    'youtube-embed',
    'LocalStorageModule',
    'duScroll',
    'textAngular',
    'ui.router',
    'oc.lazyLoad',
    componentsModuleName,
    directivesModuleName,
    libModuleName])
.run(runBlock);

angular.bootstrap(document, ['app'], {
    strictDi: true
});
