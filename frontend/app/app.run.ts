import { IdleService } from './lib/idle.service';
import { CommonService } from './lib/common.service';

interface IRootScopeUser {
    username: string;
    email: string;
    verified: boolean;
}

export interface ICustomRootScope extends ng.IRootScopeService {
    user: IRootScopeUser;
    authRequired: boolean;
    $mdMedia: any;
    trixInitialize: any;
}

export interface ICustomWindow extends ng.IWindowService {
    user: IRootScopeUser;
    authRequired: boolean;
}

/** @ngInject */
export function runBlock($rootScope: ICustomRootScope, IdleService: IdleService, $window: ICustomWindow, $mdMedia: any) {
    $rootScope.user = $window.user ? $window.user : null;
    $rootScope.authRequired = $window.authRequired ? true : false;
    $rootScope.$mdMedia = $mdMedia;
}
