import { AuthService } from './auth.service';
import IPromise = angular.IPromise;

export class IdleService {

    /** @ngInject */
    constructor(private Idle: ng.idle.IIdleService,
                private AuthService: AuthService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $window: ng.IWindowService,
                private $cookies: angular.cookies.ICookiesService,
                private toastr: any) {

        // start idle watching in 2sec if authenticated and access_session has been set
        if ($cookies.get('access_session')) {
            $timeout(() => AuthService.authstatus()
                .then(() => Idle.watch())
                .catch(() => AuthService.logout()), 5000);
        }

        $rootScope.$on('IdleStart', () => {
            toastr.warning('Ваша сессия истекает, будет выполнен выход', 'Внимание', {
                'positionClass': 'toast-top-center',
                'timeOut': 30000,
                'progressBar': true
            });
        });

        $rootScope.$on('IdleWarn', (e: any, countdown: number) => {
            console.log('Idle warning: ' + countdown);
        });

        $rootScope.$on('IdleTimeout', () => {
            console.log('Idle timeout');
            AuthService.logout().then(() => {
                if (($rootScope as any).authRequired) {
                    $window.location.href = '/login';
                }
            });
        });

        $rootScope.$on('IdleEnd', () => {
            console.log('Idle end');
            toastr.clear();
        });

        $rootScope.$on('Keepalive', () => {
            if (!$cookies.get('access_session')) {
                AuthService.logout();
            }
            AuthService.authstatus()
                .catch(() => {
                    AuthService.logout();
                });
        });

    }

}
