/** @ngInject */
export default function ThemeConfig($mdThemingProvider: any) {
    $mdThemingProvider.theme('altTheme')
    .primaryPalette('deep-orange',{
        'default': '900'})
    .accentPalette('deep-orange',{
        'default': '900'})
    $mdThemingProvider.theme('default');
    $mdThemingProvider.setDefaultTheme('altTheme');
    $mdThemingProvider.alwaysWatchTheme(true);
}
