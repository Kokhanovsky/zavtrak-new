/** @ngInject */
export default function IdleConfig(IdleProvider: angular.idle.IIdleProvider,
                                   KeepaliveProvider: angular.idle.IKeepAliveProvider,
                                   toastrConfig: any) {
    // configure Idle settings
    IdleProvider.idle(1800); // in seconds, 1800 = 30 minutes
    IdleProvider.timeout(30); // in seconds
    KeepaliveProvider.interval(20); // in seconds

    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';

}
