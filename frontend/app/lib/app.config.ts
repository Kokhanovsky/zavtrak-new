import IRootScopeService = angular.IRootScopeService;
/** @ngInject */
export default function AppConfig($mdAriaProvider: any,
                                  $locationProvider: ng.ILocationProvider,
                                  localStorageServiceProvider: ng.local.storage.ILocalStorageServiceProvider) {

    localStorageServiceProvider.setPrefix('zavtrak');
    $mdAriaProvider.disableWarnings();

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

}

