import * as angular from 'angular';
import { IPromo } from '../../../../backend/models/promo';

export class PromoListService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getList() {
        return this.$http(
            {
                method: 'GET',
                url: '/adm-service/promo/list',
                params: { }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    changeOrder(promos: IPromo[]) {
        let ids = promos.map(promo => promo._id);
        return this.$http(
            {
                method: 'PUT',
                url: '/adm-service/promo/change-order',
                data: { ids }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/adm-service/promo/item',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    remove(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/adm-service/promo',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
