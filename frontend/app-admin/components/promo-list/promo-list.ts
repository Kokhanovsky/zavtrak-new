import * as angular from 'angular';
import { PromoListService } from './promo-list.service';

import promoListComponentOptions from './promo-list.component';

let promoListModule: ng.IModule = angular.module('promoList', []);

promoListModule.component('promoList', promoListComponentOptions);
promoListModule.service('PromoListService', PromoListService);

let promoListModuleName: string = promoListModule.name;

export default promoListModuleName;


