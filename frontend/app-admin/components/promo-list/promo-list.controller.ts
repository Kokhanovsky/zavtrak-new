import * as angular from 'angular';
import { PromoListService } from './promo-list.service';
import { IPromo } from '../../../../backend/models/promo';
import IHttpService = angular.IHttpService;

class PromoListController {

    promos: IPromo[];
    onEdit: any;
    editId: string;

    /** @ngInject */
    constructor(private PromoListService: PromoListService,
                private $timeout: ng.ITimeoutService,
                private $rootScope: ng.IRootScopeService,
                private $mdDialog: any) {
        this.getList();
    }

    getList() {
        (this.$rootScope as any).loading = true;
        this.PromoListService.getList().then((promos: IPromo[]) => {
            this.promos = promos;
        }).catch(err => console.log(err)).then(() => {
            (this.$rootScope as any).loading = false;
        });
    }

    changeOrder() {
        (this.$rootScope as any).loading = true;
        this.PromoListService.changeOrder(this.promos).then((promos: IPromo[]) => {
            console.log('order changed')
        }).catch(err => console.log(err)).then(() => {
            (this.$rootScope as any).loading = false;
        });
    }

    edit(id) {
        this.editId = id;
        this.$timeout(this.onEdit);
    }

    remove(id) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить слайд?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.promos = this.promos.filter((promo: IPromo) => promo._id !== id);
            this.PromoListService.remove(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }
}

export default PromoListController;
