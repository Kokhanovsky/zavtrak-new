import PromoListController from './promo-list.controller';
import './promo-list.sass';

let promoListComponent: ng.IComponentOptions = {
    bindings: {
        onEdit: '&',
        editId: '='
    },
    template: require('./promo-list.html'),
    controller:  PromoListController
};

export default promoListComponent;
