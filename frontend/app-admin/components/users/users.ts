import * as angular from 'angular';
import userItemsComponentOptions from './users.component';
import { UsersService } from './users.service';

let usersModule: ng.IModule = angular.module('users', []);

usersModule.component('users', userItemsComponentOptions);
usersModule.service('UsersService', UsersService);

let usersModuleName: string = usersModule.name;

export default usersModuleName;


