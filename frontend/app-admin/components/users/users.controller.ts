import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { UsersService, IUserCustom } from "./users.service";

class UsersController {

    filterOptions: any = [
        {
            title: 'все',
            value: 'all'
        },
        {
            title: 'пользователи',
            value: 'user'
        },
        {
            title: 'админы',
            value: 'admin'
        }
    ];

    page: 1;

    userItems: IUserCustom[] = [];
    total: number;
    _typeFilter: string = 'all';
    _searchFilter: string;
    perPage: number = 10;
    loading: boolean = true;

    get searchFilter() {
        return this._searchFilter;
    }

    set searchFilter(value: string) {
        this._searchFilter = value;
        this.page = 1;
        this.getUsers();
    }

    set typeFilter(value: string) {
        this._typeFilter = value;
        this.page = 1;
        this.getUsers();
    }

    get typeFilter() {
        return this._typeFilter;
    }

    /** @ngInject */
    constructor(private UsersService: UsersService,
                private $rootScope: any) {
        this.getUsers();
    }

    getUsers() {
        this.$rootScope.loading = true;
        this.UsersService.getUsers(this.page, this._typeFilter, this._searchFilter).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.userItems = [];
                return;
            }
            this.userItems = res.items;
            this.highlightFilter();
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading user items') })
        .then(() => { this.$rootScope.loading = false })
    }

    getPage() {
        setTimeout(() => {
            this.getUsers();
        })
    }

    private highlightFilter(): any[] {
        if (!this._searchFilter) {
            return this.userItems;
        }
        let res = angular.copy(this.userItems);
        let regex = new RegExp('('+this._searchFilter+')', 'gi');
        let replace = '<span class="highlighted">$1</span>';
        this.userItems = res.map((user: any) => {
            user.fullname = user.fullname ? user.fullname.replace(regex, replace) : '';
            user.nickname = user.nickname ? user.nickname.replace(regex, replace) : '';
            user.email = user.email ? user.email.replace(regex, replace) : '';
            return user;
        });
    }

}

export default UsersController;
