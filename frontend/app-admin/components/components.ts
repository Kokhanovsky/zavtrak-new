import * as angular from "angular";
import categoryModuleName from '../../app/components/category/category';
import statusModuleName from '../../app/components/status/status';
import switchModuleName from '../components/switch/switch';
import usersModuleName from '../components/users/users';
import promoModuleName from '../components/promo/promo';
import promoListModuleName from '../components/promo-list/promo-list';
import promoAddModuleName from '../components/promo-add/promo-add';
import photoModuleName from '../../app/components/photo/photo';
import avatarModuleName from '../../app/components/avatar/avatar';
import photoSingleUploadModuleName from '../../app/components/photo-single-upload/photo-single-upload';
import admItemsModuleName from '../components/adm-items/adm-items';
import itemsListModuleName from '../components/items-list/items-list';

let componentsModule: ng.IModule = angular.module('app.components', [
    categoryModuleName,
    statusModuleName,
    switchModuleName,
    usersModuleName,
    promoModuleName,
    promoListModuleName,
    promoAddModuleName,
    photoModuleName,
    photoSingleUploadModuleName,
    admItemsModuleName,
    itemsListModuleName,
    avatarModuleName
]);

let componentsModuleName: string = componentsModule.name;

export default componentsModuleName;
