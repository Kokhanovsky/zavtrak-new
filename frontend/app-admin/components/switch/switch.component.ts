import SwitchController from './switch.controller';
import './switch.sass';

let switchComponent: ng.IComponentOptions = {
    bindings: {
        value: '=',
        options: '<'
    },
    template: require('./switch.html'),
    controller:  SwitchController
};

export default switchComponent;
