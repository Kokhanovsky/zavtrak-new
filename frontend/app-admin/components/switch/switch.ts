import * as angular from 'angular';

import switchComponentOptions from './switch.component';

let switchModule: ng.IModule = angular.module('switch', []);

switchModule.component('switch', switchComponentOptions);

let switchModuleName: string = switchModule.name;

export default switchModuleName;


