import * as angular from "angular";
import IHttpService = angular.IHttpService;

class SwitchController {

    value: string[];
    options: any[];

    /** @ngInject */
    constructor() {

    }

    $onInit() {
        if (!this.value) {
            this.value = [];
            this.value.push(this.options[0].value);
        }
    }

    setValue(val: string) {
        /*let index = this.value.indexOf(val);
        if (this.value.indexOf(val) === -1) {
            this.value.push(val);
        } else {
            this.value.splice(index, 1);
        }*/
        this.value = [];
        this.value.push(val);
    }

    isActive(val: string) {
        return this.value.indexOf(val) !== -1;
    }

}

export default SwitchController;
