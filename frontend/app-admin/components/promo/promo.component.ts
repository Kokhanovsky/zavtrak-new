import PromoController from './promo.controller';
import './promo.sass';

let promoComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./promo.html'),
    controller:  PromoController
};

export default promoComponent;
