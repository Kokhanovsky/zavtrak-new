import * as angular from 'angular';
import promoComponentOptions from './promo.component';
import { PromoService } from './promo.service';

let promoModule: ng.IModule = angular.module('promo', []);

promoModule.component('promo', promoComponentOptions);
promoModule.service('PromoService', PromoService);

let promoModuleName: string = promoModule.name;

export default promoModuleName;


