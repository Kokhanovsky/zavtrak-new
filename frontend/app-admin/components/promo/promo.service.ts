import * as angular from 'angular';

export class PromoService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    deleteItem(itemId: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/item',
                data: { itemId }
            });
    }

}
