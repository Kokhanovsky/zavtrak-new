import { AdmArticlesService } from './adm-articles.service';
import { IArticle } from '../../../../../backend/models/article';

class AdmArticlesController {

    articles: IArticle[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmArticlesService: AdmArticlesService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $window: ng.IWindowService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getArticles();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getArticles());
    }

    getArticles() {
        (this.$rootScope as any).loading = true;
        this.AdmArticlesService.getArticles(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.articles = [];
                return;
            }
            this.articles = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => {
            console.log('There were errors while loading articles')
        })
        .then(() => {
            (this.$rootScope as any).loading = false
        })
    }

    editItem(id: string) {
        this.$window.location.href = '/article-add/' + id;
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить статью?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.articles = this.articles.filter(article => (article as any)._id !== id);
            this.AdmArticlesService.deleteArticle(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }
}

export default AdmArticlesController;
