import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IArticle } from '../../../../../backend/models/article';
import { Common } from '../../../../../backend/lib/common';

export class AdmArticlesService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getArticles(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/articles',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((article: IArticle) => {
                let item: IItem = {
                    _id: article._id,
                    title: article.title,
                    url: Common.getArticleUrl(article),
                    editUrl: '/article-add?edit=' +  article._id,
                    imageSrc: Common.getArticleImgSrc(article),
                    createdAt: Common.formatCreatedDate(article.createdAt),
                    status: article.status,
                    user: article.user
                };
                return item;
            });
            return data;
        });
    }

    deleteArticle(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/articles',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
