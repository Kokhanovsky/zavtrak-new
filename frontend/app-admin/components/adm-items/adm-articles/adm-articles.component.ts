import AdmArticlesController from './adm-articles.controller';
import './adm-articles.sass';

let admArticlesComponent: ng.IComponentOptions = {
    template: require('./adm-articles.html'),
    controller:  AdmArticlesController
};

export default admArticlesComponent;
