import { AdmAdvicesService } from './adm-advices.service';
import { IAdvice } from '../../../../../backend/models/advice';
import * as angular from 'angular';

class AdmAdvicesController {

    advices: IAdvice[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmAdvicesService: AdmAdvicesService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getAdvices();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getAdvices());
    }

    getAdvices() {
        (this.$rootScope as any).loading = true;
        this.AdmAdvicesService.getAdvices(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.advices = [];
                return;
            }
            this.advices = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(err => {
            console.log(err)
        })
        .then(() => {
            (this.$rootScope as any).loading = false
        })
    }

    editItem(id: string) {
        this.$mdDialog.show({
            locals: { id },
            controller: ChangeController,
            controllerAs: '$ctrl',
            template: require('./change-advice.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((advice: IAdvice) => {
            this.advices = this.advices.map(a => {
                if (advice._id === a._id) {
                    (a as any).title = advice.content;
                }
                return a;
            });
        }, () => {
            console.log('cancel');
        });
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить совет?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.advices = this.advices.filter(advice => (advice as any)._id !== id);
            this.AdmAdvicesService.deleteAdvice(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }
}

class ChangeController {

    advice: IAdvice;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private $scope: ng.IScope,
                private AdmAdvicesService: AdmAdvicesService,
                private id: string) {

        AdmAdvicesService.edit(id).then((advice: IAdvice) => {
            this.advice = advice;
        });
    }

    save() {
        this.AdmAdvicesService.put(this.advice).then(advice => {
            this.$mdDialog.hide(advice);
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AdmAdvicesController;
