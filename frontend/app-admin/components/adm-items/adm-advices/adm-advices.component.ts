import AdmAdvicesController from './adm-advices.controller';
import './adm-advices.sass';

let admAdvicesComponent: ng.IComponentOptions = {
    template: require('./adm-advices.html'),
    controller:  AdmAdvicesController
};

export default admAdvicesComponent;
