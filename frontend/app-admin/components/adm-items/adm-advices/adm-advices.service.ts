import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { Common } from '../../../../../backend/lib/common';
import { IAdvice } from '../../../../../backend/models/advice';

export class AdmAdvicesService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getAdvices(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/advices',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((advice: IAdvice) => {
                let item: IItem = {
                    _id: advice._id,
                    title: advice.content,
                    url: Common.getAdviceUrl(advice),
                    editUrl: '/advice-add?edit=' +  advice._id,
                    createdAt: Common.formatCreatedDate(advice.createdAt),
                    status: advice.status,
                    user: advice.user
                };
                return item;
            });
            return data;
        });
    }

    deleteAdvice(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/advices',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/advices',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    put(advice: IAdvice) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/adm-items/advices',
                data: { advice }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
