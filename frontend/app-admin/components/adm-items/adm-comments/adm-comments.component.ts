import AdmCommentsController from './adm-comments.controller';
import './adm-comments.sass';

let admCommentsComponent: ng.IComponentOptions = {
    template: require('./adm-comments.html'),
    controller:  AdmCommentsController
};

export default admCommentsComponent;
