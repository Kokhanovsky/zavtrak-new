import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IComment } from '../../../../../backend/models/comment';
import { Common } from '../../../../../backend/lib/common';


export class AdmCommentsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getComments(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/comments',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((comment: IComment) => {
                let item: IItem = {
                    _id: comment._id,
                    title: comment.comment,
                    //url: Common.getCommentUrl(comment),
                    url: '',
                    editUrl: '/comment-add?edit=' +  comment._id,
                    createdAt: Common.formatCreatedDate(comment.createdAt),
                    status: comment.status,
                    user: comment.user
                };
                return item;
            });
            return data;
        });
    }

    deleteComment(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/comments',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/comments',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    put(comment: IComment) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/adm-items/comments',
                data: { comment }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
