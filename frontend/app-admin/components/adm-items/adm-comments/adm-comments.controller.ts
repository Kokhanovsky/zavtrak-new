import { AdmCommentsService } from './adm-comments.service';
import { IComment } from '../../../../../backend/models/comment';
import * as angular from 'angular';

class AdmCommentsController {

    comments: IComment[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmCommentsService: AdmCommentsService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getComments();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getComments());
    }

    getComments() {
        (this.$rootScope as any).loading = true;
        this.AdmCommentsService.getComments(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.comments = [];
                return;
            }
            this.comments = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading comments') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    editItem(id: string) {
        this.$mdDialog.show({
            locals: { id },
            controller: ChangeController,
            controllerAs: '$ctrl',
            template: require('./change-comment.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((comment: IComment) => {
            this.comments  = this.comments.map(c => {
                if (c._id === comment._id) {
                    (c as any).title = comment.comment;
                }
                return c;
            });
        }, () => {
            console.log('cancel');
        });
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить комментарий?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.comments = this.comments.filter(comment => (comment as any)._id !== id);
            this.AdmCommentsService.deleteComment(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

}

class ChangeController {

    comment: IComment;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private AdmCommentsService: AdmCommentsService,
                private id: string) {

        AdmCommentsService.edit(id).then((comment: IComment) => {
            this.comment = comment;
        });
    }

    save() {
        this.AdmCommentsService.put(this.comment).then(comment => {
            this.$mdDialog.hide(comment);
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AdmCommentsController;
