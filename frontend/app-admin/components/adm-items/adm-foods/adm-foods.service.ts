import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IFood } from '../../../../../backend/models/food';
import { Common } from '../../../../../backend/lib/common';


export class AdmFoodsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getFoods(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/foods',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((food: IFood) => {
                let item: IItem = {
                    _id: food._id,
                    title: food.food,
                    //url: Common.getFoodUrl(food),
                    url: '',
                    editUrl: '/food-add?edit=' +  food._id,
                    createdAt: Common.formatCreatedDate(food.createdAt),
                    status: food.status,
                    user: food.user
                };
                return item;
            });
            return data;
        });
    }

    deleteFood(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/foods',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/foods',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    put(food: IFood) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/adm-items/foods',
                data: { food }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
