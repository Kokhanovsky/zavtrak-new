import { AdmFoodsService } from './adm-foods.service';
import { IFood } from '../../../../../backend/models/food';
import * as angular from 'angular';

class AdmFoodsController {

    foods: IFood[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmFoodsService: AdmFoodsService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getFoods();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getFoods());
    }


    getFoods() {
        (this.$rootScope as any).loading = true;
        this.AdmFoodsService.getFoods(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.foods = [];
                return;
            }
            this.foods = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading foods') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    editItem(id: string) {
        this.$mdDialog.show({
            locals: { id },
            controller: ChangeController,
            controllerAs: '$ctrl',
            template: require('./change-food.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((food: IFood) => {
            this.foods  = this.foods.map(c => {
                if (c._id === food._id) {
                    (c as any).title = food.food;
                }
                return c;
            });
        }, () => {
            console.log('cancel');
        });
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить продукт?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.foods = this.foods.filter(food => (food as any)._id !== id);
            this.AdmFoodsService.deleteFood(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

}

class ChangeController {

    food: IFood;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private AdmFoodsService: AdmFoodsService,
                private id: string) {

        AdmFoodsService.edit(id).then((food: IFood) => {
            this.food = food;
        });
    }

    save() {
        this.AdmFoodsService.put(this.food).then(food => {
            this.$mdDialog.hide(food);
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AdmFoodsController;
