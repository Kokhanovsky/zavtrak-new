import AdmFoodsController from './adm-foods.controller';
import './adm-foods.sass';

let admFoodsComponent: ng.IComponentOptions = {
    template: require('./adm-foods.html'),
    controller:  AdmFoodsController
};

export default admFoodsComponent;
