import AdmTagsController from './adm-tags.controller';
import './adm-tags.sass';

let admTagsComponent: ng.IComponentOptions = {
    template: require('./adm-tags.html'),
    controller:  AdmTagsController
};

export default admTagsComponent;
