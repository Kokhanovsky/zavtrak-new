import { AdmTagsService } from './adm-tags.service';
import { ITag } from '../../../../../backend/models/tag';
import * as angular from 'angular';

class AdmTagsController {

    tags: ITag[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmTagsService: AdmTagsService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getTags();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getTags());
    }

    getTags() {
        (this.$rootScope as any).loading = true;
        this.AdmTagsService.getTags(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.tags = [];
                return;
            }
            this.tags = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading tags') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    editItem(id: string) {
        this.$mdDialog.show({
            locals: { id },
            controller: ChangeController,
            controllerAs: '$ctrl',
            template: require('./change-tag.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((tag: ITag) => {
            this.tags  = this.tags.map(c => {
                if (c._id === tag._id) {
                    (c as any).title = tag.tag;
                }
                return c;
            });
        }, () => {
            console.log('cancel');
        });
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить тег?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.tags = this.tags.filter(tag => (tag as any)._id !== id);
            this.AdmTagsService.deleteTag(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

}

class ChangeController {

    tag: ITag;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private AdmTagsService: AdmTagsService,
                private id: string) {

        AdmTagsService.edit(id).then((tag: ITag) => {
            this.tag = tag;
        });
    }

    save() {
        this.AdmTagsService.put(this.tag).then(tag => {
            this.$mdDialog.hide(tag);
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AdmTagsController;
