import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { ITag } from '../../../../../backend/models/tag';
import { Common } from '../../../../../backend/lib/common';


export class AdmTagsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getTags(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/tags',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((tag: ITag) => {
                let item: IItem = {
                    _id: tag._id,
                    title: tag.tag,
                    //url: Common.getTagUrl(tag),
                    url: '',
                    editUrl: '/tag-add?edit=' +  tag._id,
                    createdAt: Common.formatCreatedDate(tag.createdAt),
                    status: tag.status,
                    user: tag.user
                };
                return item;
            });
            return data;
        });
    }

    deleteTag(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/tags',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/tags',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    put(tag: ITag) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/adm-items/tags',
                data: { tag }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
