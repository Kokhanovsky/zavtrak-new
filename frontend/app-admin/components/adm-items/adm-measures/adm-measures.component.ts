import AdmMeasuresController from './adm-measures.controller';
import './adm-measures.sass';

let admMeasuresComponent: ng.IComponentOptions = {
    template: require('./adm-measures.html'),
    controller:  AdmMeasuresController
};

export default admMeasuresComponent;
