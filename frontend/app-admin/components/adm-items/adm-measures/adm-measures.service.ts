import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IMeasure } from '../../../../../backend/models/measure';
import { Common } from '../../../../../backend/lib/common';


export class AdmMeasuresService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getMeasures(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/measures',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((measure: IMeasure) => {
                let item: IItem = {
                    _id: measure._id,
                    title: measure.measure,
                    //url: Common.getMeasureUrl(measure),
                    url: '',
                    editUrl: '/measure-add?edit=' +  measure._id,
                    createdAt: Common.formatCreatedDate(measure.createdAt),
                    status: measure.status,
                    user: measure.user
                };
                return item;
            });
            return data;
        });
    }

    deleteMeasure(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/measures',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }


    edit(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/measures',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

    put(measure: IMeasure) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/adm-items/measures',
                data: { measure }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
