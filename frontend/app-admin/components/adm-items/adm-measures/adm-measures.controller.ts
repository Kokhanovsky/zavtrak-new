import { AdmMeasuresService } from './adm-measures.service';
import { IMeasure } from '../../../../../backend/models/measure';
import * as angular from 'angular';

class AdmMeasuresController {

    measures: IMeasure[] = [];
    perPage: number = 10;
    total: number;
    page: number = 1;
    query: string;
    status: string[];


    /** @ngInject */
    constructor(private AdmMeasuresService: AdmMeasuresService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getMeasures()
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getMeasures());
    }

    getMeasures() {
        (this.$rootScope as any).loading = true;
        this.AdmMeasuresService.getMeasures(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.measures = [];
                return;
            }
            this.measures = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading measures') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    editItem(id: string) {
        this.$mdDialog.show({
            locals: { id },
            controller: ChangeController,
            controllerAs: '$ctrl',
            template: require('./change-measure.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((measure: IMeasure) => {
            this.measures  = this.measures.map(c => {
                if (c._id === measure._id) {
                    (c as any).title = measure.measure;
                }
                return c;
            });
        }, () => {
            console.log('cancel');
        });
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить единицу измерения?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.measures = this.measures.filter(measure => (measure as any)._id !== id);
            this.AdmMeasuresService.deleteMeasure(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

}

class ChangeController {

    measure: IMeasure;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private AdmMeasuresService: AdmMeasuresService,
                private id: string) {

        AdmMeasuresService.edit(id).then((measure: IMeasure) => {
            this.measure = measure;
        });
    }

    save() {
        this.AdmMeasuresService.put(this.measure).then(measure => {
            this.$mdDialog.hide(measure);
        });
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default AdmMeasuresController;
