import AdmVideosController from './adm-videos.controller';
import './adm-videos.sass';

let admVideosComponent: ng.IComponentOptions = {
    template: require('./adm-videos.html'),
    controller:  AdmVideosController
};

export default admVideosComponent;
