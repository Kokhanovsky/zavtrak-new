import IPromise = angular.IPromise;
import { IPagination, IItem } from '../../../../../backend/lib/interfaces';
import { IVideo } from '../../../../../backend/models/video';
import { Common } from '../../../../../backend/lib/common';


export class AdmVideosService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getVideos(page: number, query: string, status: string[]): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/videos',
                params: { page, query, status }
            })
        .then((response: any) => {
            let data: IPagination = response.data;
            data.docs = data.docs.map((video: IVideo) => {
                let item: IItem = {
                    _id: video._id,
                    title: video.title,
                    url: Common.getVideoUrl(video),
                    editUrl: '/video-add?edit=' +  video._id,
                    imageSrc: Common.getVideoImgSrc(video),
                    createdAt: Common.formatCreatedDate(video.createdAt),
                    status: video.status,
                    user: video.user
                };
                return item;
            });
            return data;
        });
    }

    deleteVideo(id: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/adm-items/videos',
                data: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }
}
