import { AdmVideosService } from './adm-videos.service';
import { IVideo } from '../../../../../backend/models/video';

class AdmVideosController {

    videos: IVideo[] = [];
    page: number = 1;
    perPage: number = 10;
    total: number;
    query: string;
    status: string[];

    /** @ngInject */
    constructor(private AdmVideosService: AdmVideosService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $window: ng.IWindowService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getVideos();
            }, true);
        });
    }

    getPage() {
        this.$timeout(() => this.getVideos());
    }

    getVideos() {
        (this.$rootScope as any).loading = true;
        this.AdmVideosService.getVideos(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.videos = [];
                return;
            }
            this.videos = res.docs;
            this.total = res.total;
        })
        .catch(() => {
            console.log('There were errors while loading videos')
        })
        .then(() => {
            (this.$rootScope as any).loading = false
        })
    }

    editItem(id: string) {
        this.$window.location.href = '/video-add/' + id;
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить видео?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.videos = this.videos.filter(video => (video as any)._id !== id);
            this.AdmVideosService.deleteVideo(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

}

export default AdmVideosController;
