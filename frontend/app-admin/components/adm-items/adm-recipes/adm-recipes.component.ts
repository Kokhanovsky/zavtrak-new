import AdmRecipesController from './adm-recipes.controller';
import './adm-recipes.sass';

let admRecipesComponent: ng.IComponentOptions = {
    template: require('./adm-recipes.html'),
    controller:  AdmRecipesController
};

export default admRecipesComponent;
