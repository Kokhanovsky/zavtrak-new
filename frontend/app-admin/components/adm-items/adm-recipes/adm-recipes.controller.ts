import { AdmRecipesService } from './adm-recipes.service';
import { IRecipe } from '../../../../../backend/models/recipe';

class AdmRecipesController {

    recipes: IRecipe[] = [];
    perPage: number = 10;
    total: number;
    page: number = 1;
    status: string[];
    query: string;

    /** @ngInject */
    constructor(private AdmRecipesService: AdmRecipesService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private $window: ng.IWindowService,
                private $mdDialog: any) {
    }

    $onInit() {
        this.$timeout(() => {
            this.$rootScope.$watch(() => [this.status, this.query], () => {
                this.page = 1;
                this.getRecipes();
                console.log('watch')
            }, true);
        })
    }

    getPage() {
        this.$timeout(() => this.getRecipes());
    }

    getRecipes() {
        (this.$rootScope as any).loading = true;
        this.AdmRecipesService.getRecipes(this.page, this.query, this.status).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.recipes = [];
                return;
            }
            this.recipes = res.docs;
            /*this.highlightFilter();*/
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading recipes') })
        .then(() => { (this.$rootScope as any).loading = false })
    }

    editItem(id: string) {
        this.$window.location.href = '/recipe-add/' + id;
    }

    deleteItem(id: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить рецепт?')
        .textContent('Пожалуйста, выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            (this.$rootScope as any).loading = true;
            this.recipes = this.recipes.filter(recipe => (recipe as any)._id !== id);
            this.AdmRecipesService.deleteRecipe(id).then(() => {
                console.log('remove call')
            }).catch(err => console.log(err)).then(() => {
                (this.$rootScope as any).loading = false;
            });
        });
    }

/*
    private highlightFilter(): any[] {
        if (!this._query) {
            return this.recipes;
        }
        let res = angular.copy(this.recipes);
        this.recipes = res.map((item: any) => {
            item.title = item.title.replace(new RegExp('('+this._query+')', 'gi'),
                '<span class="highlighted">$1</span>');
            return item;
        });
    }
*/


}

export default AdmRecipesController;
