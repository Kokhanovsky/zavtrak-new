import './adm-items.sass';
import AdmItemsController from './adm-items.controller';

let admItemsComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./adm-items.html'),
    controller: AdmItemsController
};

export default admItemsComponent;
