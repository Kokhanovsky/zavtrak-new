import { AdmItemsService } from './adm-items.service';
import { ICounts } from '../../../../backend/lib/interfaces';

class AdmItemsController {

    counts: ICounts = {
        recipes: 0,
        articles: 0,
        advices: 0
    };

    /** @ngInject */
    constructor(private AdmItemsService: AdmItemsService, private $rootScope: ng.IRootScopeService) {
    }

    $onInit() {
        this.getCounts();
        this.$rootScope.$on('status.changed', () => this.getCounts());
    }

    getCounts() {
        this.AdmItemsService.getCounts().then((res: any) => {
            this.counts = res;
        });
    }

}

export default AdmItemsController;
