import IPromise = angular.IPromise;

export class AdmItemsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getCounts(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/adm-items/counts'
            })
        .then(result => result.data);
    }

}
