import * as angular from "angular";

import admItemsComponentOptions from './adm-items.component';

import routerConfig from './adm-items.config';

let admItemsModule: ng.IModule = angular.module('admItemsModule', []);

admItemsModule.config(routerConfig);
admItemsModule.component('admItems', admItemsComponentOptions);


// COUNTS
import { AdmItemsService } from './adm-items.service';
import AdmCountsController from './adm-items.controller';

admItemsModule.service('AdmItemsService', AdmItemsService);
admItemsModule.controller('AdmCountsController', AdmCountsController);


// RECIPES
import admRecipesComponentOptions from './adm-recipes/adm-recipes.component';
import { AdmRecipesService } from './adm-recipes/adm-recipes.service';
import AdmRecipesController from './adm-recipes/adm-recipes.controller';

admItemsModule.service('AdmRecipesService', AdmRecipesService);
admItemsModule.controller('AdmRecipesController', AdmRecipesController);
admItemsModule.component('admRecipes', admRecipesComponentOptions);


// ARTICLES
import admArticlesComponentOptions from './adm-articles/adm-articles.component';
import { AdmArticlesService } from './adm-articles/adm-articles.service';
import AdmArticlesController from './adm-articles/adm-articles.controller';

admItemsModule.service('AdmArticlesService', AdmArticlesService);
admItemsModule.controller('AdmArticlesController', AdmArticlesController);
admItemsModule.component('admArticles', admArticlesComponentOptions);


// ADVICES
import admAdvicesComponentOptions from './adm-advices/adm-advices.component';
import { AdmAdvicesService } from './adm-advices/adm-advices.service';
import AdmAdvicesController from './adm-advices/adm-advices.controller';

admItemsModule.service('AdmAdvicesService', AdmAdvicesService);
admItemsModule.controller('AdmAdvicesController', AdmAdvicesController);
admItemsModule.component('admAdvices', admAdvicesComponentOptions);


// VIDEOS
import admVideosComponentOptions from './adm-videos/adm-videos.component';
import { AdmVideosService } from './adm-videos/adm-videos.service';
import AdmVideosController from './adm-videos/adm-videos.controller';

admItemsModule.service('AdmVideosService', AdmVideosService);
admItemsModule.controller('AdmVideosController', AdmVideosController);
admItemsModule.component('admVideos', admVideosComponentOptions);

// TAGS
import admTagsComponentOptions from './adm-tags/adm-tags.component';
import { AdmTagsService } from './adm-tags/adm-tags.service';
import AdmTagsController from './adm-tags/adm-tags.controller';

admItemsModule.service('AdmTagsService', AdmTagsService);
admItemsModule.controller('AdmTagsController', AdmTagsController);
admItemsModule.component('admTags', admTagsComponentOptions);

// FOODS
import admFoodsComponentOptions from './adm-foods/adm-foods.component';
import { AdmFoodsService } from './adm-foods/adm-foods.service';
import AdmFoodsController from './adm-foods/adm-foods.controller';

admItemsModule.service('AdmFoodsService', AdmFoodsService);
admItemsModule.controller('AdmFoodsController', AdmFoodsController);
admItemsModule.component('admFoods', admFoodsComponentOptions);

// MEASURES
import admMeasuresComponentOptions from './adm-measures/adm-measures.component';
import { AdmMeasuresService } from './adm-measures/adm-measures.service';
import AdmMeasuresController from './adm-measures/adm-measures.controller';

admItemsModule.service('AdmMeasuresService', AdmMeasuresService);
admItemsModule.controller('AdmMeasuresController', AdmMeasuresController);
admItemsModule.component('admMeasures', admMeasuresComponentOptions);

// COMMENTS
import admCommentsComponentOptions from './adm-comments/adm-comments.component';
import { AdmCommentsService } from './adm-comments/adm-comments.service';
import AdmCommentsController from './adm-comments/adm-comments.controller';

admItemsModule.service('AdmCommentsService', AdmCommentsService);
admItemsModule.controller('AdmCommentsController', AdmCommentsController);
admItemsModule.component('admComments', admCommentsComponentOptions);


let admItemsModuleName: string = admItemsModule.name;
export default admItemsModuleName;


