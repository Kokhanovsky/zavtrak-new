/** @ngInject */
export default function ($stateProvider: ng.ui.IStateProvider) {
    $stateProvider.state({
        name: 'adm-recipes',
        url: '/adm-service/adm-items/recipes',
        template: '<adm-recipes></adm-recipes>'
    });

    $stateProvider.state({
        name: 'adm-articles',
        url: '/adm-service/adm-items/articles',
        template: '<adm-articles></adm-articles>'
    });

    $stateProvider.state({
        name: 'adm-advices',
        url: '/adm-service/adm-items/advices',
        template: '<adm-advices></adm-advices>'
    });

    $stateProvider.state({
        name: 'adm-videos',
        url: '/adm-service/adm-items/videos',
        template: '<adm-videos></adm-videos>'
    });

    $stateProvider.state({
        name: 'adm-foods',
        url: '/adm-service/adm-items/foods',
        template: '<adm-foods></adm-foods>'
    });

    $stateProvider.state({
        name: 'adm-measures',
        url: '/adm-service/adm-items/measures',
        template: '<adm-measures></adm-measures>'
    });

    $stateProvider.state({
        name: 'adm-comments',
        url: '/adm-service/adm-items/comments',
        template: '<adm-comments></adm-comments>'
    });

    $stateProvider.state({
        name: 'adm-tags',
        url: '/adm-service/adm-items/tags',
        template: '<adm-tags></adm-tags>'
    });

}
