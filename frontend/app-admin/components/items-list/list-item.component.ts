import './items-list.sass';

let listItem: angular.IComponentOptions = {
    bindings: {
        item: '<',
        type: '<',
        editItem: '&',
        deleteItem: '&'
    },
    template: require('./list-item.html')
};

export default listItem;
