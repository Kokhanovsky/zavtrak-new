import ItemsListController from './items-list.controller';
import './items-list.sass';

let categoryComponent: angular.IComponentOptions = {
    bindings: {
        items: '<',
        type: '<',
        perPage: '<',
        total: '<',
        page: '=',
        query: '=',
        status: '=',
        getPage: '&',
        editItem: '&',
        deleteItem: '&'
    },
    template: require('./items-list.html'),
    controller:  ItemsListController
};

export default categoryComponent;
