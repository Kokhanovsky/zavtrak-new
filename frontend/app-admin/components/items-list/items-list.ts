import * as angular from "angular";

import itemsListComponentOptions from './items-list.component';
import listItemComponentOptions from './list-item.component';

let itemsListModule: ng.IModule = angular.module('itemsListModule', []);

itemsListModule.component('itemsList', itemsListComponentOptions);
itemsListModule.component('listItem', listItemComponentOptions);

let itemsListModuleName: string = itemsListModule.name;

export default itemsListModuleName;

