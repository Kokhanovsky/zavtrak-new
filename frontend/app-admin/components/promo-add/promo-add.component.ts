import PromoAddController from './promo-add.controller';
import './promo-add.sass';

let promoAddComponent: ng.IComponentOptions = {
    bindings: {
        onAdd: '&',
        editId: '<'
    },
    template: require('./promo-add.html'),
    controller:  PromoAddController
};

export default promoAddComponent;
