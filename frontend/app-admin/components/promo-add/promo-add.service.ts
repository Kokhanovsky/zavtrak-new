import * as angular from 'angular';
import { IPromo } from '../../../../backend/models/promo';

export class PromoAddService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(promoData: IPromo) {
        return this.$http(
            {
                method: 'POST',
                url: '/adm-service/promo',
                data: promoData
            }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http(
            {
                method: 'PUT',
                url: '/adm-service/promo',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    getPromo(id: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/adm-service/promo/edit',
                params: { id }
            })
        .then((response: any): any => {
            return response.data;
        });
    }

}
