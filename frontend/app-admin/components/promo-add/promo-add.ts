import * as angular from 'angular';

import promoAddComponentOptions from './promo-add.component';
import { PromoAddService } from './promo-add.service';

let promoAddModule: ng.IModule = angular.module('promoAdd', []);

promoAddModule.component('promoAdd', promoAddComponentOptions);
promoAddModule.service('PromoAddService', PromoAddService);

let promoAddModuleName: string = promoAddModule.name;

export default promoAddModuleName;


