import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { PromoAddService } from './promo-add.service';
import { IPromo } from '../../../../backend/models/promo';

class PromoAddController {

    value: string;
    options: any[];
    promoData: IPromo;
    editId: string;
    loading: boolean = false;
    sendform: boolean = false;
    onAdd: any;

    /** @ngInject */
    constructor(private PromoAddService: PromoAddService, private $rootScope: ng.IRootScopeService) {

    }

    $onInit() {
        if (this.editId) {
            this.loading = true;
            (this.$rootScope as any).loading = true;
            this.PromoAddService.getPromo(this.editId).then((promo: IPromo) => {
                this.promoData = promo;
            })
            .catch(console.log)
            .then(() => {
                (this.$rootScope as any).loading = false;
                this.loading = false;
            });
        }
    }

    add(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        let action = this.editId ? this.PromoAddService.edit(this.promoData) : this.PromoAddService.add(this.promoData);
        action
        .then(() => {
            this.onAdd();
        })
        .catch((err: any) => {
            console.log(err);
        }).then(() => {
            this.loading = false;
            (this.$rootScope as any).loading = false;
        });
    }
}

export default PromoAddController;
