import * as angular from "angular";
import * as moment from "moment";

import 'angular-material';
import 'angular-messages';
import 'angular-cookies';
import 'angular-recaptcha';
import 'angular-toastr';
import 'angular-drag-and-drop-lists';
import 'angular-paging';
import 'angular-youtube-embed'
import 'ng-idle';
import 'ng-file-upload';
import 'angular-sanitize';
import 'angular-local-storage';
import 'angular-scroll';
import 'angular-ui-router';


(window as any).tinycolor = require('tinycolor2/tinycolor.js');
import 'md-color-picker';

import 'ng-img-crop/compile/minified/ng-img-crop.js';

import 'hammerjs';

// import css styles
import '../app/cssmodules/index.sass';
import 'md-color-picker/dist/mdColorPicker.css';

// import all components
import componentsModuleName from './components/components';

// import all directives
import directivesModuleName from '../app/directives/directives';

// import libs
import libModuleName from '../app/lib/lib';

import { runBlock } from './app.run';

moment.locale("ru");

angular.module('app', [
    'ngMaterial',
    'ngMessages',
    'ngIdle',
    'vcRecaptcha',
    'toastr',
    'ngCookies',
    'ngFileUpload',
    'ngImgCrop',
    'dndLists',
    'bw.paging',
    'ngSanitize',
    'ngSanitize',
    'youtube-embed',
    'LocalStorageModule',
    'duScroll',
    'mdColorPicker',
    'ui.router',
    componentsModuleName,
    directivesModuleName,
    libModuleName])
.run(runBlock);

angular.bootstrap(document, ['app'], {
    strictDi: true
});
