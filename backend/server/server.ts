import { Request, Response, Express } from 'express';
import { Authorize } from '../lib/authorize';
import { Logger } from '../lib/logger';
import { Locals } from '../lib/locals';
import { ErrorHandler } from '../lib/error.handler';
import { Telegram } from '../lib/telegram';
import * as express from 'express';
import * as config from 'config';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as mongoose from 'mongoose';
import webpackConst from '../const';
import userStorage from './user.storage';
import Routes from '../pages/routes';

interface CsrfRequest extends Request {
    csrfToken: any;
}

let expressLayouts: any = require('express-ejs-layouts');
let csrf = require('csurf');

export class Server {

    authorize: Authorize = null;
    express: Express;
    locals: Locals;
    telegram: Telegram;
    categoriesTree: any;
    logger: Logger;
    listening: any;

    constructor() {
        this.express = express();
    }

    init(): Promise<any> {
        return this.mongooseConnect().then(() => {
            this.listen();
            this.authorize = new Authorize(this);
            this.logger = new Logger(this);
            this.locals = new Locals(this);
            this.telegram = new Telegram();
            Routes(this);
            userStorage(this);
            this.logger.logErrors();
            new ErrorHandler(this);
            return Promise.resolve();
        });
    }

    sendJson(res: express.Response, data: any) {
        res.status(200).header('Content-Type', 'application/json').json(data);
    }

    private mongooseConnect() {
        (mongoose as any).Promise = global.Promise;
        return mongoose.connect(config.get('mongoDB') as string);
    }

    private listen() {

        let app = this.express;

        let isLocal = config.get('env') === 'localhost';
        let port = config.get('port');

        app.use(express.static(webpackConst.publicPath));

        // We want to run the workflow only in dev mode
        if (isLocal) {

            const httpProxy = require('http-proxy');
            let proxy = httpProxy.createProxyServer();
            // We require the bundler inside the if block because
            // it is only needed in a development environment.
            let bundle = require('./bundle')();

            // Any requests to localhost:3000/build is proxied
            // to webpack-dev-server
            app.all(webpackConst.buildPathName + '*', (req: any, res: any) => {
                proxy.web(req, res, {
                    target: 'http://localhost:' + webpackConst.devPort
                });
            });

            // It is important to catch any errors from the proxy or the
            // server will crash. An example of this is connecting to the
            // server when webpack is bundling
            proxy.on('error', () => {
                console.log('Could not connect to proxy, please try again...');
            });

        }

        app.set('view engine', 'ejs');

        app.set('secret', config.get('secret'));

        app.disable('x-powered-by');

        app.use(cookieParser());

        app.use(expressLayouts);

        // use body parser so we can get info from POST and/or URL parameters
        app.use(bodyParser.urlencoded({extended: false}));

        app.use(bodyParser.json());

        // enable CSRF protection to /api/* routes
        let conditionalCSRF = (req: Request, res: Response, next: any) => {
            let xcsrf = csrf({cookie: true});
            let pattern = /^\/api\/.*$/g;
            if (pattern.test(req.path)) {
                xcsrf = csrf({
                    cookie: true,
                    ignoreMethods: ['GET', 'HEAD', 'OPTIONS']
                })
            }
            xcsrf(req, res, next);
        };

        app.use(conditionalCSRF);

        app.use((req: CsrfRequest, res: Response, next: any) => {
            res.cookie('XSRF-TOKEN', req.csrfToken());
            next();
        });

        app.use((req, res, next: any) => {
            if (req.url.match(/^\/(css|js|images|fonts|storage|build)\/.+/)) {
                res.setHeader('Cache-Control', 'public, max-age=31536000');
            }
            next();
        });

        this.listening = app.listen(port, () => {
            console.log('Server running on port ' + port);
        });
    }
}
