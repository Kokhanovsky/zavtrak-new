import { Request, Response } from "express";
import { Server } from '../../server/server';
import { ArticleController } from './article.controller';
import * as multer from "multer";
import { PhotoLib } from '../../lib/photo.lib';

export default (server: Server) => {
    let storage = multer.memoryStorage();
    let upload = multer({ storage, limits: { fileSize: 1024 * 1024 * 15 } });

    server.express.get(/^\/arts\/art([0-9]+)\.html/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            ctrl.renderArticle();
        });

    server.express.get('/articles',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            ctrl.renderArticlesList();
        });

    server.express.get(/(^\/articles\/bytag\/(.+)?)|(^\/articles(\/.+)?)/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            ctrl.renderArticlesList();
        });

    server.express.get('/article-add/:id?',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            ctrl.renderArticleAdd();
        });

    server.express.post('/api/article',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            return ctrl.add().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.put('/api/article',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            return ctrl.put().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.get('/api/article/edit',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ArticleController(req, res, next);
            return ctrl.model.getArticleById(req.query.id).then(data => {
                server.sendJson(res, data)
            }).catch(err => next(err));
        });

    server.express.post(
        '/api/article/photo',
        server.authorize.jwt('admin'),
        upload.array('files', 8),
        (req: Request, res: Response, next: any) => {
            let photoLib = new PhotoLib(req, res, next);
            photoLib.upload('article')
            .then(files => server.sendJson(res, files))
            .catch(next);
        });

    server.express.delete(
        '/api/article/photo',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let photoLib = new PhotoLib(req, res, next);
            photoLib.deleteImage(req.body.file._id, 'article').then(() => {
                server.sendJson(res, { status: 'ok' });
            }).catch(next);
        });
}
