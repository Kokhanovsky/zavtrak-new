import { Response, Request } from 'express';
import Article, { IArticle } from '../../models/article';
import { ArticleModel } from './article.model';
import { Common } from '../../lib/common';
import { ArticleContent } from './article.content';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IItemsList } from '../../lib/interfaces';
import * as mongoose from 'mongoose';
import File, { IFile } from '../../models/file';
import * as _ from 'underscore';
import { IComment } from '../../models/comment';

export class ArticleController {

    model: ArticleModel;
    content: ArticleContent;

    constructor(private req: Request, private res: Response, private next: any) {
        this.model = new ArticleModel(req);
        this.content = new ArticleContent();
    }

    renderArticleAdd() {
        this.model.getCategories().then(categories => {
            let id = this.req.params && this.req.params.id;
            let jsVars = [{ categories: JSON.stringify(categories) }];
            if (id) {
                this.model.getArticleById(id).then(() => {
                    this.res.render(require.resolve('./templates/article-add.template.ejs'), {
                        title: 'Редактирование статьи',
                        id,
                        jsVars
                    });
                }).catch(this.next);
            } else {
                this.res.render(require.resolve('./templates/article-add.template.ejs'), {
                    title: 'Добавление статьи',
                    id,
                    jsVars
                });
            }
        });
    }

    renderArticlesList() {
        this.model.getArticlesList().then((articles: IItemsList) => {
            let params = this.model.ArticleParams.params;
            let content = ArticleContent.getListContent(params);
            this.res.render(require.resolve('./templates/article-list.template.ejs'), {
                title: content.title,
                titleHead: content.titleHead,
                metaDescription: content.metaDescription,
                metaKeywords: content.metaKeywords,
                breadcrumbs: ArticleContent.getBreadcrumbsList(params),
                articles,
                params,
                queryParams: this.model.ArticleParams.getQueryParams()
            });
        }).catch(err => this.next(err));
    }

    renderArticle() {
        this.model.getArticle()
        .then((article: IArticle) => {
            if (!article) {
                let err = new Error('Article not found');
                (err as any).status = 404;
                return this.next(err);
            }
            if (article.status === 'denied') {
                this.res.status(404);
            }
            Promise.all([
                ItemsLib.getSimilarItems(article, 'article'),
                ItemsLib.getComments(article, 'article'),
                ItemsLib.getRating(article, 'article')]).then((d: any[]) => {
                this.res.render(require.resolve('./templates/article.template.ejs'), {
                    title: article.title,
                    metaDescription: Common.truncate(article.annot, 100),
                    metaKeywords: article.tags.map((t: any) => t.tag),
                    breadcrumbs: this.content.getBreadcrumbsArticle(article),
                    article,
                    similarItems: d[0],
                    comments: d[1],
                    rating: d[2]
                })
            }).catch(err => this.next(err))
        });
    }

    add() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let article: IArticle = new Article({
                user: this.req.user.id,
                category: reqItem.category,
                title: reqItem.title,
                content: reqItem.content,
                slug: Common.translit(Common.truncate(reqItem.content, 40)),
                tags: reqItem.tags,
                tag_string: reqItem.tags.join(' ')
            });
            article.save().then((article: IArticle) => {
                let photos = reqItem.photos ? _.flatten(reqItem.photos).filter(photo => !!photo) : [];
                this.addPhotos(article._id, photos).then(() => resolve(Common.getArticleUrl(article)));
            }).catch(reject);
        });
    }

    put() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let filter: any = { _id: reqItem._id };
            if (this.req.user.role !== 'admin') {
                filter['user'] = this.req.user.id;
            }
            Article.findOne(filter).then((article: IArticle) => {
                if (!article) {
                    reject();
                    let err: any = new Error('The article not found');
                    err.status = 404;
                    return this.next(err);
                }
                article.title = reqItem.title;
                article.category = reqItem.category;
                article.content = reqItem.content;
                article.tags = reqItem.tags;
                article.tag_string = reqItem.tags.join(' ');
                article.photos = reqItem.photos;
                article.save().then((article: IArticle) => {
                    resolve(Common.getArticleUrl(article));
                }).catch(reject);
            }).catch(reject);
        });
    }

    private addPhotos(item: mongoose.Schema.Types.ObjectId, photos: IFile[]) {
        return new Promise((resolve, reject) => {
            if (!photos || !photos.length) {
                return resolve();
            }
            let promises = [];
            photos.forEach(photo => {
                File.findOne({ _id: photo._id, 'user': this.req.user.id }).then(file => {
                    if (file) {
                        file.item = item;
                        file.description = photo.description;
                        promises.push(file.save());
                    }
                }).catch(err => reject(err));
            });
            Promise.all(promises).then(res => resolve()).catch(err => reject(err));
        });
    }
}
