import { Request } from 'express';
import { IParams, ArticleParams } from './article.params';
import Article, { IArticle } from '../../models/article';
import { IVideo } from '../../models/video';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IImage, IItemsList } from '../../lib/interfaces';
import { Common } from '../../lib/common';
import { IAdvice } from '../../models/advice';
import { IRecipe } from '../../models/recipe';
import ArticleCategory, { IArticleCategory } from '../../models/article-category';
import Comment, { IComment } from '../../models/comment';

export class ArticleModel {

    ArticleParams: ArticleParams;

    constructor(private req: Request) {
        this.ArticleParams = new ArticleParams(req);
    }

    static getImage(article: IArticle): IImage {
        let image: IImage;
        if (!article.photos.length) {
            return null;
        }
        image = {
            src: '/storage/cache/articles/' + article.photos[0]._id + '/300x290.jpg',
            description: article.photos[0].description
        };
        return image;
    }

    static getSimilar(item: IVideo | IRecipe | IArticle | IAdvice, limit = 100, exclude?: any[]): Promise<ISimilarItem[]> {
        let filters = ItemsLib.getSimilarFilters(item, exclude);
        return Article.find(filters,
            { score: { $meta: 'textScore' } })
        .sort({ score: { $meta: 'textScore' } })
        .select('title photos articleNumber')
        .populate('categories photos tags')
        .limit(limit)
        .lean().then((articles: any[]) => {
            let similarItems: ISimilarItem[] = [];
            articles.forEach(article => {
               similarItems.push({
                   title: article.title,
                   image: ArticleModel.getImage(article),
                   url: Common.getArticleUrl(article),
                   type: 'article',
                   score: article.score
               })
            });
            return similarItems;
        });
    }

    getArticlesList() {
        let perPage = 16;
        let filter: any = { status: 'approved' };
        return this.ArticleParams.getArticleListParams()
        .then((params: IParams) => {
                if (params.q) {
                    filter['$text'] = { $search: params.q };
                }
                if (params.tag) {
                    filter['tags'] = { $in: [params.tag._id] };
                }
                return Article.find(filter)
                .sort({ createdAt: -1 })
                .select('title slug photos articleNumber createdAt')
                .populate('categories photos')
                .limit(perPage)
                .skip(perPage * (params.p - 1))
                .lean()
                .then((data: any) => Article.count(filter).then(count => {
                    let res: IItemsList;
                    res = {
                        data,
                        paging: {
                            pageNum: params.p,
                            pagesCount: Math.ceil(count / perPage)
                        }
                    };
                    return res;
                }))
            }
        );

    }

    getArticle() {
        return this.ArticleParams.getArticleParams()
        .then((params: IParams) => Article.findOne({
            articleNumber: params.articleNumber
        })
        .populate('categories photos tags')
        .lean());
    }

    getArticleById(id: string) {
        let filter: any = { _id: id };
        if (this.req.user.role !== 'admin') {
            filter['user'] = this.req.user.id;
        }
        return new Promise((resolve, reject) => {
            Article.findOne(filter)
            .select('category tags photos slug articleNumber content title videos')
            .populate('photos')
            .lean()
            .then((article: any) => {
                if (!article) {
                    let error: any = new Error('Article not found');
                    error.status = 404;
                    return reject(error);
                } else {
                    //article.photos = this.processPhotos(article.photos);
                    return resolve(article);
                }
            })
            .catch(reject);
        });
    }

    getCategories(): Promise<IArticleCategory[]> {
        return ArticleCategory.find({}).lean();
    }

}
