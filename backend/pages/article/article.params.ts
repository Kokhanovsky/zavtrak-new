import { Request } from 'express';
import { Common } from '../../lib/common';
import Tag, { ITag } from '../../models/tag';

export interface IParams {
    q?: string;
    p?: number;
    sort?: string;
    articleNumber?: string;
    articleSlug?: string;
    tagSlug?: string;
    tag?: ITag;
}

export class ArticleParams {

    public params: IParams = {};

    constructor(private req: Request) {
    }

    getArticleListParams(): Promise<any> {
        let promises: Array<Promise<any>> = [ Promise.resolve() ];
        return Promise.all(promises).then(res => {
            this.params.p = this.req.query.p ? Math.abs(this.req.query.p * 1) : 1;
            if (this.req.query.q) {
                this.params.q = this.req.query.q;
            }
            if (this.req.params[1]) {
                this.params.tagSlug = Common.trimChar('/', this.req.params[1]);
                return this.getTagBySlug().then((tag: ITag) => {
                    this.params.tag = tag;
                    return this.params;
                });
            }
            return this.params;
        });
    }

    getArticleParams(): Promise<any> {
        let promises: Array<Promise<any>> = [ Promise.resolve() ];
        return Promise.all(promises).then(res => {
            this.params = {
                articleNumber: Common.trimChar('/', this.req.params[0])
            };
            return this.params;
        });
    }

    getQueryParams() {
        let params: any = {};
        if (this.params.p) {
            params.p = this.params.p;
        }
        if (this.params.q) {
            params.q = this.params.q;
        }
        return params;
    }

    private getTagBySlug() {
        return new Promise((resolve, reject) => {
            if (!this.req.params[1]) {
                return resolve(null);
            }
            let slug = Common.trimChar('/', this.req.params[1]);
            Tag.findOne({ slug })
            .then((tag: any) => {
                if (!tag) {
                    let error: any = new Error('Tag not found');
                    error.status = 404;
                    return reject(error);
                }
                return resolve(tag);
            }).catch(reject);
        });
    }

}
