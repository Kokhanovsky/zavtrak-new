import { IArticle } from '../../models/article';
import { IBreadcrumb, IContent } from '../../lib/interfaces';
import { IParams } from './article.params';
import * as path from 'path';

export class ArticleContent {

    constructor() {
    }

    static getBreadcrumbsList(params: IParams) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Блог', url: '/articles'});

        if (params.tag) {
            res.push({title: params.tag.tag, url: path.join('/articles/bytag', params.tag.slug)});
        }

        return res;
    }

    getBreadcrumbsArticle(article: IArticle) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Блог', url: '/articles'});
        res.push({title: article.title, url: null});
        return res;
    }

    static getListContent(params: IParams): IContent {
        let content: IContent = {};
        content.title = 'Кулинарный блог на Zavtraka.net';
        content.titleHead = 'Статьи';
        if (params.tag) {
            content.title = params.tag.tag + ' - кулинарный блог на Zavtraka.net';
            content.titleHead = "По тегу: " + params.tag.tag;
        }
        return content;
    }
}
