import { Response, Request } from 'express';
import { IVideo, default as Video } from '../../models/video';
import { VideoModel } from './video.model';
import { Common } from '../../lib/common';
import { VideoContent } from './video.content';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IItemsList } from '../../lib/interfaces';
import * as mongoose from 'mongoose';

export class VideoController {

    model: VideoModel;
    content: VideoContent;

    constructor(private req: Request, private res: Response, private next: any) {
        this.model = new VideoModel(req, res);
        this.content = new VideoContent();
    }

    renderVideoAdd() {
        let jsScripts = [
            '<script src="https://www.youtube.com/iframe_api"></script>'
        ];
        let id = this.req.params && this.req.params.id;
        if (id) {
            this.model.getVideoById(id).then(() => {
                this.res.render(require.resolve('./templates/video-add.template.ejs'), {
                    jsScripts,
                    title: 'Редактирование видео',
                    id
                });
            }).catch(this.next);
        } else {
            this.res.render(require.resolve('./templates/video-add.template.ejs'), {
                jsScripts,
                title: 'Добавление видео',
                id
            });
        }
    }

    renderVideosList() {
        this.model.getVideosList().then((videos: IItemsList) => {
            let params = this.model.VideoParams.params;
            let content = VideoContent.getListContent(params);
            this.res.render(require.resolve('./templates/videos-list.template.ejs'), {
                title: content.title,
                titleHead: content.titleHead,
                metaDescription: content.metaDescription,
                metaKeywords: content.metaKeywords,
                breadcrumbs: VideoContent.getBreadcrumbsList(params),
                videos,
                params,
                queryParams: this.model.VideoParams.getQueryParams()
            });
        }).catch(err => this.next(err));
    }

    renderVideo() {
        this.model.getVideo()
        .then((video: IVideo) => {
            if (!video) {
                let err: any = new Error('Video not found');
                err.status = 404;
                return this.next(err);
            }
            if (video.status === 'denied') {
                this.res.status(404);
            }
            Promise.all([
                ItemsLib.getSimilarItems(video, 'video'),
                ItemsLib.getComments(video, 'video'),
                ItemsLib.getRating(video, 'video')]).then((d: any[]) => {
                this.res.render(require.resolve('./templates/video.template.ejs'), {
                    title: video.title,
                    metaDescription: Common.truncate(video.description, 100),
                    metaKeywords: video.tags.map((t: any) => t.tag),
                    breadcrumbs: this.content.getBreadcrumbs(video),
                    video,
                    similarItems: d[0],
                    comments: d[1],
                    rating: d[2]
                })
            }).catch(err => this.next(err))
        });
    }

    add() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let video: IVideo = new Video({
                user: this.req.user.id,
                title: reqItem.title,
                video_id: reqItem.video_id,
                description: reqItem.description,
                categories: reqItem.categories,
                category_string: reqItem.categories.join(' '),
                tags: reqItem.tags,
                tag_string: reqItem.tags.join(' ')
            });
            video.save().then((video: IVideo) => {
                resolve(Common.getVideoUrl(video));
            }).catch(reject);
        });
    }

    put() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            Video.findOne({ _id: reqItem._id }).then((video: IVideo) => {
                if (!video) {
                    reject();
                    let err: any = new Error('The video not found');
                    err.status = 404;
                    return this.next(err);
                }
                video.title = reqItem.title;
                video.video_id = reqItem.video_id;
                video.categories = reqItem.categories;
                video.category_string = reqItem.categories.join(' ');
                video.description = reqItem.description;
                video.tags = reqItem.tags;
                video.tag_string = reqItem.tags.join(' ');
                video.save().then((video: IVideo) => {
                    resolve(Common.getVideoUrl(video));
                }).catch(reject);
            }).catch(reject);
        });

    }

}
