import { Request } from 'express';
import { IParams, VideoParams } from './video.params';
import Video, { IVideo } from '../../models/video';
import { IArticle } from '../../models/article';
import { IRecipe } from '../../models/recipe';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IImage, IItemsList } from '../../lib/interfaces';
import { Common } from '../../lib/common';
import { IAdvice } from '../../models/advice';


export class VideoModel {

    VideoParams: VideoParams;

    constructor(private req: Request, private res) {
        this.VideoParams = new VideoParams(req);
    }

    static getImage(video: IVideo): IImage {
        let image: IImage;
        image = {
            src: 'http://img.youtube.com/vi/'+ video.video_id + '/mqdefault.jpg',
            description: ''
        };
        return image;
    }

    static getSimilar(item: IVideo | IRecipe | IArticle | IAdvice, limit = 100, exclude?: any[]) {
        let filters = ItemsLib.getSimilarFilters(item, exclude);
        return Video.find(filters, { score: { $meta: "textScore" } })
        .sort({ score: { $meta: 'textScore' } })
        .select('title photos videoNumber video_id')
        .populate('categories photos')
        .limit(limit)
        .lean().then((videos: any[]) => {
            let similarItems: ISimilarItem[] = [];
            videos.forEach(video => {
                similarItems.push({
                    title: video.title,
                    image: VideoModel.getImage(video),
                    url: Common.getVideoUrl(video),
                    type: 'video',
                    score: video.score
                })
            });
            return similarItems;
        });
    }

    getVideosList() {
        let perPage = 16;
        let filter: any = { status: 'approved' };
        return this.VideoParams.getVideoListParams()
        .then((params: IParams) => {
            if (params.q) {
                filter['$text'] = { $search: params.q };
            }
            if (params.tag) {
                filter['tags'] = { $in: [params.tag._id] };
            }
            return Video.find(filter)
                .sort({ createdAt: -1 })
                .select('videoNumber title slug photos video_id createdAt')
                .populate('categories photos')
                .limit(perPage)
                .skip(perPage * (params.p - 1))
                .lean()
                .then((data: any) => Video.count(filter).then(count => {
                    let res: IItemsList = {
                        data,
                        paging: {
                            pageNum: params.p,
                            pagesCount: Math.ceil(count / perPage)
                        }
                    };
                    return res;
                }))
            }
        );

    }

    getVideo() {
        return this.VideoParams.getVideoParams()
        .then((params: IParams) => Video.findOne({
                videoNumber: params.videoNumber
            })
            .populate('categories photos tags')
            .lean());
    }

    getVideoById(id: string) {
        return new Promise((resolve, reject) => {
            Video.findOne({ _id: id })
            .then((video: any) => {
                if (!video) {
                    let error: any = new Error('Video not found');
                    error.status = 404;
                    return reject(error);
                } else {
                    return resolve(video);
                }
            })
            .catch(reject);
        });
    }

}
