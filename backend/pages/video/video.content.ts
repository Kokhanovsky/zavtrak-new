import { IBreadcrumb, IContent } from '../../lib/interfaces';
import { IVideo } from '../../models/video';
import { IParams } from './video.params';
import * as path from 'path';

export class VideoContent {

    constructor() {
    }

    getBreadcrumbs(video: IVideo) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Видеорецепты', url: '/videos'});
        res.push({title: video.title, url: null});
        return res;
    }

    static getBreadcrumbsList(params: IParams) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Видеорецепты', url: '/videos'});

        if (params.tag) {
            res.push({title: params.tag.tag, url: path.join('/videos/bytag', params.tag.slug)});
        }

        return res;
    }

    static getListContent(params: IParams): IContent {
        let content: IContent = {};
        content.title = 'Видеорецепты на Zavtraka.net';
        content.titleHead = 'Видеорецепты';
        if (params.tag) {
            content.title = params.tag.tag + ' - видеорецепты на Zavtraka.net';
            content.titleHead = "По тегу: " + params.tag.tag;
        }
        return content;
    }
}
