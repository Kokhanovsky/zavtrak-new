import { Request, Response } from "express";
import { Server } from '../../server/server';
import { VideoController } from './video.controller';

export default (server: Server) => {

    server.express.get('/video-add/:id?',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            ctrl.renderVideoAdd();
        });

    server.express.get(/^\/videos\/item([0-9]+)\/?/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            ctrl.renderVideo();
        });

    server.express.get('/videos',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            ctrl.renderVideosList();
        });

    server.express.get(/(^\/videos\/bytag\/(.+)?)|(^\/videos(\/.+)?)/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            ctrl.renderVideosList();
        });

    server.express.post('/api/video',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            return ctrl.add().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.put('/api/video',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            return ctrl.put().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.get('/api/video/edit',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new VideoController(req, res, next);
            return ctrl.model.getVideoById(req.query.id).then(data => server.sendJson(res, data)).catch(err => next(err));
        });

}
