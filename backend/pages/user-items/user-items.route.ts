import { Request, Response } from "express";
import { Server } from '../../server/server';
import { UserItemsController } from './user-items.controller';

export default (server: Server) => {

    server.express.get(['/user-items', '/user-items/recipes', '/user-items/articles', '/user-items/advices'],
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            res.render(require.resolve('./user-items.template.ejs'), {
                title: 'Мои публикации'
            });
        });

    server.express.get('/api/user-items/counts',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.getCounts().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.get('/api/user-recipes',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.getRecipes().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.get('/api/user-articles',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.getArticles().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.get('/api/user-advices',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.getAdvices().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/user-recipes',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.deleteRecipe().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/user-articles',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.deleteArticle().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/user-advices',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.deleteAdvice().then((data: any) => server.sendJson(res, data)).catch(next);
        });


}
