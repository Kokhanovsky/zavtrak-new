import { Request, Response } from 'express';
import { Server } from '../../server/server';
import Recipe, { IRecipe } from '../../models/recipe';
import Article, { IArticle } from '../../models/article';
import Advice, { IAdvice } from '../../models/advice';
import File, { IFile } from '../../models/file';
import * as config from 'config';
import * as del from 'del';
import * as path from 'path';

export class UserItemsController {

    private storageDir: string = config.get('storageDir').toString();
    private recipeImagesDir = 'recipes';
    private articleImagesDir = 'articles';

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    getRecipes() {
        return (Recipe as any).paginate({user: this.req.user._id, title: new RegExp(this.req.query.query, 'i') }, {
            page: this.req.query.page,
            select: 'title photos createdAt status slug recipeNumber',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        });
    }

    getArticles() {
        return (Article as any).paginate({user: this.req.user._id, title: new RegExp(this.req.query.query, 'i') }, {
            page: this.req.query.page,
            select: 'title photos createdAt status slug articleNumber',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        });
    }

    getAdvices() {
        return (Advice as any).paginate({user: this.req.user._id, content: new RegExp(this.req.query.query, 'i') }, {
            page: this.req.query.page,
            select: 'content createdAt status adviceNumber slug',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        });
    }

    getCounts() {
        let promises = [];
        promises.push(Recipe.count({user: this.req.user._id}));
        promises.push(Article.count({user: this.req.user._id}));
        promises.push(Advice.count({user: this.req.user._id}));
        return Promise.all(promises).then(res => {
            return {
                recipes: res[0],
                articles: res[1],
                advices: res[2]
            }
        });
    }

    deleteRecipe() {
        let recipeId: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Recipe.findOneAndRemove({user: this.req.user._id, _id: recipeId }).then((recipe: IRecipe) => {
                if (recipe) {
                    this.deleteImages(recipe.photos, 'recipe').then(() => {
                        console.log('Recipe images have been deleted');
                    });
                    return resolve();
                } else {
                    return reject(new Error('A recipe not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteArticle() {
        let articleId: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Article.findOneAndRemove({user: this.req.user._id, _id: articleId }).then((article: IArticle) => {
                if (article) {
                    this.deleteImages(article.photos, 'article').then(() => {
                        console.log('Article images have been deleted');
                    });
                    return resolve();
                } else {
                    return reject(new Error('An article not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteAdvice() {
        let recipeId: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Advice.findOneAndRemove({user: this.req.user._id, _id: recipeId }).then((advice: IAdvice) => {
                if (advice) {
                    return resolve();
                } else {
                    return reject(new Error('An advice not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteImages(files: IFile[], type: string) {
        let promises = [];
        let dir: string;
        switch(type) {
            case 'article':
                dir = this.articleImagesDir;
                break;
            case 'recipe':
                dir = this.recipeImagesDir;
                break;
            default:
                return Promise.reject(new Error('Item type not found'));
        }
        files.forEach((file: any) => {
            file = file.toString();
            let fl = File.findOne({ filename: file, type });
            promises.push(del(path.join(this.storageDir, dir, file + '.jpg')));
            promises.push(del(path.join(this.storageDir, 'cache', dir, file)));
            promises.push(fl.remove().exec());
        });
        return Promise.all(promises);
    }

}
