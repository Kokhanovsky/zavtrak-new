import { Request, Response }  from 'express';
import { Server } from '../../server/server';
import { ProfileController } from './profile.controller';
import { IUser } from '../../models/user';

export default (server: Server) => {

    server.express.get('/api/user',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.getUser()
                .then((user: IUser) => {
                    server.sendJson(res, user);
                })
                .catch(next);

        });

    server.express.put('/api/user',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.putUser(req)
                .then(() => {
                    server.sendJson(res, {'success': true});
                })
                .catch(next);

        });

    server.express.put('/api/user/password/change',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.changePassword(req)
            .then(() => {
                server.sendJson(res, {'status': 'ok'});
            })
            .catch((err: any) => {
                next(err);
            });

        });

    server.express.put('/api/user/email/change',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.changeEmail(req)
            .then(() => {
                server.sendJson(res, {'status': 'ok'});
            })
            .catch((err: any) => {
                next(err);
            });

        });

    server.express.put('/api/user/nickname/add',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.addNickname(req)
            .then(() => {
                server.sendJson(res, {'status': 'ok'});
            })
            .catch((err: any) => {
                next(err);
            });

        });

}
