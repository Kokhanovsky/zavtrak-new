import { Request, Response } from "express";
import { Server } from '../../server/server';
import * as multer from 'multer';
import { ProfileController } from './profile.controller';
import User from '../../models/user';
import { IUser } from '../../models/user';
let sharp = require('sharp');

export default (server: Server) => {

    var storage = multer.memoryStorage();
    let upload = multer({ storage, limits: { fileSize: 1024 * 1024 * 15 } });

    server.express.post(
        '/api/user/avatar/upload',
        server.authorize.jwt('user'),
        upload.array('files', 1),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ProfileController(req, res, next, server);
            ctrl.uploadAvatar()
            .then(file => server.sendJson(res, { 'status': 'ok' }))
            .catch(next);
        });

    server.express.post(
        '/api/user/avatar/save',
        server.authorize.jwt('user'),
        upload.single('avatar'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ProfileController(req, res, next, server);
            ctrl.avatarSave()
            .then(() => server.sendJson(res, { 'status': 'ok' }))
            .catch(next);
        });

    server.express.get(
        '/avatar/user/:id.jpg',
        (req: Request, res: Response, next: any) => {
            User.findById(req.params.id).then((user: IUser) => {
                res.writeHead(200, { 'Content-Type': 'image/jpg' });
                res.end(user.avatarBlob);
            });
        });

    server.express.delete(
        '/api/user/avatar/delete',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ProfileController(req, res, next, server);
            ctrl.avatarDelete()
            .then(() => server.sendJson(res, { 'status': 'ok' }))
            .catch(next);
        });

}
