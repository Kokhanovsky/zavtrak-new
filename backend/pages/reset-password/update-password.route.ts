import * as express from "express";
import { Server } from '../../server/server';
import { ResetPasswordController } from './reset-password.controller';

export default (server: Server) => {

    server.express.post('/api/reset/update-password', (req: express.Request, res: express.Response, next: any) => {

        let ctrl = new ResetPasswordController(req, res, next, server);
        ctrl.updatePassword().then(() => {
            server.sendJson(res, { success: true });
        }).catch(() => {
            let err = new Error();
            err.message = 'There is an error while updating password';
            next(err);
        });

    });

}
