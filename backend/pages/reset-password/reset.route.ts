import * as express from "express";
import { Server } from '../../server/server';
import { ResetPasswordController } from './reset-password.controller';

export default (server: Server) => {

    server.express.post('/api/reset/password', (req: express.Request, res: express.Response, next: any) => {

        let ctrl = new ResetPasswordController(req, res, next, server);
        ctrl.resetPassword()
            .then(() => server.sendJson(res, { 'email': 'sent' }))
            .catch(next);

    });

    server.express.get('/reset/password/token/:token', (req: express.Request, res: express.Response, next: any) => {

        let ctrl = new ResetPasswordController(req, res, next, server);
        ctrl.validateToken()
            .then((decoded: any) => res.redirect('/reset/update-password'))
            .catch(() => res.redirect('/reset/recovery-error'));

    });


}
