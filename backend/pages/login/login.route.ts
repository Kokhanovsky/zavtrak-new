import { Request, Response } from "express";
import { Server } from '../../server/server';

export default function (server: Server) {

    server.express.post('/api/login',
        (req: Request, res: Response, next: any) => {

            server.authorize.login(req.body.email, req.body.password, req, res, next);

        });

}
