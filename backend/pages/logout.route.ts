import * as express from "express";
import { Server } from '../server/server';

export default function (server: Server) {

    server.express.get('/api/logout', (req: express.Request, res: express.Response) => {
        server.authorize.logout(res);
        server.sendJson(res, { 'status': 'logged out' });
    });

}
