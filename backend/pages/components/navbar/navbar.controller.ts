import { Request } from 'express';
import * as fs from 'fs-extra';
import * as url from 'url';
let ejs: any = require('ejs');

interface IMenuItem {
    title: string;
    url: string;
    attr?: string;
    active?: boolean;
    auth?: boolean;
    noAuth?: boolean;
    admin?: boolean;
}

export class NavbarController {

    public menuItems: IMenuItem[] = [
        { title: 'Админ', url: '/adm-service/adm-items/recipes', auth: true, admin: true },
        { title: 'Добавить рецепт', url: '/recipe-add', attr: 'hide-xs', auth: true },
        { title: 'Мои публикации', url: '/user-items/recipes', /*attr: 'hide-xs',*/ auth: true },
        { title: 'Настройки', url: '/profile', attr: 'hide-xs', auth: true },
        { title: 'Рецепты', url: '/recipes', attr: 'hide-xs' },
        { title: 'Блог', url: '/articles', attr: 'hide-xs' },
        { title: 'Выйти', url: '/logout', auth: true },
        { title: 'Вход', url: '/login', noAuth: true }
    ];

    constructor(private req: Request) {
        this.setActive();
        this.filterItems();
    }

    private setActive() {
        this.menuItems.forEach((menuItem: IMenuItem) => menuItem.active = url.parse(this.req.originalUrl).pathname === menuItem.url);
    }

    private filterItems() {
        this.menuItems = this.menuItems.filter((item: IMenuItem) => {
            if (!this.req.user) { // not auth user
                return !item.auth;
            } else {
                return (!item.admin || (this.req.user.role === 'admin')) && !item.noAuth;
            }
        });
    }

}
