import { IAdvice } from '../../models/advice';
import { IBreadcrumb, IContent } from '../../lib/interfaces';
import { IParams } from './advice.params';
import * as path from 'path';

export class AdviceContent {

    constructor() {
    }

    static getBreadcrumbsAdvicesList(params: IParams) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Полезные советы', url: '/advices'});

        if (params.tag) {
            res.push({title: params.tag.tag, url: path.join('/advices/bytag', params.tag.slug)});
        }

        return res;
    }

    getBreadcrumbsAdvice(advice: IAdvice) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Полезные советы', url: '/advices'});
        res.push({title: 'Совет', url: null});
        return res;
    }

    static getListContent(params: IParams): IContent {
        let content: IContent = {};
        content.title = 'Полезные советы на Zavtraka.net';
        content.titleHead = 'Полезные советы';
        if (params.tag) {
            content.title = params.tag.tag + ' - полезные советы на Zavtraka.net';
            content.titleHead = "По тегу: " + params.tag.tag;
        }
        return content;
    }
}
