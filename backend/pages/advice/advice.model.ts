import { Request } from 'express';
import { IParams, AdviceParams } from './advice.params';
import Advice, { IAdvice } from '../../models/advice';
import { IRecipe } from '../../models/recipe';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IItemsList } from '../../lib/interfaces';
import { Common } from '../../lib/common';

export class AdviceModel {

    AdviceParams: AdviceParams;

    constructor(private req: Request) {
        this.AdviceParams = new AdviceParams(req);
    }

    static getSimilar(item: IAdvice | IRecipe | IAdvice, limit = 100, exclude?: any[]): Promise<ISimilarItem[]> {
        let filters = ItemsLib.getSimilarFilters(item, exclude);
        return Advice.find(filters,
            { score: { $meta: 'textScore' } })
        .sort({ score: { $meta: 'textScore' } })
        .select('title photos adviceNumber')
        .populate('categories photos tags')
        .limit(limit)
        .lean().then((advices: any[]) => {
            let similarItems: ISimilarItem[] = [];
            advices.forEach(advice => {
               similarItems.push({
                   title: advice.title,
                   url: Common.getAdviceUrl(advice),
                   type: 'advice',
                   score: advice.score
               })
            });
            return similarItems;
        });
    }

    getAdvicesList() {
        let perPage = 16;
        let filter: any = { status: 'approved' };
        return this.AdviceParams.getAdviceListParams()
        .then((params: IParams) => {
                if (params.q) {
                    filter['$text'] = { $search: params.q };
                }
                if (params.tag) {
                    filter['tags'] = { $in: [params.tag._id] };
                }
                return Advice.find(filter)
                .sort({ createdAt: -1 })
                .select('content slug adviceNumber createdAt')
                .populate('categories photos')
                .limit(perPage)
                .skip(perPage * (params.p - 1))
                .lean()
                .then((data: any) => Advice.count(filter).then(count => {
                    let res: IItemsList;
                    res = {
                        data,
                        paging: {
                            pageNum: params.p,
                            pagesCount: Math.ceil(count / perPage)
                        }
                    };
                    return res;
                }))
            }
        );

    }

    getAdvice() {
        return this.AdviceParams.getAdviceParams()
        .then((params: IParams) => Advice.findOne({
            adviceNumber: params.adviceNumber,
            slug: params.adviceSlug
        })
        .populate('photos tags user')
        .lean());
    }

    getAdviceById(id: string) {
        let filter: any = { _id: id };
        if (this.req.user.role !== 'admin') {
            filter['user'] = this.req.user.id;
        }
        return new Promise((resolve, reject) => {
            Advice.findOne(filter)
            .then((advice: any) => {
                if (!advice) {
                    let error: any = new Error('Advice not found');
                    error.status = 404;
                    return reject(error);
                } else {
                    return resolve(advice);
                }
            })
            .catch(reject);
        });
    }

}
