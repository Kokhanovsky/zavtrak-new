import { Response, Request } from 'express';
import { IAdvice, default as Advice } from '../../models/advice';
import { AdviceModel } from './advice.model';
import { Common } from '../../lib/common';
import { AdviceContent } from './advice.content';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IItemsList } from '../../lib/interfaces';

export class AdviceController {

    model: AdviceModel;
    content: AdviceContent;

    constructor(private req: Request, private res: Response, private next: any) {
        this.model = new AdviceModel(req);
        this.content = new AdviceContent();
    }

    renderAdviceAdd() {
        let id = this.req.params && this.req.params.id;
        if (id) {
            this.model.getAdviceById(id).then(() => {
                this.res.render(require.resolve('./templates/advice-add.template.ejs'), {
                    title: 'Редактирование совета',
                    id
                });
            }).catch(this.next);
        } else {
            this.res.render(require.resolve('./templates/advice-add.template.ejs'), {
                title: 'Добавление совета',
                id
            });
        }
    }


    renderAdvicesList() {
        this.model.getAdvicesList().then((advices: IItemsList) => {
            let params = this.model.AdviceParams.params;
            let content = AdviceContent.getListContent(params);
            this.res.render(require.resolve('./templates/advice-list.template.ejs'), {
                title: content.title,
                titleHead: content.titleHead,
                metaDescription: content.metaDescription,
                metaKeywords: content.metaKeywords,
                breadcrumbs: AdviceContent.getBreadcrumbsAdvicesList(params),
                advices,
                params,
                queryParams: this.model.AdviceParams.getQueryParams()
            });
        }).catch(err => this.next(err));
    }

    renderAdvice() {
        this.model.getAdvice()
        .then((advice: IAdvice) => {
            if (!advice) {
                let err = new Error('Advice not found');
                (err as any).status = 404;
                return this.next(err);
            }
            if (advice.status === 'denied') {
                this.res.status(404);
            }
            ItemsLib.getSimilarItems(advice, 'advice')
            .then((similarItems: ISimilarItem[]) => {
                this.res.render(require.resolve('./templates/advice.template.ejs'), {
                    title: Common.truncate(advice.content, 100),
                    metaKeywords: advice.tags.map((t: any) => t.tag),
                    breadcrumbs: this.content.getBreadcrumbsAdvice(advice),
                    advice,
                    similarItems
                })
            }).catch(err => this.next(err))
        });
    }

    add() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let advice: IAdvice = new Advice({
                user: this.req.user.id,
                content: reqItem.content,
                slug: Common.translit(Common.truncate(reqItem.content, 40)),
                tags: reqItem.tags,
                tag_string: reqItem.tags.join(' ')
            });
            advice.save().then((advice: IAdvice) => {
                resolve(Common.getAdviceUrl(advice));
            }).catch(reject);
        });
    }

    put() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let filter: any = { _id: reqItem._id };
            if (this.req.user.role !== 'admin') {
                filter['user'] = this.req.user.id;
            }
            Advice.findOne(filter).then((advice: IAdvice) => {
                if (!advice) {
                    reject();
                    let err: any = new Error('The advice not found');
                    err.status = 404;
                    return this.next(err);
                }
                advice.content = reqItem.content;
                advice.tags = reqItem.tags;
                advice.tag_string = reqItem.tags.join(' ');
                advice.save().then((advice: IAdvice) => {
                    resolve(Common.getAdviceUrl(advice));
                }).catch(reject);
            }).catch(reject);
        });

    }

}
