import { Request } from 'express';
import { Common } from '../../lib/common';
import Tag, { ITag } from '../../models/tag';

export interface IParams {
    q?: string;
    p?: number;
    sort?: string;
    adviceNumber?: string;
    adviceSlug?: string;
    tagSlug?: string;
    tag?: ITag;
}

export class AdviceParams {

    public params: IParams = {};

    constructor(private req: Request) {
    }

    getAdviceListParams(): Promise<any> {
        let promises: Array<Promise<any>> = [ this.getTagBySlug() ];
        return Promise.all(promises).then(d => {

            if (d[0]) {
                this.params.tag = d[0];
            }

            this.params.p = this.req.query.p ? Math.abs(this.req.query.p * 1) : 1;

            if (this.req.query.q)
                this.params.q = this.req.query.q;

            return this.params;
        });
    }

    getAdviceParams(): Promise<any> {
        let promises: Array<Promise<any>> = [ Promise.resolve() ];
        return Promise.all(promises).then(res => {
            this.params = {
                adviceNumber: Common.trimChar('/', this.req.params[0]),
                adviceSlug: Common.trimChar('/', this.req.params[1])
            };
            return this.params;
        });
    }

    getQueryParams() {
        let params: any = {};
        if (this.params.p) {
            params.p = this.params.p;
        }
        if (this.params.q) {
            params.q = this.params.q;
        }
        return params;
    }

    private getTagBySlug() {
        return new Promise((resolve, reject) => {
            if (!this.req.params[1]) {
                return resolve(null);
            }
            let slug = Common.trimChar('/', this.req.params[1]);
            Tag.findOne({ slug })
            .then((tag: any) => {
                if (!tag) {
                    let error: any = new Error('Tag not found');
                    error.status = 404;
                    return reject(error);
                }
                return resolve(tag);
            }).catch(reject);
        });
    }
}
