import * as express from "express";
import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { AdviceController } from './advice.controller';

export default (server: Server) => {

    server.express.get('/advice-add/:id?',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            ctrl.renderAdviceAdd();
        });

    server.express.get(/^\/advice\/([0-9]+)\/(.+)\/?/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: express.Request, res: express.Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            ctrl.renderAdvice();
        });

    server.express.get(/(^\/advices\/bytag\/(.+)?)|(^\/advices(\/.+)?)/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            ctrl.renderAdvicesList();
        });

    server.express.get('/advices',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            ctrl.renderAdvicesList();
        });

    server.express.post('/api/advice',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            return ctrl.add().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.put('/api/advice',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            return ctrl.put().then(url => server.sendJson(res, {url})).catch(err => next(err));
        });

    server.express.get('/api/advice/edit',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdviceController(req, res, next);
            return ctrl.model.getAdviceById(req.query.id).then(data => {
                server.sendJson(res, data)
            }).catch(err => next(err));
        });

}
