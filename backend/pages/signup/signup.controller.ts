import * as config from 'config';
import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { SendEmail } from '../../lib/send.email';
import { EmailVerification } from '../../lib/email.verification';
import User, { IUser } from '../../models/user';

let reCAPTCHA = require('recaptcha2');

let recaptcha = new reCAPTCHA({
    siteKey: config.get('recaptchaSiteKey'),
    secretKey: config.get('recaptchaSecretKey')
});

let tpl = require.resolve('./templates/signup.template.ejs');

export class SignUpController {

    private verificationSecret: string = config.get('verificationSecret').toString();
    private host: string = config.get('host').toString();
    private mail: SendEmail;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.mail = new SendEmail();
    }

    renderPage() {
        this.res.render(tpl, {
            title: 'Вход',
            user: this.req.user,
            recaptchaSiteKey: config.get('recaptchaSiteKey')
        });
    }

    validateEmail() {
        let email = this.req.body.value;
        User.findOne({ email }).then((user: IUser) => {
            if (!user) {
                this.server.sendJson(this.res, {
                    isValid: true,
                    value: 'ok'
                });
            } else {
                this.server.sendJson(this.res, {
                    isValid: false,
                    value: 'Ошибка валидации Email'
                });
            }
        }).catch((err: any) => this.next(err));
    }

    addUser() {
        let email = this.req.body.email;
        let password = this.req.body.password;
        let firstname = this.req.body.firstname;
        let lastname = this.req.body.lastname;
        let role = 'user';
        let emailVerify = new EmailVerification();
        return new Promise((resolve, reject) => {
            recaptcha.validate(this.req.body.captcha)
            .then(() => {
                User.findOne({ email })
                .then((user: any) => {
                    if (!user) {
                        let user = new User({ email, firstname, lastname, password, role });
                        user.save().then(() => {
                            this.server.telegram.noticeNewUser(user);
                            resolve();
                        }).catch(reject);
                        emailVerify.verify(user).then(() => {
                            console.log('Verification email has been sent');
                        });
                    } else {
                        reject(new Error('Такой Email уже зарегистрирован'));
                    }
                })
                .catch(reject);
            })
            .catch((err: any) => reject(new Error('Неверная Captcha')));
        });

    }
}
