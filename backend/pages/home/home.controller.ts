import { Request, Response } from 'express';
import { HomeModel } from './home.model';
let tpl = require.resolve('./home.template.ejs');

export class HomeController {

    model: HomeModel;

    constructor(private req: Request, private res: Response, private next: any) {
        this.model = new HomeModel(req);
    }

    renderPage() {
        let title = 'Zavtraka.net - видеорецепты, пошаговые рецепты с фото, кулинария';
        let metaDescription = 'Zavtraka.net - видеорецепты, пошаговые рецепты с фото, а также всё, что касается кухни, продуктов и приготовления пищи';
        let metaKeywords = [
            'кулинария'
        ];

        let promises = [
            this.model.getRecipesRecent(),
            this.model.getPromo()
        ];

        Promise.all(promises).then((d: any) => {

            let jsVars = [{ recipes: JSON.stringify(d[0]), promo: JSON.stringify(d[1]) }];

            this.res.render(tpl, {
                title,
                metaDescription,
                metaKeywords,
                jsVars
            });
        }).catch(err => this.next(err));
    }

}
