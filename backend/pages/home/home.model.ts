import Recipe from '../../models/recipe';
import { Request } from 'express';
import { Common } from '../../lib/common';
import * as _ from 'underscore';
import { IRating } from '../../models/rating';
import Promo from '../../models/promo';

export class HomeModel {

    constructor(private req: Request) {
    }

    getRecipesRecent(limit = 18) {
        return Recipe.aggregate([
            { $match: {  status: 'approved' } },
            { $sort: { createdAt: -1 } },
            { $limit: limit },
            {
                $lookup: {
                    from: 'ratings',
                    localField: '_id',
                    foreignField: 'item',
                    as: 'rating'
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'user',
                    foreignField: '_id',
                    as: 'user'
                }
            },
            { $unwind: '$user' },
            { $project: { title: 1, createdAt: 1, rating: 1, time: 1, photos: 1, slug: 1, recipeNumber: 1, user: 1 } }
        ]).then((data: any[]) => {
            data.forEach(d => {
                d.rating = _.reduce(d.rating, (sum, rating: IRating) => { return sum + rating.rate }, 0) / d.rating.length;
                d.rating = d.rating ? d.rating : 0;
                d.photo = d.photos && d.photos.length ? d.photos[0].toString() : null;
                if (d.user) {
                    d.user = Common.getUserData(d.user);
                }
                d.url = Common.getRecipeUrl(d);
                delete d.photos;
                delete d.slug;
                delete d.recipeNumber;
            });
            return data;
        });
    }

    getPromo() {
        return Promo.find({}).sort('order').select('header url content image color').lean();
    }

}
