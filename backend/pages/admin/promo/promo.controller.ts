import { Request, Response } from 'express';
import { Server } from '../../../server/server';
import * as del from 'del';
import * as fs from 'fs-extra';
import File, { IFile } from '../../../models/file';
import Promo, { IPromo } from '../../../models/promo';
import * as config from 'config';
import * as mongoose from 'mongoose';
import moment = require('moment');


let crypto = require('crypto');

let path = require('path');
let sharp = require('sharp');

export class PromoController {

    private storageDir: string = config.get('storageDir').toString();
    private promoDir = 'promo';

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    getItems() {
        return Promo.find({}).sort('order').lean();
    }

    getPromo(id: string) {
        return Promo.findOne({ _id: id });
    }

    add() {
        let reqPromo: any = this.req.body;
        return new Promise((resolve, reject) => {
            Promo.findOne().sort({ 'order': -1 }).then((p: IPromo) => {
                let order = p ? p.order + 1 : 0;
                let promo: IPromo = new Promo({
                    header: reqPromo.header,
                    color: reqPromo.color,
                    url: reqPromo.url,
                    content: reqPromo.content,
                    image: reqPromo.image,
                    order
                });
                (mongoose as any).Promise = global.Promise;
                promo.save().then((promo: IPromo) => {
                    return resolve();
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        })
    }

    edit() {
        let reqPromo: any = this.req.body;
        return new Promise((resolve, reject) => {
            Promo.findOne({ _id: reqPromo._id }).then((promo: IPromo) => {
                if (!promo) {
                    reject();
                    let err: any = new Error('The promo not found');
                    err.status = 404;
                    return this.next(err);
                }
                promo.header = reqPromo.header;
                promo.color = reqPromo.color;
                promo.url = reqPromo.url;
                promo.content = reqPromo.content;
                promo.image = reqPromo.image;
                promo.save().then(() => {
                    this.clearRemovedImages();
                    console.log('The promo has been modified');
                    return resolve();
                });
            }).catch(err => reject(err));
        });
    }

    remove() {
        let promoId: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Promo.findOneAndRemove({ _id: promoId }).then((promo: IPromo) => {
                if (promo) {
                    this.removeSlide(promo.image).then(() => {
                        console.log('Images have been deleted');
                    });
                    console.log('The promo has been deleted');
                    return resolve();
                } else {
                    return reject(new Error('A promo not found'));
                }
            }).catch(err => reject(err));
        });
    }

    changeOrder() {
        let ids: any = this.req.body.ids;
        return new Promise((resolve, reject) => {
            let promises = [];
            ids.forEach((id: string, index: number) => {
                promises.push(Promo.findOne({ _id: id }).then((promo: IPromo) => {
                    promo.order = index;
                    promo.save();
                }))
            });
            Promise.all(promises).then(() => {
                return resolve();
            }).catch(err => reject(err));
        });
    }

    uploadSlide() {
        let reqFile: any = this.req.file;
        return new Promise((resolve, reject) => {
            if (!reqFile) {
                return reject(new Error('Empty request'));
            }
            this.saveFile(reqFile).then((file: string) => {
                return resolve(file);
            }).catch(reject);
        });
    }

    removeSlide(fileId: string) {
        let fl = File.findOne({ _id: fileId, type: 'promo' });
        return Promise.all([
            del(path.join(this.storageDir, this.promoDir, fileId + '.jpg')),
            del(path.join(this.storageDir, 'cache', this.promoDir, fileId)),
            fl.remove().exec()
        ]);
    }

    private saveFile(file: any): Promise<string> {
        return new Promise((resolve, reject) => {
            let dir = path.resolve(path.join(this.storageDir, this.promoDir));
            fs.ensureDir(dir, err => {
                if (err) {
                    return reject(err);
                }
                this.processImage(file.buffer, dir)
                .then(fileHash => resolve(fileHash))
                .catch(err => reject(err));
            });
        });
    }

    private processImage(buffer: any, dir: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let id = mongoose.Types.ObjectId();
            let shrp = sharp(buffer);
            let fileName = path.join(dir, id + '.jpg');
            shrp.resize(1100, 1100);
            shrp.max().withoutEnlargement()
            .jpeg({ quality: 80 })
            .toFile(fileName)
            .then(() => {
                let file = new File({
                    _id: id,
                    type: 'promo',
                    user: this.req.user._id
                });
                file.save();
                resolve(id)
            }).catch(err => reject(err));
        });
    }

    public clearRemovedImages() {
        File.find({ type: 'promo' }).then((files: IFile[]) => {
            Promo.find({}).then((promos: IPromo[]) => {
                let whichHasAnItem = promos.map((promo: IPromo) => promo.image);
                let whichInFiles = files.map((file: IFile) => file._id.toString());
                let toRemove = whichInFiles.filter((inFile: string) => {
                    return whichHasAnItem.indexOf(inFile) === -1;
                });
                let promises = [];
                toRemove.forEach((file: string) => {
                    promises.push(this.removeSlide(file))
                });
                Promise.all(promises).then(() => {
                    console.log('images removed');
                })
            })
        })
    }

}
