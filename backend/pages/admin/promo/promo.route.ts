import { Request, Response } from "express";
import { Server } from "../../../server/server";
import { PromoController } from './promo.controller';
import * as multer from "multer";

export default (server: Server) => {

    let storage = multer.memoryStorage();
    let upload = multer({ storage, limits: { fileSize: 1024 * 1024 * 50 } });

    server.express.get('/adm-service/promo',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            res.render(require.resolve('./promo.ejs'), {
                title: 'Промо',
                layout: 'layout.ejs',
                admin: true
            });

        });

    server.express.get('/adm-service/promo/list',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.getItems()
            .then(list => {
                server.sendJson(res, list)
            })
            .catch(next);
        });

    server.express.post('/adm-service/promo',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.add()
            .then(list => server.sendJson(res, list))
            .catch(next);
        });

    server.express.put('/adm-service/promo',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.edit()
            .then(list => server.sendJson(res, list))
            .catch(next);
        });

    server.express.delete(
        '/adm-service/promo',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.remove()
            .then(hash => server.sendJson(res, { hash }))
            .catch(next);
        });

    server.express.get(
        '/adm-service/promo/edit',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.getPromo(req.query.id)
            .then(data => server.sendJson(res, data))
            .catch(next);
        });

    server.express.put(
        '/adm-service/promo/change-order',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.changeOrder()
            .then(hash => server.sendJson(res, { hash }))
            .catch(next);
        });

    server.express.post(
        '/adm-service/promo/photo',
        server.authorize.jwt('admin'),
        upload.single('file'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.uploadSlide()
            .then(hash => server.sendJson(res, { hash }))
            .catch(next);
        });

    server.express.delete(
        '/adm-service/promo/photo',
        server.authorize.jwt('admin'),
        upload.single('file'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PromoController(req, res, next, server);
            ctrl.removeSlide(req.body.file)
            .then(hash => server.sendJson(res, { hash }))
            .catch(next);
        });

}
