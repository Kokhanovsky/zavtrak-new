import { Request, Response } from 'express';
import { Server } from '../../../server/server';
import Recipe, { IRecipe } from '../../../models/recipe';
import Article, { IArticle } from '../../../models/article';
import Advice, { IAdvice } from '../../../models/advice';
import File, { IFile } from '../../../models/file';
import * as config from 'config';
import * as del from 'del';
import * as path from 'path';
import Video, { IVideo } from '../../../models/video';
import Tag, { ITag } from '../../../models/tag';
import Measure, { IMeasure } from '../../../models/measure';
import Comment, { IComment } from '../../../models/comment';
import { default as Food, IFood } from '../../../models/food';
import * as _ from 'underscore';
import { Common } from '../../../lib/common';

export class AdmItemsController {

    private storageDir: string = config.get('storageDir').toString();
    private recipeImagesDir = 'recipes';
    private articleImagesDir = 'articles';

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    getRecipes() {
        return (Recipe as any).paginate(this.getFilters('title'), {
            page: this.req.query.page,
            select: 'title photos createdAt status slug recipeNumber user',
            populate: 'user',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        }).then(data => this.transformResponse(data));
    }

    getArticles() {
        return (Article as any).paginate(this.getFilters('title'), {
            page: this.req.query.page,
            select: 'title photos createdAt status slug articleNumber user',
            populate: 'user',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        }).then(data => this.transformResponse(data));
    }

    //-----------------------

    getAdvices() {
        if (this.req.query.id) {
            return Advice.findOne({_id: this.req.query.id}).then(data => this.transformResponse(data));
        } else {
            return (Advice as any).paginate(this.getFilters('content'), {
                page: this.req.query.page,
                select: 'content createdAt status user adviceNumber slug',
                populate: 'user',
                limit: 10,
                lean: true,
                sort: '-createdAt'
            }).then(data => this.transformResponse(data));
        }
    }

    putAdvice() {
        return new Promise((resolve, reject) => {
            Advice.findOne({_id: this.req.body.advice._id}).then((advice: IAdvice) => {
                advice.content = this.req.body.advice.content;
                advice.save().then((advice: IAdvice) => {
                    return resolve(advice);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    //-----------------------

    getVideos() {
        return (Video as any).paginate(this.getFilters('title'), {
            page: this.req.query.page,
            select: 'title video_id createdAt status user videoNumber',
            populate: 'user',
            limit: 10,
            lean: true,
            sort: '-createdAt'
        }).then(data => this.transformResponse(data));
    }

    //-----------------------

    getTags() {
        if (this.req.query.id) {
            return Tag.findOne({_id: this.req.query.id}).then(data => this.transformResponse(data));
        } else {
            return (Tag as any).paginate(this.getFilters('tag'), {
                page: this.req.query.page,
                select: 'tag createdAt status user',
                populate: 'user',
                limit: 10,
                lean: true,
                sort: '-createdAt'
            }).then(data => this.transformResponse(data));
        }
    }

    putTag() {
        return new Promise((resolve, reject) => {
            Tag.findOne({_id: this.req.body.tag._id}).then((tag: ITag) => {
                tag.tag = this.req.body.tag.tag;
                tag.save().then((tag: ITag) => {
                    return resolve(tag);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    //-----------------------

    getFoods() {
        if (this.req.query.id) {
            return Food.findOne({_id: this.req.query.id}).then(data => this.transformResponse(data));
        } else {
            return (Food as any).paginate(this.getFilters('food'), {
                page: this.req.query.page,
                select: 'food createdAt status user',
                populate: 'user',
                limit: 10,
                lean: true,
                sort: '-createdAt'
            }).then(data => this.transformResponse(data));
        }
    }

    putFood() {
        return new Promise((resolve, reject) => {
            Food.findOne({_id: this.req.body.food._id}).then((food: IFood) => {
                food.food = this.req.body.food.food;
                food.save().then((food: IFood) => {
                    return resolve(food);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    //-----------------------

    getMeasures() {
        if (this.req.query.id) {
            return Measure.findOne({_id: this.req.query.id}).then(data => this.transformResponse(data));
        } else {
            return (Measure as any).paginate(this.getFilters('measure'), {
                page: this.req.query.page,
                select: 'measure createdAt status user',
                populate: 'user',
                limit: 10,
                lean: true,
                sort: '-createdAt'
            }).then(data => this.transformResponse(data));
        }
    }

    putMeasure() {
        return new Promise((resolve, reject) => {
            Measure.findOne({_id: this.req.body.measure._id}).then((measure: IMeasure) => {
                measure.measure = this.req.body.measure.measure;
                measure.save().then((measure: IMeasure) => {
                    return resolve(measure);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    //-----------------------

    getComments() {
        if (this.req.query.id) {
            return Comment.findOne({_id: this.req.query.id}).then(data => this.transformResponse(data));
        } else {
            return (Comment as any).paginate(this.getFilters('comment'), {
                page: this.req.query.page,
                select: 'comment createdAt status user',
                populate: 'user',
                limit: 10,
                lean: true,
                sort: '-createdAt'
            }).then(data => this.transformResponse(data));
        }
    }

    putComment() {
        return new Promise((resolve, reject) => {
            Comment.findOne({_id: this.req.body.comment._id}).then((comment: IComment) => {
                comment.comment = this.req.body.comment.comment;
                comment.save().then((comment: IComment) => {
                    return resolve(comment);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    //-----------------------

    getCounts() {
        let promises = [];
        promises.push(Recipe.count({}));
        promises.push(Article.count({}));
        promises.push(Advice.count({}));
        promises.push(Video.count({}));
        promises.push(Tag.count({}));
        promises.push(Food.count({}));
        promises.push(Measure.count({}));
        promises.push(Comment.count({}));

        promises.push(Recipe.count({status: 'new'}));
        promises.push(Article.count({status: 'new'}));
        promises.push(Advice.count({status: 'new'}));
        promises.push(Video.count({status: 'new'}));
        promises.push(Tag.count({status: 'new'}));
        promises.push(Food.count({status: 'new'}));
        promises.push(Measure.count({status: 'new'}));
        promises.push(Comment.count({status: 'new'}));

        return Promise.all(promises).then(res => {
            return {
                recipes: res[0],
                articles: res[1],
                advices: res[2],
                videos: res[3],
                tags: res[4],
                foods: res[5],
                measures: res[6],
                comments: res[7],
                recipesNew: res[8],
                articlesNew: res[9],
                advicesNew: res[10],
                videosNew: res[11],
                tagsNew: res[12],
                foodsNew: res[13],
                measuresNew: res[14],
                commentsNew: res[15],
            }
        });
    }

    setStatus() {
        return new Promise((resolve, reject) => {
            if (!this.req.body || !this.req.body.itemId || !this.req.body.status || !this.req.body.type) {
                return reject(new Error('Empty params'));
            }
            let model;
            let type = this.req.body.type;
            let status = this.req.body.status;
            switch (type) {
                case 'recipe':
                    model = Recipe;
                    break;
                case 'article':
                    model = Article;
                    break;
                case 'advice':
                    model = Advice;
                    break;
                case 'video':
                    model = Video;
                    break;
                case 'tag':
                    model = Tag;
                    break;
                case 'food':
                    model = Food;
                    break;
                case 'measure':
                    model = Measure;
                    break;
                case 'comment':
                    model = Comment;
                    break;
            }
            model.findById(this.req.body.itemId).then((model: any) => {
                if (!model) {
                    return reject(new Error('The item not found'));
                }
                if (((type === 'article') || (type === 'recipe')) && (status === 'approved')) {
                    model.createdAt = new Date();
                }
                model.status = status;
                model.save();
                resolve();
            });
        })
    }

    deleteRecipe() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Recipe.findOneAndRemove({ _id: id }).then((recipe: IRecipe) => {
                if (recipe) {
                    this.deleteImages(recipe.photos, 'recipe').then(() => {
                        console.log('Recipe images have been deleted');
                    });
                    return resolve();
                } else {
                    return reject(new Error('A recipe not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteArticle() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Article.findOneAndRemove({ _id: id }).then((article: IArticle) => {
                if (article) {
                    this.deleteImages(article.photos, 'article').then(() => {
                        console.log('Article images have been deleted');
                    });
                    return resolve();
                } else {
                    return reject(new Error('An article not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteAdvice() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Advice.findOneAndRemove({ _id: id }).then((advice: IAdvice) => {
                if (advice) {
                    return resolve();
                } else {
                    return reject(new Error('An advice not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteVideo() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Video.findOneAndRemove({ _id: id }).then((video: IVideo) => {
                if (video) {
                    return resolve();
                } else {
                    return reject(new Error('The video not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteTag() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Tag.findOneAndRemove({ _id: id }).then((tag: ITag) => {
                if (tag) {
                    return resolve();
                } else {
                    return reject(new Error('The tag not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteFood() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Food.findOneAndRemove({ _id: id }).then((food: IFood) => {
                if (food) {
                    return resolve();
                } else {
                    return reject(new Error('The food not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteMeasure() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Measure.findOneAndRemove({ _id: id }).then((measure: IMeasure) => {
                if (measure) {
                    return resolve();
                } else {
                    return reject(new Error('The measure not found'));
                }
            }).catch(err => reject(err));
        });
    }

    deleteComment() {
        let id: any = this.req.body.id;
        return new Promise((resolve, reject) => {
            Comment.findOneAndRemove({ _id: id }).then((comment: IComment) => {
                if (comment) {
                    return resolve();
                } else {
                    return reject(new Error('The comment not found'));
                }
            }).catch(err => reject(err));
        });
    }


    deleteImages(files: IFile[], type: string) {
        let promises = [];
        let dir: string;
        switch(type) {
            case 'article':
                dir = this.articleImagesDir;
                break;
            case 'recipe':
                dir = this.recipeImagesDir;
                break;
            default:
                return Promise.reject(new Error('Item type not found'));
        }
        files.forEach((file: any) => {
            file = file.toString();
            let fl = File.findOne({ filename: file, type });
            promises.push(del(path.join(this.storageDir, dir, file + '.jpg')));
            promises.push(del(path.join(this.storageDir, 'cache', dir, file)));
            promises.push(fl.remove().exec());
        });
        return Promise.all(promises);
    }

    private getFilters(field: string) {
        let filter: any = {};
        if (this.req.query.query) {
            filter[field] = new RegExp(this.req.query.query, 'i');
        }
        if (this.req.query.status) {
            let status = this.req.query.status;
            if (!_.isArray(status)) {
                status = [status];
            }
            filter.status = { $in: status };
        }
        return filter
    }

    private transformResponse(data: any) {
        if (data.docs) {
            data.docs = data.docs.map(item => {
                item.user = Common.getUserData(item.user);
                return item;
            });
        }
        if (data.user) {
            data.user = Common.getUserData(data.user);
        }
        return data;
    }

}
