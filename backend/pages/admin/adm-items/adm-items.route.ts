import { Request, Response } from "express";
import { Server } from '../../../server/server';
import { AdmItemsController } from './adm-items.controller';

export default (server: Server) => {

    server.express.get(['/adm-service/adm-items/recipes',
                        '/adm-service/adm-items/articles',
                        '/adm-service/adm-items/advices',
                        '/adm-service/adm-items/videos',
                        '/adm-service/adm-items/tags',
                        '/adm-service/adm-items/foods',
                        '/adm-service/adm-items/measures',
                        '/adm-service/adm-items/comments'
        ],
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            res.render(require.resolve('./adm-items.ejs'), {
                title: 'Публикации',
                layout: 'layout.ejs',
                admin: true
            });
        });

    server.express.get('/api/adm-items/counts',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getCounts().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.get('/api/adm-items/recipes',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getRecipes().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.get('/api/adm-items/articles',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getArticles().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/advices',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getAdvices().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.put('/api/adm-items/advices',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.putAdvice().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/videos',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getVideos().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/tags',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getTags().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.put('/api/adm-items/tags',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.putTag().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/foods',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getFoods().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.put('/api/adm-items/foods',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.putFood().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/measures',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getMeasures().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.put('/api/adm-items/measures',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.putMeasure().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.get('/api/adm-items/comments',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.getComments().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.put('/api/adm-items/comments',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.putComment().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    //-------------------

    server.express.delete('/api/adm-items/recipes',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteRecipe().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/articles',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteArticle().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/advices',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteAdvice().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/comments',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteComment().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/videos',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteVideo().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/tags',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteTag().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/foods',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteFood().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.delete('/api/adm-items/measures',
        server.authorize.jwt('admin'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.deleteMeasure().then((data: any) => server.sendJson(res, data)).catch(next);
        });

    server.express.post('/api/moderate/status',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AdmItemsController(req, res, next, server);
            ctrl.setStatus().then((data: any) => {
                server.sendJson(res, {status: 'ok'});
            }).catch(next);
        });

}
