import { Server } from "../server/server";

let routes = [
    'admin/promo/promo.route.js',
    'admin/users/users.route.js',
    'admin/adm-items/adm-items.route.js',

    'api/categories.route.js',
    'api/promo.route.js',
    'api/tags.route.js',
    'api/measures.route.js',
    'api/foods.route.js',
    'api/rating.route.js',

    'authstatus.route.js',
    'home/home.route.js',
    'article/article.route.js',
    'recipe/recipe.route.js',
    'video/video.route.js',
    'advice/advice.route.js',

    'agreement/agreement.route.js',
    'login/login.page.route.js',
    'login/login.route.js',
    'logout/logout.route.js',
    'logout.route.js',

    'sitemap/sitemap.route.js',
    'robots-txt/robots-txt.route.js',

    'profile/avatar-upload.route.js',
    'profile/profile.page.route.js',
    'profile/profile.route.js',
    'reset-password/reset-password.page.route.js',
    'reset-password/reset.route.js',
    'reset-password/update-password.page.route.js',
    'reset-password/update-password.route.js',
    'signup/email.validate.route.js',
    'signup/signup.adduser.route.js',
    'signup/signup.page.route.js',
    'signup/verification.route.js',
    'social/facebook.route.js',
    'social/google.route.js',
    'social/instagram.route.js',
    'social/odnoklassniki.route.js',
    'social/vkontakte.route.js',
    'user-items/user-items.route.js',
    'user-page/user-page.route.js',

    // location is important because of routes priority

];

export default (server: Server) => {
    routes.forEach((route: string) => require(`${__dirname}/../pages/` + route).default(server));
}
