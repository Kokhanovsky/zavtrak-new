import * as express from "express";
import { Server } from '../../server/server';
import Category from '../../models/category';

export default (server: Server) => {

    server.express.get('/api/categories',
        (req: express.Request, res: express.Response, next: any) => {

            Category.GetFullArrayTree((err: any, tree: any) => {
                res.json(tree);
            });

        });

}
