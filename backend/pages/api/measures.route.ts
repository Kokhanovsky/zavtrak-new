import * as express from 'express';
import { Server } from '../../server/server';
import Measure, { IMeasure } from '../../models/measure';
let mongoose = require('mongoose');

export default (server: Server) => {

    server.express.get('/api/measures',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response) => {
            let q = req.query.q ? req.query.q : '';
            Measure.find({
                measure: {$regex: q, $options: 'i'},
                $or: [{ status: 'approved' }, { user: req.user._id}]
            }).lean().select('measure').limit(10).then((measures: IMeasure[]) => {
                res.json(measures);
            });

        });

    server.express.post('/api/measures/ids',
        (req: express.Request, res: express.Response) => {
            let ids = req.body.ids.map(id => mongoose.Types.ObjectId(id));
            Measure.find({'_id': {$in: ids}}).lean().select('measure').then((measures: IMeasure[]) => {
                res.json(measures);
            });
        });

    server.express.post('/api/measures',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response, next: any) => {

            let newMeasure = req.body.measure;

            if (!newMeasure) {
                return next(new Error('Empty measure'));
            }

            Measure.findOne({measure: { $regex : new RegExp('^' + newMeasure + '$', "i") } }).then((measure: IMeasure) => {
                if (!measure) {
                    let newMeasureModel = new Measure({
                        'measure': newMeasure.toLowerCase(),
                        'user': req.user._id
                    });
                    newMeasureModel.save().then((measure: IMeasure) => res.json({ _id: measure._id, measure: measure.measure }));
                } else {
                    return res.json({ _id: measure._id, measure: measure.measure });
                }
            })

        });

}
