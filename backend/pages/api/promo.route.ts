import { Request, Response } from "express";
import { Server } from "../../server/server";
import Promo from '../../models/promo';

export default (server: Server) => {

    server.express.get('/api/promo',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            return Promo.find({}).sort('order').select('header url content image color').lean().then(promo => {
                server.sendJson(res, promo);
            });
        });

}
