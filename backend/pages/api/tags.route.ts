import * as express from 'express';
import { Server } from '../../server/server';
import Tag, { ITag } from '../../models/tag';
import { Common } from '../../lib/common';
let mongoose = require('mongoose');

export default (server: Server) => {

    server.express.get('/api/tags',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response) => {
            let q = req.query.q ? req.query.q : '';
            Tag.find({
                tag: { $regex: q, $options: 'i' },
                $or: [{ status: 'approved' }, { user: req.user._id}]
            })
            .lean().select('tag').limit(10).then((tags: ITag[]) => {
                res.json(tags);
            });

        });

    server.express.post('/api/tags/ids',
        (req: express.Request, res: express.Response) => {
            let ids = req.body.ids.map(id => mongoose.Types.ObjectId(id));
            Tag.find({ '_id': { $in: ids } }).lean().select('tag').then((tags: ITag[]) => {
                res.json(tags);
            });
        });

    server.express.post('/api/tags',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response, next: any) => {

            let newTag = req.body.tag;

            if (!newTag) {
                return next(new Error('Empty tag'));
            }

            Tag.findOne({ tag: { $regex: new RegExp('^' + newTag + '$', "i") } }).then((tag: ITag) => {
                if (!tag) {
                    let newTagModel = new Tag({
                        'tag': newTag.toLowerCase(),
                        'slug': Common.translit(newTag),
                        'user': req.user._id
                    });
                    newTagModel.save().then((tag: ITag) => res.json({ _id: tag._id, tag: tag.tag }));
                } else {
                    return res.json({ _id: tag._id, tag: tag.tag });
                }
            })

        });

}
