import * as express from 'express';
import { Server } from '../../server/server';
import Recipe from '../../models/recipe';
import Article from '../../models/article';
import Video from '../../models/video';
import Rating from '../../models/rating';

export default (server: Server) => {

    server.express.post('/api/rating',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response, next: any) => {
            let item = req.body.item;
            let rating = parseInt(req.body.rating);
            let type = req.body.itemType;
            let model;
            switch (type) {
                case 'recipe':
                    model = Recipe;
                    break;
                case 'article':
                    model = Article;
                    break;
                case 'video':
                    model = Video;
                    break;
            }
            if (!model || !rating || !item) {
                return next(new Error('Wrong parameters values'));
            }
            model.find({_id: item}).then(() => {
                Rating.findOne({item, type, user: req.user.id}).remove().exec().then(() => {
                    let rt = new Rating({
                        item,
                        type,
                        rate: rating,
                        user: req.user.id
                    });
                    rt.save().then(() => {
                        res.json({'status': 'ok'});
                    }).catch((err) => next(err));
                }).catch((err) => next(err));
            });
        });

}
