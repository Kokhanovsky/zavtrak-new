import * as express from 'express';
import { Server } from '../../server/server';
import Food, { IFood } from '../../models/food';
import { Common } from '../../lib/common';

let mongoose = require('mongoose');

export default (server: Server) => {

    server.express.get('/api/foods',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response) => {
            let food = req.query.food ? req.query.food : '';
            Food.find({
                food: { $regex: food, $options: 'i' },
                $or: [{ status: 'approved' }, { user: req.user._id }]
            }).lean().select('food').limit(10).then((foods: IFood[]) => {
                res.json(foods);
            });

        });

    server.express.post('/api/foods/ids',
        (req: express.Request, res: express.Response) => {
            let ids = req.body.ids.map(id => mongoose.Types.ObjectId(id));
            Food.find({ '_id': { $in: ids } }).lean().select('food').then((foods: IFood[]) => {
                res.json(foods);
            });
        });

    server.express.post('/api/foods',
        server.authorize.jwt('user'),
        (req: express.Request, res: express.Response, next: any) => {

            let newFood = req.body.food;

            if (!newFood) {
                return next(new Error('Empty food'));
            }

            Food.findOne({ food: { $regex: new RegExp('^' + newFood + '$', "i") } }).then((food: IFood) => {
                if (!food) {
                    let newFoodModel = new Food({
                        'food': newFood,
                        'slug': Common.translit(newFood),
                        'user': req.user._id
                    });
                    newFoodModel.save().then((food: IFood) => res.json({
                        _id: food._id,
                        food: food.food
                    }));
                } else {
                    return res.json({ _id: food._id, food: food.food });
                }
            })

        });

}
