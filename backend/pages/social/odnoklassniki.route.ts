import { Request, Response } from 'express';
import { Server } from '../../server/server';
import User, { IUser } from '../../models/user';
import * as config from 'config';

import passport = require('passport');
let OdnoklassnikiStrategy = require('passport-ok-strategy').Strategy;

interface IOdnoklassnikiConfig {
    clientID: string;
    clientPublic: string;
    clientSecret: string;
    callbackURL: string;
}

// Routes
export default function (server: Server) {

    let okConfig: IOdnoklassnikiConfig = config.get('odnoklassnikiAuth') as any;
    let host = config.get('host');

    passport.use(new OdnoklassnikiStrategy({
            clientID: okConfig.clientID,
            clientPublic: okConfig.clientPublic,
            clientSecret: okConfig.clientSecret,
            callbackURL: host + okConfig.callbackURL
        },

        // odnoklassniki will send back the token and profile
        (token: string, refreshToken: string, profile: any, done: any) => {

            process.nextTick(() => {

                User.findOne({ odnoklassniki: profile.id }).then((user: IUser) => {
                    if (user) {
                        done(null, user)
                    } else {
                        let name: string[] = profile.displayName.split(' ');
                        let firstname = name[0] ? name[0] : '';
                        let lastname = name[1] ? name[1] : '';
                        let user = new User({
                            email: profile.email,
                            firstname,
                            lastname,
                            role: 'user',
                            odnoklassniki: profile.id
                        });
                        user.save((err: any) => {
                            if (err) {
                                done(err);
                            }
                            server.telegram.noticeNewUser(user);
                            done(null, user);
                        });
                    }
                }).catch((err: any) => done(err));

            });
        }));

    server.express.get('/auth/odnoklassniki', (req: any, res: any, next: any) => {
        if (req.query.redir) {
            res.cookie('social-redir', req.query.redir);
        }
        next();
    }, passport.authenticate('odnoklassniki'));

    server.express.get(okConfig.callbackURL,
        passport.authenticate('odnoklassniki', { failureRedirect: '/login' }),
        (req: Request, res: Response) => {
            server.authorize.setToken(req.user, req, res);
            let url = req.cookies['social-redir'] ? req.cookies['social-redir'] : '/profile';
            res.clearCookie('social-redir');
            res.redirect(url);
        }
    );
}
