import { Request, Response } from 'express';
import { Server } from '../../server/server';
import User, { IUser } from '../../models/user';
import * as config from 'config';

import passport = require('passport');
let VkontakteStrategy = require('passport-vkontakte').Strategy;

interface IVKConfig {
    clientID: string;
    clientSecret: string;
    callbackURL: string;
}

// Routes
export default function (server: Server) {

    let vkConfig: IVKConfig = config.get('vkontakteAuth') as any;
    let host = config.get('host');

    passport.use(new VkontakteStrategy({
            clientID: vkConfig.clientID,
            clientSecret: vkConfig.clientSecret,
            callbackURL: host + vkConfig.callbackURL
        },

        // vkontakte will send back the token and profile
        (token: string, refreshToken: string, profile: any, done: any) => {

            process.nextTick(() => {

                User.findOne({ vkontakte: profile.id }).then((user: IUser) => {
                    if (user) {
                        done(null, user)
                    } else {
                        let name: string[] = profile.displayName.split(' ');
                        let firstname = name[0] ? name[0] : '';
                        let lastname = name[1] ? name[1] : '';
                        let user = new User({
                            email: profile.email,
                            firstname,
                            lastname,
                            role: 'user',
                            vkontakte: profile.id
                        });
                        user.save((err: any) => {
                            if (err) {
                                done(err);
                            }
                            server.telegram.noticeNewUser(user);
                            done(null, user);
                        });
                    }
                }).catch((err: any) => done(err));

            });
        }));

    server.express.get('/auth/vkontakte', (req: any, res: any, next: any) => {
        if (req.query.redir) {
            res.cookie('social-redir', req.query.redir);
        }
        next();
    }, passport.authenticate('vkontakte'));

    server.express.get(vkConfig.callbackURL,
        passport.authenticate('vkontakte', { failureRedirect: '/login' }),
        (req: Request, res: Response) => {
            server.authorize.setToken(req.user, req, res);
            let url = req.cookies['social-redir'] ? req.cookies['social-redir'] : '/profile';
            res.clearCookie('social-redir');
            res.redirect(url);
        }
    );

}
