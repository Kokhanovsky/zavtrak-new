import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { SitemapController } from './sitemap.controller';

export default (server: Server) => {

    server.express.get('/sitemap-recipes.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.recipes();
        });

    server.express.get('/sitemap-articles.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.articles();
        });

    server.express.get('/sitemap-videos.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.videos();
        });

    server.express.get('/sitemap-advices.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.advices();
        });


}
