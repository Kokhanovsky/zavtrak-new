import { Request, Response } from 'express';
import { Server } from '../../server/server';
import Recipe, { IRecipe } from '../../models/recipe';
import { Common } from '../../lib/common';
import Article, { IArticle } from '../../models/article';
import { IAdvice, default as Advice } from '../../models/advice';
import Video, { IVideo } from '../../models/video';
import moment = require('moment');

let sm = require('sitemap');

export class SitemapController {

    private sitemap;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.sitemap = sm.createSitemap({
            hostname: 'https://zavtraka.net',
            cacheTime: 600000
        });
    }

    recipes() {
        return Recipe.find({ status: 'approved' }).then((recipes: IRecipe[]) => {
            recipes.forEach((recipe: IRecipe) => this.sitemap.add({ url: Common.getRecipeUrl(recipe) }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    articles() {
        return Article.find({ status: 'approved' }).then((articles: IArticle[]) => {
            articles.forEach((article: IArticle) => this.sitemap.add({ url: Common.getArticleUrl(article) }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    advices() {
        return Advice.find({ status: 'approved' }).then((advices: IAdvice[]) => {
            advices.forEach((advice: IAdvice) => this.sitemap.add({ url: Common.getAdviceUrl(advice) }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    videos() {
        return Video.find({ status: 'approved' }).then((videos: IVideo[]) => {
            videos.forEach((video: IVideo) => this.sitemap.add({ url: Common.getVideoUrl(video) }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

}
