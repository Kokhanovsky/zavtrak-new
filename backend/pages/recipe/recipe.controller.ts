import { Response, Request } from 'express';
import { IRecipe, default as Recipe } from '../../models/recipe';
import { RecipeModel } from './recipe.model';
import { Common } from '../../lib/common';
import { RecipeContent } from './recipe.content';
import { ItemsLib } from '../../lib/items.lib';
import { IItemsList } from '../../lib/interfaces';
import * as mongoose from 'mongoose';
import File, { IFile } from '../../models/file';
import * as _ from 'underscore';
import { PhotoLib } from '../../lib/photo.lib';
import { IComment } from '../../models/comment';

export class RecipeController {

    model: RecipeModel;
    content: RecipeContent;
    photoLib: PhotoLib;

    constructor(private req: Request, private res: Response, private next: any) {
        this.model = new RecipeModel(req);
        this.photoLib = new PhotoLib(req, res, next);
    }

    renderRecipeAdd() {
        let id = this.req.params && this.req.params.id;
        let jsScripts = [
            '<script src="https://www.youtube.com/iframe_api"></script>'
        ];
        if (id) {
            this.model.getRecipeById(id).then(() => {
                this.res.render(require.resolve('./templates/recipe-add.template.ejs'), {
                    title: 'Редактирование рецепта',
                    id,
                    jsScripts
                });
            }).catch(this.next);
        } else {
            this.res.render(require.resolve('./templates/recipe-add.template.ejs'), {
                title: 'Добавление рецепта',
                id,
                jsScripts
            });
        }
    }

    renderRecipesList() {
        this.model.getRecipesList().then((recipes: IItemsList) => {
            let params = this.model.RecipeParams.params;
            let content = RecipeContent.getListContent(params);
            this.res.render(require.resolve('./templates/recipes-list.template.ejs'), {
                title: content.title,
                titleHead: content.titleHead,
                metaDescription: content.metaDescription,
                metaKeywords: content.metaKeywords,
                breadcrumbs: RecipeContent.getBreadcrumbsRecipeList(params),
                recipes,
                params,
                queryParams: this.model.RecipeParams.getQueryParams()
            });
        }).catch(err => this.next(err));
    }

    renderRecipe() {
        let recipe: IRecipe;
        let comments: IComment[];
        this.model.getRecipe().then((recipe: IRecipe) => {
            let jsScripts = [
                '<script src="https://www.youtube.com/iframe_api"></script>'
            ];
            if (!recipe) {
                let err = new Error('Recipe not found');
                (err as any).status = 404;
                return this.next(err);
            }
            if ((recipe.status === 'denied') || (recipe.status === 'new')) {
                this.res.status(404);
            }
            Promise.all([
                ItemsLib.getSimilarItems(recipe, 'recipe'),
                ItemsLib.getComments(recipe, 'recipe'),
                ItemsLib.getRating(recipe, 'recipe')
            ]).then((d: any[]) => {
                this.res.render(require.resolve('./templates/recipe.template.ejs'), {
                    title: recipe.title + '. Пошаговый рецепт с фото.',
                    metaDescription: Common.truncate(recipe.description, 100),
                    metaKeywords: recipe.tags.map((t: any) => t.tag),
                    breadcrumbs: RecipeContent.getBreadcrumbsRecipe(recipe),
                    recipe,
                    similarItems: d[0],
                    comments: d[1],
                    rating: d[2],
                    ratingCaption: 'Оцените рецепт',
                    jsScripts
                })
            }).catch(err => this.next(err))
        });
    }

    add() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let recipe: IRecipe = new Recipe({
                user: this.req.user.id,
                title: reqItem.title,
                description: reqItem.description,
                time: reqItem.time,
                slug: Common.translit(reqItem.title),
                photos: reqItem.photos,
                steps: reqItem.steps,
                ingredients: reqItem.ingredients,
                tags: reqItem.tags,
                tag_string: reqItem.tags.join(' '),
                categories: reqItem.categories,
                category_string: reqItem.tags.join(' '),
            });
            recipe.save().then((recipe: IRecipe) => {
                let photos = _.flatten([reqItem.steps.map(step => step.photos), reqItem.photos]).filter(photo => !!photo);
                this.addPhotos(recipe._id, photos).then(() => resolve(Common.getRecipeUrl(recipe)));
            }).catch(reject);
        });
    }

    put() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            let filter: any = { _id: reqItem._id };
            if (this.req.user.role !== 'admin') {
                filter['user'] = this.req.user.id;
            }
            Recipe.findOne(filter).then((recipe: IRecipe) => {
                if (!recipe) {
                    reject();
                    let err: any = new Error('The recipe not found');
                    err.status = 404;
                    return this.next(err);
                }
                recipe.title = reqItem.title;
                recipe.description = reqItem.description;
                recipe.ingredients = reqItem.ingredients;
                recipe.time = reqItem.time;
                recipe.photos = reqItem.photos;
                recipe.videos = reqItem.videos;
                recipe.categories = reqItem.categories;
                recipe.category_string = reqItem.categories.join(' ');
                recipe.tags = reqItem.tags;
                recipe.tag_string = reqItem.tags.join(' ');
                recipe.steps = reqItem.steps;
                recipe.save().then((recipe: IRecipe) => {
                    let photos = _.flatten([reqItem.steps.map(step => step.photos), reqItem.photos]).filter(photo => !!photo);
                    this.addPhotos(recipe._id, photos).then(() => resolve(Common.getRecipeUrl(recipe)));
                }).catch(reject);
            }).catch(reject);
        });

    }

    private addPhotos(item: mongoose.Schema.Types.ObjectId, photos: IFile[]) {
        return new Promise((resolve, reject) => {
            if (!photos || !photos.length) {
                return resolve();
            }
            let promises = [];
            photos.forEach(photo => {
                File.findOne({ _id: photo._id, 'user': this.req.user.id }).then(file => {
                    if (file) {
                        file.item = item;
                        file.description = photo.description;
                        promises.push(file.save());
                    }
                }).catch(err => reject(err));
            });
            Promise.all(promises).then(res => resolve()).catch(err => reject(err));
        });
    }

}
