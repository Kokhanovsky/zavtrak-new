import { IBreadcrumb, IContent } from '../../lib/interfaces';
import { IRecipe } from '../../models/recipe';
import { IParams } from './recipe.params';
import { ICategory } from '../../models/category';
import * as path from 'path';


export class RecipeContent {

    static getBreadcrumbsRecipe(recipe: IRecipe) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Рецепты', url: '/recipes'});
        res.push({title: recipe.title, url: null});
        return res;
    }

    static getBreadcrumbsRecipeList(params: IParams) {
        let res: IBreadcrumb[] = [];
        res.push({title: 'Главная', url: '/'});
        res.push({title: 'Рецепты', url: '/recipes'});

        if (params.category && params.category.ancestors && params.category.ancestors.length) {
            params.category.ancestors.reverse().forEach((cat: ICategory) => {
                res.push({title: cat.title, url: path.join('/recipes', cat.slug)});
            });
        }

        if (params.tag) {
            res.push({title: params.tag.tag, url: path.join('/recipes/bytag', params.tag.slug)});
        }

        return res;
    }

    static getListContent(params: IParams): IContent {
        let content: IContent = {};
        content.title = 'Кулинарные рецепты - рецепты на Zavtraka.net';
        content.titleHead = 'Кулинарные рецепты';
        if (params.tag) {
            content.title = params.tag.tag + ' - рецепты на Zavtraka.net';
            content.titleHead = params.tag.tag;
        }
        if (params.category) {
            content.title = params.category.title;
            content.titleHead = params.category.title;
        }
        return content;
    }

}
