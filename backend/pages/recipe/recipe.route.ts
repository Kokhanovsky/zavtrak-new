import { Request, Response } from "express";
import { Server } from "../../server/server";
import { RecipeController } from './recipe.controller';
import * as multer from "multer";
import { PhotoLib } from '../../lib/photo.lib';

export default (server: Server) => {

    let storage = multer.memoryStorage();
    let upload = multer({ storage, limits: { fileSize: 1024 * 1024 * 15 } });

    server.express.get('/recipe-add/:id?',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            ctrl.renderRecipeAdd();
        });

    server.express.get(/^\/recipes\/item([0-9]+)\/(.+)\/?/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            ctrl.renderRecipe();
        });

    server.express.get(/(^\/recipes\/bytag\/(.+)?)|(^\/recipes(\/.+)?)/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            ctrl.renderRecipesList();
        });

    server.express.post('/api/recipe',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            return ctrl.add().then(url => server.sendJson(res, { url })).catch(err => next(err));
        });

    server.express.put('/api/recipe',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            return ctrl.put().then(url => server.sendJson(res, { url })).catch(err => next(err));
        });

    server.express.get('/api/recipe/edit',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new RecipeController(req, res, next);
            return ctrl.model.getRecipeById(req.query.id).then(data => {
                server.sendJson(res, data)
            }).catch(err => next(err));
        });

    server.express.post(
        '/api/recipe/photo',
        server.authorize.jwt('user'),
        upload.array('files', 8),
        (req: Request, res: Response, next: any) => {
            let photoLib = new PhotoLib(req, res, next);
            photoLib.upload('recipe')
            .then(files => server.sendJson(res, files))
            .catch(next);
        });

    server.express.delete(
        '/api/recipe/photo',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let photoLib = new PhotoLib(req, res, next);
            photoLib.deleteImage(req.body.file._id, 'recipe').then(() => {
                server.sendJson(res, { status: 'ok' });
            }).catch(next);
        });
}
