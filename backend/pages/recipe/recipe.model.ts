import { Request } from 'express';
import { IParams, RecipeParams } from './recipe.params';
import Recipe, { IRecipe } from '../../models/recipe';
import { IVideo } from '../../models/video';
import { IArticle } from '../../models/article';
import { ItemsLib } from '../../lib/items.lib';
import { ISimilarItem, IImage, IItemsList } from '../../lib/interfaces';
import { Common } from '../../lib/common';
import { IAdvice } from '../../models/advice';
import Rating from '../../models/rating';
import Comment, { IComment } from '../../models/comment';

export class RecipeModel {

    RecipeParams: RecipeParams; // class

    static getImage(recipe: IRecipe): IImage {
        let image: IImage;
        if (!recipe.photos.length) {
            return null;
        }
        image = {
            src: '/storage/cache/recipes/' + recipe.photos[0]._id + '/300x290.jpg',
            description: recipe.photos[0].description
        };
        return image;
    }

    static getSimilar(item: IVideo | IRecipe | IArticle | IRecipe | IAdvice, limit = 100, exclude?: any[]) {
        let filters = ItemsLib.getSimilarFilters(item, exclude);
        return Recipe.find(filters,
            { score: { $meta: "textScore" } })
        .sort({ score: { $meta: 'textScore' } })
        .select('title slug photos recipeNumber')
        .populate('categories photos')
        .limit(limit)
        .lean().then((recipes: any[]) => {
            let similarItems: ISimilarItem[] = [];
            recipes.forEach(recipe => {
                similarItems.push({
                    title: recipe.title,
                    image: RecipeModel.getImage(recipe),
                    url: Common.getRecipeUrl(recipe),
                    type: 'recipe',
                    score: recipe.score
                })
            });
            return similarItems;
        });
    }

    constructor(private req: Request) {
        this.RecipeParams = new RecipeParams(req);
    }

    getRecipesList() {
        let perPage = 16;
        let filter = { status: 'approved' };

        return this.RecipeParams.getRecipeListParams()
            .then((params: IParams) => {
                if (params.q) {
                    filter['$text'] = { $search: params.q };
                }
                if (params.category) {
                    filter['categories'] = { $in: params.category.children.map(cat => cat._id) };
                }
                if (params.tag) {
                    filter['tags'] = { $in: [params.tag._id] };
                }
                return Recipe.find(filter)
                    .sort({createdAt: -1})
                    .select('recipeNumber title slug photos createdAt')
                    .populate('categories photos')
                    .limit(perPage)
                    .skip(perPage * (params.p - 1))
                    .lean()
                    .then((data: any) => Recipe.count(filter).then(count => {
                        let res: IItemsList = {
                            data,
                            paging: {
                                pageNum: params.p,
                                pagesCount: Math.ceil(count / perPage)
                            }
                        };
                        return res;
                    }))
            }
        );

    }

    getRecipe() {
        return this.RecipeParams.getRecipeParams()
            .then((params: IParams) => Recipe.findOne({
                recipeNumber: params.recipeNumber,
                slug: params.recipeSlug
            })
            .populate('categories photos steps.photos user tags ingredients.food ingredients.measure steps.ingredients.food steps.ingredients.measure')
            .lean());
    }

    getRecipeById(id: string) {
        let filter: any = { _id: id };
        if (this.req.user.role !== 'admin') {
            filter['user'] = this.req.user.id;
        }
        return new Promise((resolve, reject) => {
            Recipe.findOne(filter)
            .select('categories tags photos ingredients slug recipeNumber description steps title time videos')
            .populate('photos ingredients.food ingredients.measure steps.photos steps.ingredients.food steps.ingredients.measure')
            .lean()
            .then((recipe: any) => {
                if (!recipe) {
                    let error: any = new Error('Recipe not found');
                    error.status = 404;
                    return reject(error);
                } else {
                    recipe.ingredients = this.processIngredients(recipe.ingredients);
                    recipe.photos = this.processPhotos(recipe.photos);
                    recipe.steps = this.processSteps(recipe.steps);
                    return resolve(recipe);
                }
            })
            .catch(reject);
        });
    }

    processIngredients(ingredients: any[]) {
        if (!ingredients || !ingredients.length) {
            return ingredients;
        }
        return ingredients.map(i => {
            let res = {};
            if (i.food) {
                res['food'] = {
                    _id: i.food._id,
                    food: i.food.food
                }
            }
            if (i.measure) {
                res['measure'] = {
                    _id: i.measure._id,
                    measure: i.measure.measure
                }
            }
            if (i.quantity) {
                res['quantity'] = i.quantity;
            }
            return res;
        })
    }

    processPhotos(photos: any[]) {
        if (!photos || !photos.length) {
            return photos;
        }
        return photos.map(photo => {
            return {
                _id: photo._id,
                description: photo.description
            }
        });
    }

    processSteps(steps: any[]) {
        if (!steps || !steps.length) {
            return steps;
        }
        return steps.map(step => {
            return {
                _id: step._id,
                photos: this.processPhotos(step.photos),
                ingredients: this.processIngredients(step.ingredients),
                description: step.description
            }
        });
    }
}
