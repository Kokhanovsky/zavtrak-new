import { Request } from 'express';
import { Common } from '../../lib/common';
import Category, { ICategory } from '../../models/category';
import Tag, { ITag } from '../../models/tag';

export interface IParams {
    q?: string;
    p?: number;
    sort?: string;
    recipeNumber?: string;
    recipeSlug?: string;
    categorySlug?: string;
    category?: ICategory;
    tagSlug?: string;
    tag?: ITag;
}

export class RecipeParams {

    public params: IParams = {};

    constructor(private req: Request) {
    }

    getRecipeListParams() : Promise<any> {
        this.params.p = this.req.query.p ? Math.abs(this.req.query.p * 1) : 1;
        if (this.req.query.q) {
            this.params.q = this.req.query.q;
        }
        if (this.req.params[1]) {
            this.params.tagSlug = Common.trimChar('/', this.req.params[1]);
            return this.getTagBySlug().then((tag: ITag) => {
                this.params.tag = tag;
                return this.params;
            });
        }
        if (this.req.params[3]) {
            this.params.categorySlug = Common.trimChar('/', this.req.params[3]);
            return this.getCategoryBySlug().then((category: ICategory) => {
                this.params.category = category;
                return this.params;
            });
        }
        return Promise.resolve(this.params);
    }

    getRecipeParams(): Promise<any> {
        this.params = {
            recipeNumber: this.req.params[0],
            recipeSlug: Common.trimChar('/', this.req.params[1])
        };
        let promises: Array<Promise<any>> = [ Promise.resolve() ];
        return Promise.all(promises).then(res => {
            return this.params;
        });
    }

    getQueryParams() {
        let params: any = {};
        if (this.params.p) {
            params.p = this.params.p;
        }
        if (this.params.q) {
            params.q = this.params.q;
        }
        return params;
    }

    private getCategoryBySlug() {
        return new Promise((resolve, reject) => {
            Category.findOne({slug: this.params.categorySlug})
            .then((category: any) => {
                if (!category) {
                    let error: any = new Error('Category not found');
                    error.status = 404;
                    return reject(error);
                }
                Promise.all([
                    this.getChildren(category),
                    this.getAncestors(category)
                ]).then((d: any[]) => {
                    category.children = d[0];
                    category.ancestors = d[1];
                    return resolve(category);
                });
            }).catch(reject);
        });
    }

    private getTagBySlug() {
        return new Promise((resolve, reject) => {
            Tag.findOne({slug: this.params.tagSlug})
            .then((tag: any) => {
                if (!tag) {
                    let error: any = new Error('Tag not found');
                    error.status = 404;
                    return reject(error);
                }
                return resolve(tag);
            }).catch(reject);
        });
    }

    getAncestors(category: ICategory): Promise<ICategory[]> {
        let ancestors: ICategory[] = [category];
        return new Promise((resolve, reject) => {
            return (category as any).getAncestors((err: any, docs: ICategory[]) => {
                if (err) {
                    return reject(err);
                }
                if (!docs || !docs.length) {
                    return resolve(ancestors);
                }
                resolve(ancestors.concat(docs));
            });
        });
    }

    getChildren(category: ICategory): Promise<ICategory> {
        let children: ICategory[] = [category];
        return new Promise((resolve, reject) => {
            return (category as any).getChildren((err: any, docs: ICategory[]) => {
                if (err) {
                    return reject(err);
                }
                if (!docs || !docs.length) {
                    return resolve(children);
                }
                resolve(children.concat(docs));
            });
        });
    }

}
