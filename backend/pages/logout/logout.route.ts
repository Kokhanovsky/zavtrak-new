import { Request, Response } from 'express';
import { Server } from '../../server/server';

export default function (server: Server) {

    server.express.get('/logout',
        (req: Request, res: Response) => {

            server.authorize.logout(res);
            res.redirect('/');

        });

}
