import * as mongoose from 'mongoose';
import { IUser } from './user';
let AutoIncrement = require('mongoose-sequence');

let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface IVideo extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    videoNumber: number;
    slug: string,
    video_id: string;
    title: string;
    description: string;
    category_string: string;
    categories: mongoose.Schema.Types.ObjectId[];
    tag_string: string;
    tags: mongoose.Schema.Types.ObjectId[];
    user: IUser;
    status: string,
    createdAt: Date;
}

let videoSchema = new mongoose.Schema({
    videoNumber: Number,
    slug: String,
    video_id: String,
    title: String,
    description: String,
    category_string: String,
    categories: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category', index: true }],
        default: []
    },
    tag_string: String,
    tags: {
        type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Tag', index: true}],
        default: []
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'videoNumber' })
.plugin(mongoosePaginate);

videoSchema.index({
        'title': 'text',
        'description': 'text',
        'tag_string': 'text'
    },
    {
        name: 'textIndex',
        weights: {
            title: 10,
            tags: 10,
            content: 5
        },
        default_language: 'russian'
    });

const Video: mongoose.Model<IVideo> = mongoose.model<IVideo>('Video', videoSchema);

export default Video;
