import * as mongoose from 'mongoose';
import { IUser } from './user';

(mongoose as any).Promise = global.Promise;

export interface IChat extends mongoose.Document {
    id: mongoose.Schema.Types.ObjectId,
    user: IUser;
    chatId: number;
}

let chatSchema = new mongoose.Schema({
    chatId: String,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

const Chat = mongoose.model<IChat>('Chat', chatSchema) as any;

export default Chat;
