import * as mongoose from 'mongoose';
import { IUser } from './user';
import { IUserData } from '../lib/interfaces';
let materializedPlugin = require('mongoose-materialized');

let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface IComment extends mongoose.Document {
    parentId: IComment;
    item: mongoose.Schema.Types.ObjectId;
    comment: string;
    user: IUser;
    userData?: IUserData;
    type: string;
    ids?: mongoose.Schema.Types.ObjectId[];
    ancestors?: IComment[];
    status: string,
    createdAt: Date;
}

let commentSchema = new mongoose.Schema({
    comment: { type: String, index: true },
    type: { type: String, index: true },
    item: mongoose.Schema.Types.ObjectId,
    parentId: { type: mongoose.Schema.Types.ObjectId, ref: 'Comment', index: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
});

commentSchema.plugin(materializedPlugin)
.plugin(mongoosePaginate);

const Comment = mongoose.model<IComment>('Comment', commentSchema) as any;

export default Comment;
