import * as mongoose from 'mongoose';
import { IUser } from './user';
import { IFile } from './file';

let Food = require('./food');
let Measure = require('./measure');
let Tag = require('./tag');
let User = require('./user');
let Category = require('./category');

let mongoosePaginate = require('mongoose-paginate');
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IRecipeIngredient {
    ingredient: mongoose.Schema.Types.ObjectId,
    measure: mongoose.Schema.Types.ObjectId,
    quantity: number;
}

export interface IRecipeVideo {
    title?: string;
    youtubeId: string;
}

export interface IRecipeStep {
    description: string;
    ingredients?: IRecipeIngredient[],
    photos?: mongoose.Schema.Types.ObjectId[];
}

export interface IRecipe extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    recipeNumber: number;
    title: string;
    description: string;
    time: number;
    slug: string;
    ingredients: IRecipeIngredient[],
    videos: IRecipeVideo[];
    steps: IRecipeStep[];
    cuisine: mongoose.Schema.Types.ObjectId;
    category_string: string;
    categories: mongoose.Schema.Types.ObjectId[];
    tag_string: string;
    tags: mongoose.Schema.Types.ObjectId[];
    user: IUser;
    photos?: IFile[];
    status: string,
    createdAt: Date;
}

let recipeSchema = new mongoose.Schema({
    recipeNumber: Number,
    title: String,
    description: String,
    time: Number,
    slug: String,
    category: mongoose.Schema.Types.ObjectId,
    cuisine: mongoose.Schema.Types.ObjectId,
    photos: {
        type: [{ type: mongoose.Schema.Types.ObjectId,ref: 'File', index: true }],
        default: []
    },
    ingredients: {
        type: [{
            food: { type: mongoose.Schema.Types.ObjectId, ref: 'Food', index: true },
            measure: { type: mongoose.Schema.Types.ObjectId, ref: 'Measure', index: true },
            quantity: Number
        }],
        default: []
    },
    videos: {
        type: [{
            title: String,
            youtubeId: String
        }],
        default: []
    },
    steps: {
        type: [{
            description: String,
            ingredients: {
                type: [{
                    food: { type: mongoose.Schema.Types.ObjectId, ref: 'Food', index: true },
                    measure: { type: mongoose.Schema.Types.ObjectId, ref: 'Measure', index: true },
                    quantity: Number
                }],
                default: []
            },
            photos: {
                type: [{
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'File'
                }],
                default: []
            }
        }],
        default: []
    },
    category_string: String,
    categories: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category', index: true }],
        default: []
    },
    tag_string: String,
    tags: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag', index: true }],
        default: []
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'recipeNumber' })
.plugin(mongoosePaginate);

recipeSchema.index({
        'title': 'text',
        'description': 'text',
        'tag_string': 'text'
    },
    {
        name: 'textIndex',
        weights: {
            title: 10,
            tags: 10,
            content: 5
        },
        default_language: 'russian'
    });

const Recipe: mongoose.Model<IRecipe> = mongoose.model<IRecipe>('Recipe', recipeSchema);

export default Recipe;
