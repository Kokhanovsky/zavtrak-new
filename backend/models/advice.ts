import * as mongoose from 'mongoose';
import { IUser } from './user';

(mongoose as any).Promise = global.Promise;

let mongoosePaginate = require('mongoose-paginate');
let AutoIncrement = require('mongoose-sequence');

export interface IAdvice extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    adviceNumber: number;
    slug: string;
    content: string;
    tag_string: string;
    tags: mongoose.Schema.Types.ObjectId[];
    user: IUser;
    photos?: mongoose.Schema.Types.ObjectId[];
    status: string,
    createdAt: Date;
}

let adviceSchema = new mongoose.Schema({
    adviceNumber: Number,
    slug: String,
    content: String,
    photos: {
        type: [{ type: mongoose.Schema.Types.ObjectId,ref: 'File', index: true }],
        default: []
    },
    tag_string: String,
    tags: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag', index: true }],
        default: []
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'adviceNumber' })
.plugin(mongoosePaginate);

adviceSchema.index({
        'content': 'text',
        'tag_string': 'text'
    },
    {
        name: 'textIndex',
        weights: {
            tag_string: 10,
            content: 5
        },
        default_language: 'russian'
    });

const Advice: mongoose.Model<IAdvice> = mongoose.model<IAdvice>('Advice', adviceSchema);

export default Advice;
