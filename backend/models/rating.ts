import * as mongoose from 'mongoose';
import { IUser } from './user';

(mongoose as any).Promise = global.Promise;

export interface IRating extends mongoose.Document {
    user: IUser;
    item?: mongoose.Schema.Types.ObjectId;
    type?: string;
    rate?: number;
    createdAt?: Date;
}

let ratingSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    item: mongoose.Schema.Types.ObjectId,
    type: String,
    rate: Number,
    createdAt: { type: Date, default: Date.now }
});

const Rating: mongoose.Model<IRating> = mongoose.model<IRating>('Rating', ratingSchema);

export default Rating;
