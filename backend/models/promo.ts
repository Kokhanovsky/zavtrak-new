import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

export interface IPromo extends mongoose.Document {
    header: string;
    color: string;
    url: string;
    content: string;
    image: string;
    order: number;
    createdAt: Date;
}

let promoSchema = new mongoose.Schema({
    header: String,
    color: String,
    url: String,
    content: String,
    image: String,
    order: { type: Number, 'default': 0 },
    createdAt: { type: Date, 'default': Date.now }
});

const Promo: mongoose.Model<IPromo> = mongoose.model<IPromo>('Promo', promoSchema);

export default Promo;
