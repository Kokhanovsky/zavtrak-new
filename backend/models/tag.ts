import * as mongoose from 'mongoose';
import { IUser } from './user';

let AutoIncrement = require('mongoose-sequence');

let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface ITag extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    id: number;
    tag: string;
    slug: string;
    user: IUser;
    status: string;
    createdAt: Date;
}

let tagSchema = new mongoose.Schema({
    id: Number,
    tag: String,
    slug: String,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'tagNumber' })
.plugin(mongoosePaginate);

const Tag: mongoose.Model<ITag> = mongoose.model<ITag>('Tag', tagSchema);

export default Tag;
