import * as mongoose from 'mongoose';
import { IUser } from './user';

let AutoIncrement = require('mongoose-sequence');
let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface IFood extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    foodNumber: number;
    user: IUser;
    food: string;
    slug: string;
    status: string;
    createdAt: Date;
}

let foodSchema = new mongoose.Schema({
    foodNumber: Number,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    food: String,
    slug: String,
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'foodNumber' })
.plugin(mongoosePaginate);

const Food: mongoose.Model<IFood> = mongoose.model<IFood>('Food', foodSchema);

export default Food;
