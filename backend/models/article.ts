import * as mongoose from 'mongoose';
import { IUser } from './user';
import { IFile } from './file';

let mongoosePaginate = require('mongoose-paginate');
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IArticle extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    category: mongoose.Schema.Types.ObjectId;
    articleNumber: number;
    title: string;
    content: string;
    annot: string;
    meta_description: string;
    category_id: number;
    tags: mongoose.Schema.Types.ObjectId[];
    tag_string: string;
    user: IUser;
    photos?: IFile[];
    status: string,
    createdAt: Date;
}

let articleSchema = new mongoose.Schema({
    articleNumber: Number,
    title: String,
    content: String,
    annot: String,
    meta_description: String,
    category_id: Number,
    category: mongoose.Schema.Types.ObjectId,
    photos: {
        type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'File' }],
        default: []
    },
    tag_string: String,
    tags: {
        type: [{type: mongoose.Schema.Types.ObjectId, ref: 'Tag', index: true}],
        default: []
    },
    comments: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Tag', index: true,
        default: []
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(AutoIncrement, { inc_field: 'articleNumber' })
.plugin(mongoosePaginate);

articleSchema.index({
        'title': 'text',
        'content': 'text',
        'tag_string': 'text'
    },
    {
        name: 'textIndex',
        weights: {
            title: 10,
            tag_string: 10,
            content: 5
        },
        default_language: 'russian'
    });

const Article: mongoose.Model<IArticle> = mongoose.model<IArticle>('Article', articleSchema);

export default Article;
