import * as mongoose from 'mongoose';

let materializedPlugin = require('mongoose-materialized');

(mongoose as any).Promise = global.Promise;

export interface ICategory extends mongoose.Document {
    id: string;
    parent_id: string;
    title: string;
    slug: string;
    parentId: mongoose.Schema.Types.ObjectId;
    ids?: mongoose.Schema.Types.ObjectId[];
    ancestors?: ICategory[];
    children?: ICategory[];
}

let categorySchema = new mongoose.Schema({
    id: Number,
    parent_id: Number,
    title: String,
    slug: String,
});

categorySchema.plugin(materializedPlugin);

const Category = mongoose.model<ICategory>('Category', categorySchema) as any;

export default Category;
