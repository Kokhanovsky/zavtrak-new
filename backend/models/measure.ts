import * as mongoose from 'mongoose';
import { IUser } from './user';

let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface IMeasure extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    measure: string;
    short: string;
    status: string;
    user: IUser;
    createdAt: Date;
}

let ingredientSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
    measure: String,
    short: String,
    status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
    createdAt: { type: Date, 'default': Date.now }
})
.plugin(mongoosePaginate);

const Measure: mongoose.Model<IMeasure> = mongoose.model<IMeasure>('Measure', ingredientSchema);

export default Measure;
