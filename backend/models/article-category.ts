import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

export interface IArticleCategory extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    id: number;
    title: string;
    slug: string;
}

let articleCategorySchema = new mongoose.Schema({
    id: Number,
    title: String,
    slug: String
});

const ArticleCategory: mongoose.Model<IArticleCategory> = mongoose.model<IArticleCategory>('Article-Category', articleCategorySchema);

export default ArticleCategory;
