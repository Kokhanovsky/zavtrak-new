import * as mongoose from 'mongoose';
import { IUser } from './user';

(mongoose as any).Promise = global.Promise;

export interface IFile extends mongoose.Document {
    user: IUser;
    item: mongoose.Schema.Types.ObjectId;
    type: string;
    description: string;
    createdAt: Date;
}

let fileSchema = new mongoose.Schema({
    user: mongoose.Schema.Types.ObjectId,
    item: mongoose.Schema.Types.ObjectId,
    type: String,
    description: String,
    createdAt: { type: Date, default: Date.now }
});

const File: mongoose.Model<IFile> = mongoose.model<IFile>('File', fileSchema);

export default File;
