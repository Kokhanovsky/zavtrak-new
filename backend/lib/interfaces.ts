import * as mongoose from 'mongoose';

export interface IPagination {
    docs: any[],
    total: number,
    limit: number,
    page: number,
    pages: number
}

export interface ICounts {
    recipes?: number;
    articles?: number;
    advices?: number;
    videos?: number;
    tags?: number;
    ingredients?: number;
    measures?: number;
    comments?: number;
}

export interface IItem {
    _id: mongoose.Schema.Types.ObjectId,
    title: string,
    url: string,
    editUrl: string,
    imageSrc?: string,
    createdAt: string,
    status: string,
    user?: any
}

export interface IBreadcrumb {
    title: string;
    url: string;
}

export interface ISimilarItem {
    title: string;
    image?: IImage;
    url: string;
    type: string;
    score: number;
}

export interface IImage {
    src: string;
    description: string;
}

export interface IPaging {
    pageNum: number;
    pagesCount: number;
}

export interface IItemsList {
    data: any;
    paging: IPaging;
}

export interface IContent {
    title?: string;
    titleHead?: string;
    metaKeywords?: string[],
    metaDescription?: string;
}

export interface IUserData {
    name: string;
    avatarSrc: string;
    profileUrl: string;
    isOnline: boolean;
}

export interface IRatingData {
    value: number;
    max: number;
    count: number;
}

export interface IYoutubeItem {
    title: string,
    description: string,
    youtubeId: string
}
