import * as nodemailer from 'nodemailer';
import * as emailTransport from 'nodemailer-smtp-transport';
import * as config from 'config';
import * as fs from 'fs-extra';
let ejs: any = require('ejs');
let juice = require('juice');

export class SendEmail {

    constructor() {
    }

    send(emailTo: string, subject: string, templateFile: string, data: any) {
        let transporter = nodemailer.createTransport(emailTransport({}));
        return new Promise((resolve, reject) => {
            fs.readFile(templateFile, 'utf-8', (err: any, template: string) => {
                if (err) {
                    reject(err);
                    return;
                }
                let mailData = {
                    from: config.get('noReplyEmail').toString(),
                    to: emailTo,
                    subject,
                    html: ejs.render(juice(template), data)
                };
                transporter.sendMail(mailData, (err: any) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve('The email has been sent');
                });
            });
        });
    }
}
