import * as crypto from "crypto";
import { Request } from "express";
import * as moment from "moment";
import * as _ from "lodash";
import { IUser } from '../models/user';
import { IRecipe } from '../models/recipe';
import { IVideo } from '../models/video';
import { IArticle } from '../models/article';
import { IUserData } from './interfaces';
import { IAdvice } from '../models/advice';
import { IRating } from '../models/rating';


let translit = require("translit")({
    "А": "A",
    "а": "a",
    "Б": "B",
    "б": "b",
    "В": "V",
    "в": "v",
    "Г": "G",
    "г": "g",
    "Д": "D",
    "д": "d",
    "Е": "E",
    "е": "e",
    "Ё": "E",
    "ё": "e",
    "Ж": "Zh",
    "ж": "zh",
    "З": "Z",
    "з": "z",
    "И": "I",
    "и": "i",
    "Й": "Y",
    "й": "y",
    "К": "K",
    "к": "k",
    "Л": "L",
    "л": "l",
    "М": "M",
    "м": "m",
    "Н": "N",
    "н": "n",
    "О": "O",
    "о": "o",
    "П": "P",
    "п": "p",
    "Р": "R",
    "р": "r",
    "С": "S",
    "с": "s",
    "Т": "T",
    "т": "t",
    "У": "U",
    "у": "u",
    "Ф": "F",
    "ф": "f",
    "Х": "Kh",
    "х": "kh",
    "Ц": "Ts",
    "ц": "ts",
    "Ч": "Ch",
    "ч": "ch",
    "Ш": "Sh",
    "ш": "sh",
    "Щ": "Sch",
    "щ": "sch",
    "ь": "",
    "Ы": "Y",
    "ы": "y",
    "ъ": "",
    "Э": "E",
    "э": "e",
    "Ю": "Yu",
    "ю": "yu",
    "Я": "Ya",
    "я": "ya"
});

export class Common {

    static translit(str: string) {
        str = translit(str.toLowerCase()).replace(/[^a-z0-9_\s-]+/g, '');
        return str.replace(/\s+/g, '_');
    }

    static thousandSeparator(str: any) {
        if (!str) {
            return str;
        }
        return str.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1&thinsp;')
    }

    static formatCreatedDate(date: Date) {
        moment.locale("ru");
        let long = 'D MMMM в H:mm';
        if (moment().year() > moment(date).year()) {
            long = 'D MMMM YYYY г. в H:mm';
        }
        return moment(date).calendar(null, {
            lastWeek: long,
            nextWeek: long,
            sameElse: long
        }).toLocaleLowerCase();
    }

    static makeToken(): Promise<string> {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(20, (err: any, buf: any) => {
                if (err) {
                    return reject(err);
                }
                return resolve(buf.toString('hex'));
            });
        });
    }

    static makeHashFromString(str: string) {
        return require('crypto').createHash('md5').update(str).digest("hex");
    }

    static getRandString() {
        return Math.random().toString(36).substr(2, 5);
    }

    static getNetwork(req: Request) {
        let res: any = null;
        if (req.user.facebook) {
            res = {
                network: 'facebook',
                profile: 'https://facebook.com/' + req.user.facebook
            };
        }
        if (req.user.instagram) {
            res = {
                network: 'instagram',
                profile: 'https://instagram.com/' + req.user.instagram
            };
        }
        if (req.user.google) {
            res = {
                network: 'google',
                profile: 'https://plus.google.com/' + req.user.google
            };
        }
        if (req.user.vkontakte) {
            res = {
                network: 'vkontakte',
                profile: 'https://vk.com/id' + req.user.vkontakte
            };
        }
        if (req.user.odnoklassniki) {
            res = {
                network: 'odnoklassniki',
                profile: 'https://ok.ru/profile/' + req.user.odnoklassniki
            };
        }
        return res;
    }

    static getProfileUrl(user: IUser) {
        return '/' + (user.nickname ? 'user/' + user.nickname : 'id/' + user.shortId);
    }

    static getUserSocial(user: IUser) {
        if (user.facebook)
            return 'facebook';
        if (user.odnoklassniki)
            return 'odnoklassniki';
        if (user.instagram)
            return 'instagram';
        if (user.vkontakte)
            return 'vkontakte';
        if (user.google)
            return 'google';
        return null;
    }

    static getUserAvatarUrl(user: IUser) {
        return user.avatarBlob ? '/avatar/user/' + user._id + '.jpg' : null;
    }

    static truncate(text: string, n: number, useWordBoundary: boolean = true) {
        if (!text) {
            return text;
        }
        let isTooLong = text.length > n,
            s_ = isTooLong ? text.substr(0, n - 1) : text;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '...' : s_;
    }

    static trimChar(charToRemove: string, str: string) {
        if (!str) {
            return str;
        }
        while (str.charAt(0) == charToRemove) {
            str = str.substring(1);
        }

        while (str.charAt(str.length - 1) == charToRemove) {
            str = str.substring(0, str.length - 1);
        }
        return str;
    }

    static getRecipeImgSrc(recipe: IRecipe, dimensions: string = '300x290') {
        return (!recipe.photos || !recipe.photos.length) ? '' : '/storage/cache/recipes/'+ recipe.photos[0] + '/' + dimensions + '.jpg';
    }

    static getArticleImgSrc(article: IArticle, dimensions: string = '300x290') {
        return (!article.photos || !article.photos.length) ? '' : '/storage/cache/articles/'+ article.photos[0] + '/' + dimensions + '.jpg';
    }

    static getVideoImgSrc(video: IVideo) {
        return 'http://img.youtube.com/vi/' + video.video_id + '/mqdefault.jpg';
    }

    static getRecipeUrl(recipe: IRecipe) {
        return '/recipes/item' + recipe.recipeNumber + '/' + recipe.slug;
    }

    static getVideoUrl(video: IVideo) {
        return '/videos/item' + video.videoNumber+ '/';
    }

    static getArticleUrl(article: IArticle) {
        return '/arts/art' + article.articleNumber+ '.html';
    }

    static getAdviceUrl(advice: IAdvice) {
        return '/advice/' + advice.adviceNumber + '/' + advice.slug;
    }

    static replaceUrlParam(url, paramName, paramValue) {
        if (paramValue == null)
            paramValue = '';
        let pattern = new RegExp('\\b(' + paramName + '=).*?(&|$)')
        if (url.search(pattern) >= 0) {
            return url.replace(pattern, '$1' + paramValue + '$2');
        }
        return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
    }

    static groupingCond(groupBy, array, index) {
        return (!((index + 1) % groupBy) && (index !== array.length - 1))
    }

    static getUserData(user: IUser): IUserData {
        let name = [];
        if (user.firstname) name.push(user.firstname);
        if (user.lastname) name.push(user.lastname);
        let res: IUserData = {
            name: name.join(' '),
            isOnline: !!(user.lastActivity && (user.lastActivity > moment().add(-15, 'minutes').toDate())),
            avatarSrc: user.avatarBlob ? Common.getUserAvatarUrl(user) : '',
            profileUrl: Common.getProfileUrl(user)
        };
        return res;
    }

}
