import { Request, Response } from "express";
import File, { IFile } from "../models/file";
import * as config from "config";
import * as fs from "fs-extra";
import * as mongoose from 'mongoose';
import * as del from 'del';
import { IArticle } from '../models/article';
import { IRecipe } from '../models/recipe';

let path = require('path');
let sharp = require('sharp');

export class PhotoLib {

    private storageDir: string = config.get('storageDir').toString();

    constructor(private req: Request, private res: Response, private next: any) {
    }

    upload(type: string) {
        let files: any[] = (this.req as any).files;
        return new Promise((resolve, reject) => {
            if (!files || !files.length) {
                return reject(new Error('Empty request'));
            }
            let promises = [];
            files.forEach(file => promises.push(this.saveFile(file, type)));
            Promise.all(promises).then(res => resolve(res)).catch(err => reject(err));
        });
    }

    deleteImage(fileId: string, type: string) {
        return new Promise((resolve, reject) => {
            let filters: any = { _id: fileId, type };
            if (this.req.user.role !== 'admin') {
                filters.user = this.req.user.id;
            }
            File.findOne(filters).then(file => {
                if (!file) {
                    return reject('File not found or not enough privileges');
                }
                let dir = this.getDir(type);
                Promise.all([
                    del(path.join(dir.original, fileId + '.jpg')),
                    del(path.join(dir.cache, fileId)),
                    file.remove()
                ]).then(res => resolve()).catch(err => reject(err));
            });
        });
    }

    private processImage(buffer: any, dir: string, type: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let id = mongoose.Types.ObjectId();
            let shrp = sharp(buffer);
            let fileName = path.join(dir, id + '.jpg');
            shrp.resize(1200, 1200);
            shrp.max().withoutEnlargement()
            .jpeg({ quality: 83 })
            .toFile(fileName)
            .then(() => {
                let file = new File({
                    _id: id,
                    type,
                    user: this.req.user._id
                });
                file.save().then(res => resolve(id)).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    private saveFile(file: any, type: string): Promise<string> {
        return new Promise((resolve, reject) => {
            let dir = this.getDir(type);
            if (!dir) {
                return reject(new Error('Wrong type'));
            }
            fs.ensureDir(dir.original, err => {
                if (err) {
                    return reject(err);
                }
                this.processImage(file.buffer, dir.original, type)
                .then(fileHash => resolve({ _id: fileHash }))
                .catch(err => reject(err));
            });
        });
    }

    private getDir(type: string) {
        let res: any = {};
        switch(type) {
            case 'recipe':
                res = {
                    original: path.resolve(path.join(this.storageDir, 'recipes')),
                    cache: path.resolve(path.join(this.storageDir, 'cache', 'recipes'))
                };
                break;
            case 'article':
                res = {
                    original: path.resolve(path.join(this.storageDir, 'articles')),
                    cache: path.resolve(path.join(this.storageDir, 'cache', 'articles'))
                };
                break;
        }
        return res;
    }

}

