import * as config from 'config';
import * as jwt from 'jsonwebtoken';
import { Common } from './common';
import { IUser } from '../models/user';
import { SendEmail } from '../lib/send.email';

let tplVerifyEmail = require.resolve('./templates/verification.email.template.ejs');

export class EmailVerification {

    private host: string = config.get('host').toString();
    private verificationSecret: string = config.get('verificationSecret').toString();
    private mail: SendEmail;

    constructor() {
        this.mail = new SendEmail();
    }

    verify(user: IUser) {
        return new Promise((resolve, reject) => {
            Common.makeToken().then((token: string) => {
                user.verificationToken = token;
                user.verified = false;
                let url = this.getVerificationUrl(user.email, token);
                this.mail.send(user.email, 'Пожалуйста, подтвердите свой Email', tplVerifyEmail, {
                    user: user.firstname,
                    url
                })
                .then(() => {
                    return resolve(user);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    }

    getVerificationUrl(email: string, token: string): string {
        let base64 = new Buffer(this.makeJwt(email, token)).toString('base64');
        return this.host + '/user/verification/token/' + base64;
    }

    private makeJwt(email: string, token: string) {
        let expires = 60 * 60 * 48; // 48 hours
        return jwt.sign({ email, token }, this.verificationSecret, { expiresIn: expires });
    }
}
