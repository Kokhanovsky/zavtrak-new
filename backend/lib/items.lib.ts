import { IVideo } from '../models/video';
import { IRecipe } from '../models/recipe';
import { IArticle } from '../models/article';
import { ArticleModel } from '../pages/article/article.model';
import { RecipeModel } from '../pages/recipe/recipe.model';
import { VideoModel } from '../pages/video/video.model';
import { ISimilarItem, IRatingData } from './interfaces';
import * as _ from 'underscore';
import { IAdvice } from '../models/advice';
import Comment, {IComment} from '../models/comment';
import { Common } from './common';
import Rating from '../models/rating';
import { IRating } from '../models/rating';

export class ItemsLib {

    static getSimilarTags(item: IVideo | IRecipe | IArticle | IAdvice) {
        let categories = [], tags = [];
        if (item && (item as any).categories && (item as any).categories.length) {
            categories = (item as any).categories;
        }
        if (item && item.tags && item.tags.length) {
            tags = item.tags;
        }
        return tags.concat(categories).map((i: any) => i._id).join(' ');
    }

    static getSimilarFilters(item: IVideo | IRecipe | IArticle | IAdvice, exclude?: any[]) {
        let searchString = ItemsLib.getSimilarTags(item);
        return {
            $and: [
                { '_id': { $ne: item._id } },
                { 'status': 'approved' },
                { $text: { $search: searchString } }
            ]
        };
    }

    static getSimilarItems(item: IVideo | IRecipe | IArticle | IAdvice, type: string = null, limit: number = 8): Promise<ISimilarItem[]> {
        return new Promise((resolve, reject) => {
            Promise.all([
                ArticleModel.getSimilar(item),
                VideoModel.getSimilar(item),
                RecipeModel.getSimilar(item)
            ]).then(data => {
                let res = ItemsLib.mergeSimilarItems(data, type).slice(0, limit);
                return resolve(res);
            }).catch(err => reject(err));
        });
    }

    static mergeSimilarItems(arr: Array<ISimilarItem[]>, type: string = null) {
        let merged: ISimilarItem[] = [];
        arr.forEach(a => merged = merged.concat(a));
        merged.forEach(item => {
           if (item.type === type) {
               item.score += 0.5;
           }
        });
        return _.sortBy(merged, (obj: ISimilarItem) => -obj.score);
    }

    static getComments(item: IArticle | IRecipe | IVideo, type: string) {
        return new Promise((resolve, reject) => {
            Comment.find({type: type, item: item._id}).populate('user parentId').lean().then((comments: IComment[]) => {
                let promises: Promise<any>[] = [];
                comments.forEach(comment => {
                    comment.userData = Common.getUserData(comment.user);
                    delete comment.user;
                    if (comment.parentId) {
                        promises.push(Comment.findOne(comment.parentId).populate('user').lean().then(parentItem => {
                            comment.parentId = parentItem;
                            comment.parentId.userData = Common.getUserData(comment.parentId.user);
                            delete comment.parentId.user;
                            return comment;
                        }));
                    } else {
                        promises.push(Promise.resolve(comment));
                    }
                });
                return Promise.all(promises).then(items => {
                    return resolve(items);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    static getRating(item: IRecipe | IArticle | IVideo, type): Promise<IRatingData> {
        function calcAvgRating(rating: IRating[]) {
            if (!rating.length) {
                return 0;
            }
            let sum = 0;
            let count = 0;
            rating.forEach(r => {
                count += 1;
                sum += r.rate;
            });
            return sum / count;
        }
        return new Promise((resolve, reject) => {
            Rating.find({type, item: item._id}).populate('user').lean().then((rating: IRating[]) => {
                if (!rating) {
                    return resolve(null);
                }
                let ratingData: IRatingData = {
                    value: parseFloat(calcAvgRating(rating).toFixed(2)),
                    count: rating.length,
                    max: 5
                };
                return resolve(ratingData);
            }).catch(err => reject(err));
        });
    }

}
