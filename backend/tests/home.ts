import Recipe from '../models/recipe';
import { IRecipe } from '../models/recipe';
import { Common } from '../lib/common';
import Article, { IArticle } from '../models/article';
import Advice, { IAdvice } from '../models/advice';
import Video, { IVideo } from '../models/video';

export default (server, chai) => {

    before(() => {
        Recipe.find({status: 'approved'}).then((recipes: IRecipe[]) => {
            describe('Get recipes pages', () => {
                recipes.forEach((recipe: IRecipe) => {
                    let url = Common.getRecipeUrl(recipe);
                    it('it should render the recipe' + url, (done) => {
                        chai.request(server.express)
                        .get(url)
                        .end((err, res) => {
                            res.should.have.status(200);
                            done()
                        });
                    });
                });
            });
        });

        Article.find({status: 'approved'}).then((articles: IArticle[]) => {
            describe('Get articles pages', () => {
                articles.forEach((article: IArticle) => {
                    let url = Common.getArticleUrl(article);
                    it('it should render the article ' + url, (done) => {
                        chai.request(server.express)
                        .get(url)
                        .end((err, res) => {
                            res.should.have.status(200);
                            done()
                        });
                    });
                });
            });
        });

        Advice.find({status: 'approved'}).then((advices: IAdvice[]) => {
            describe('Get advice pages', () => {
                advices.forEach((advice: IAdvice) => {
                    let url = Common.getAdviceUrl(advice);
                    it('it should render the advice ' + url, (done) => {
                        chai.request(server.express)
                        .get(url)
                        .end((err, res) => {
                            res.should.have.status(200);
                            done()
                        });
                    });
                });
            });
        });

        Video.find({status: 'approved'}).then((videos: IVideo[]) => {
            describe('Get advice pages', () => {
                videos.forEach((video: IVideo) => {
                    let url = Common.getVideoUrl(video);
                    it('it should render the video ' + url, (done) => {
                        chai.request(server.express)
                        .get(url)
                        .end((err, res) => {
                            res.should.have.status(200);
                            done()
                        });
                    });
                });
            });
        });


    });

    describe('Get static pages', () => {

        it('it should render homepage', function (done) {
            chai.request(server.express)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                //res.body.should.be.a('array');
                //res.body.length.should.be.eql(0);
                done();
            });
        });

        it('it should render agreement', function (done) {
            chai.request(server.express)
            .get('/agreement')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });


        it('it should render recipe add page', (done) => {
            chai.request(server.express)
            .get('/recipe-add')
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
        });

        it('it should render articles page', (done) => {
            chai.request(server.express)
            .get('/articles')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        it('it should render an article page', (done) => {
            chai.request(server.express)
            .get('/arts/art5.html')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        it('it should not access admin moderate page', (done) => {
            chai.request(server.express)
            .get('/adm-service/adm-items/recipes')
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
        });

        it('it should not access admin users page', (done) => {
            chai.request(server.express)
            .get('/adm-service/users')
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
        });

        it('it should render videos page', (done) => {
            chai.request(server.express)
            .get('/videos')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

}
