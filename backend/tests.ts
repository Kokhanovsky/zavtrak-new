process.env.NODE_ENV = 'tests';

let chai = require('chai');
chai.use(require('chai-http'));
chai.should();

import { Server } from './server/server';
let server = new Server();

let testFiles = ['home.js'];

let importTests = (server: Server) => {
    testFiles.forEach((test: string) => require(`${__dirname}/tests/` + test).default(server, chai));
};

describe("top", () => {

    before((done) => {
        server.init().then(() => done());
    });

    importTests(server);

});
