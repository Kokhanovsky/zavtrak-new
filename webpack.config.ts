import * as fs from 'fs-extra';
import * as Webpack from 'webpack';
import webpackConst from './backend/const';
/*

fs.remove(webpackConst.buildPath, () => {
    console.log('Build path successfully deleted');
});
*/

let configCommon = {

    // Makes sure errors in console map to the correct file
    // and line number
    devtool: 'eval',
    entry: [

        // For hot style updates
        'webpack/hot/dev-server',

        // The script refreshing the browser on none hot updates
        'webpack-dev-server/client?http://localhost:' + webpackConst.devPort,

        // Our application
        webpackConst.frontendEntry
    ],
    output: {

        // We need to give Webpack a path. It does not actually need it,
        // because files are kept in memory in webpack-dev-server, but an
        // error will occur if nothing is specified. We use the buildPath
        // as that points to where the files will eventually be bundled
        // in production
        path: webpackConst.buildPath,
        filename: webpackConst.frontendBundleFilename,

        // Everything related to Webpack should go through a build path,
        // localhost:3000/build. That makes proxying easier to handle
        publicPath: '/build/'
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['', '.ts', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: ['ng-annotate', 'ts-loader'],
                exclude: [webpackConst.nodeModulesPath]
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.sass$/,
                loader: 'style!css!sass'
            },
            {
                test: /\.scss/,
                loader: 'style!css!sass'
            },
            {
                test: /\.html$/,
                loader: 'raw'
            }
        ]
    },

    // We have to manually add the Hot Replacement plugin when running
    // from Node
    plugins: [
        new Webpack.HotModuleReplacementPlugin()
    ]
};

//=================================================

let configAdmin = (Object as any).assign({}, configCommon, {
    output: {
        path: webpackConst.buildPath,
        filename: webpackConst.adminBundleFilename,
        publicPath: webpackConst.buildPathName
    },
    entry: [
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:' + webpackConst.devPort,
        webpackConst.adminEntry
    ]
});

let configFrontend = (Object as any).assign({}, configCommon, {});

module.exports = [configFrontend, configAdmin];
